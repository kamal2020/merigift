@extends('layouts.app')

@section('content')
    <div id="rev_slider_1078_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic4export" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.4.1 auto mode -->
        <div id="rev_slider_1078_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-3045" data-transition="parallaxtoleft" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ URL::asset('front_asset/images/shop/bg1.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption shopSlide-Subtitle   tp-resizeme"
                         id="slide-1706-layer-1"
                         data-x="['left','left','left','left']" data-hoffset="['680','15','15','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-102','-102','-92','-68']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-fontsize="['20','20','20','20']"
                         data-lineheight="['25','25','25','25']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 7; white-space: nowrap;text-transform:left;">Flowers Collection </div>
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption shop-slider-title   tp-resizeme"
                         id="slide-1706-layer-2"
                         data-x="['left','left','left','left']" data-hoffset="['680','15','15','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','1']"
                         data-fontsize="['55','55','40','30']"
                         data-lineheight="['65','65','55','35']"
                         data-width="['800','700','621','420']"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":1400,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[10,10,10,10]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 6; text-align:left; max-width:1170px; white-space: normal;"><span class="rev-c-text">Best Collections</span> <br> India's best gift List</div>
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption shopSlider-Button btn  btn-white-outline "
                         id="slide-1706-layer-3"
                         data-x="['left','left','left','left']" data-hoffset="['680','15','15','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['112','112','112','96']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="button"
                         data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                         data-responsive_offset="on"
                         data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[12,12,12,12]"
                         data-paddingright="[35,35,35,35]"
                         data-paddingbottom="[12,12,12,12]"
                         data-paddingleft="[35,35,35,35]"

                         style="z-index: 8; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Explore Now </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-3046" data-transition="parallaxtoleft" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ URL::asset('front_asset/images/shop/bg2.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption shopSlide-Subtitle   tp-resizeme"
                         id="slide-1706-layer-1"
                         data-x="['left','left','left','left']" data-hoffset="['680','15','15','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-102','-102','-92','-68']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-fontsize="['20','20','20','20']"
                         data-lineheight="['25','25','25','25']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 7; white-space: nowrap;text-transform:left;">Couple Collection </div>
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption shop-slider-title   tp-resizeme"
                         id="slide-1706-layer-2"
                         data-x="['left','left','left','left']" data-hoffset="['680','15','15','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','1']"
                         data-fontsize="['55','55','40','30']"
                         data-lineheight="['65','65','55','35']"
                         data-width="['800','700','621','420']"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":1400,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[10,10,10,10]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 6; text-align:left; max-width:1170px; white-space: normal;"><span class="rev-c-text">Affordable Gifts</span> <br> Beautifull Gifts</div>
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption shopSlider-Button btn  btn-white-outline "
                         id="slide-1706-layer-3"
                         data-x="['left','left','left','left']" data-hoffset="['680','15','15','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['112','112','112','96']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="button"
                         data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                         data-responsive_offset="on"
                         data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[12,12,12,12]"
                         data-paddingright="[35,35,35,35]"
                         data-paddingbottom="[12,12,12,12]"
                         data-paddingleft="[35,35,35,35]"

                         style="z-index: 8; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Explore Now </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-3049" data-transition="parallaxtoleft" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ URL::asset('front_asset/images/shop/bg3.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption shopSlide-Subtitle   tp-resizeme"
                         id="slide-1706-layer-11"
                         data-x="['left','left','left','left']" data-hoffset="['680','15','15','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-102','-102','-92','-68']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"
                         data-fontsize="['20','20','20','20']"
                         data-lineheight="['25','25','25','25']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":800,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 7; white-space: nowrap;text-transform:left;">Nice Packing </div>
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption shop-slider-title   tp-resizeme"
                         id="slide-1706-layer-12"
                         data-x="['left','left','left','left']" data-hoffset="['680','15','15','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','1']"
                         data-fontsize="['55','55','40','30']"
                         data-lineheight="['65','65','55','35']"
                         data-width="['800','700','621','420']"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":1400,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[10,10,10,10]"
                         data-paddingleft="[0,0,0,0]"
                         style="z-index: 6; text-align:left; max-width:1170px; white-space: normal;"><span class="rev-c-text">Beautifull</span> <br> Free Gift Packing</div>
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption shopSlider-Button btn  btn-white-outline "
                         id="slide-1706-layer-13"
                         data-x="['left','left','left','left']" data-hoffset="['680','15','15','15']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['112','112','112','96']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="button"
                         data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                         data-responsive_offset="on"
                         data-responsive="off"
                         data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[12,12,12,12]"
                         data-paddingright="[35,35,35,35]"
                         data-paddingbottom="[12,12,12,12]"
                         data-paddingleft="[35,35,35,35]"

                         style="z-index: 8; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Explore Now </div>
                </li>
                <!-- SLIDE  -->

            </ul>
            <div class="tp-bannertimer" style="height: 7px; background-color: rgba(255, 255, 255, 0.25);"></div>	</div>
    </div><!-- END REVOLUTION SLIDER -->
    <div class="container pt90 pb60">
        <div class="title-heading1 mb40">
            <h2>Benifits</h2>
        </div>


        <div class="row">
            <div class="pricing-table  align-items-center">
                <div class="col-md-4 plan">
                    <div class="plan-header">
                        <h1 class="text-info">Benifit 1 </h1>

                    </div>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-check-circle-o"></i> <strong>Unique</strong> Gift</li>
                        <li><i class="fa fa-check-circle-o"></i> <strong>Reliable</strong> Price</li>
                        <li><i class="fa fa-check-circle-o"></i> <strong>Best</strong> Offers</li>
                        <li><i class="fa fa-check-circle-o"></i> <strong>Full</strong> Support</li>
                    </ul>
                </div>
                <div class="col-md-4 plan popular-plan">
                    <div class="plan-header">

                        <h1 class="text-primary">Benifit 2</h1>
                    </div>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-check-circle-o"></i> <strong>Unique</strong> Gift</li>
                        <li><i class="fa fa-check-circle-o"></i> <strong>Reliable</strong> Price</li>
                        <li><i class="fa fa-check-circle-o"></i> <strong>Best</strong> Offers</li>
                        <li><i class="fa fa-check-circle-o"></i> <strong>Full</strong> Support</li>
                    </ul>
                </div>
                <div class="col-md-4 plan">
                    <div class="plan-header">
                        <h1 class="text-info">Benifit 3 </h1>
                    </div>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-check-circle-o"></i> <strong>Unique</strong> Gift</li>
                        <li><i class="fa fa-check-circle-o"></i> <strong>Reliable</strong> Price</li>
                        <li><i class="fa fa-check-circle-o"></i> <strong>Best</strong> Offers</li>
                        <li><i class="fa fa-check-circle-o"></i> <strong>Full</strong> Support</li>
                    </ul>
                </div>
            </div>
        </div>


    </div>

    <div class="container mb80">
        <div class="title-heading1 mb40">
            <h2>Product Overview</h2>
        </div>
        <!-- Carousel -->
        <div class="carousel-product owl-carousel owl-theme">
            <div class="item">

                <a href="#" class="simple-hover">
                    <img src="{{ URL::asset('front_asset/images/shop/p1.jpg') }}" alt="" class="img-fluid">
                </a>
                <div class="clearfix product-meta">
                    <p class="lead mb0"><a href="#">Show More</a></p>
                </div>
            </div>
            <div class="item">
                <a href="#" class="simple-hover">
                    <img src="{{ URL::asset('front_asset/images/shop/p2.jpg') }}" alt="" class="img-fluid">
                </a>
                <div class="clearfix product-meta">

                    <p class="lead mb0"><a href="#">Show More</a></p>
                </div>
            </div>
            <div class="item">
                <a href="#" class="simple-hover">
                    <img src="{{ URL::asset('front_asset/images/shop/p3.jpg') }}" alt="" class="img-fluid">
                </a>
                <div class="clearfix product-meta">

                    <p class="lead mb0"><a href="#">Show More</a></p>
                </div>
            </div>
            <div class="item">
                <a href="#" class="simple-hover">
                    <img src="{{ URL::asset('front_asset/images/shop/p4.jpg') }}" alt="" class="img-fluid">
                </a>
                <div class="clearfix product-meta">
                    <p class="lead mb0"><a href="#">Show More</a></p>
                </div>
            </div>
            <div class="item">
                <a href="#" class="simple-hover">
                    <img src="{{ URL::asset('front_asset/images/shop/p6.jpg') }}" alt="" class="img-fluid">
                </a>
                <div class="clearfix product-meta">
                    <p class="lead mb0"><a href="#">Running Shoes</a></p>
                    <h4 class=""> $25.00</h4>
                </div>
            </div>
            <div class="item">
                <a href="#" class="simple-hover">
                    <img src="{{ URL::asset('front_asset/images/shop/p7.jpg') }}" alt="" class="img-fluid">
                </a>
                <div class="clearfix product-meta">
                    <p class="lead mb0"><a href="#">Show More</a></p>
                </div>
            </div>
        </div> <!-- /Carousel -->
    </div>


    <div class="container pb60">
        <div class="title-heading1 mb40">
            <h2>Choose from more than 300 top brands</h2>
        </div>
        <div class="block_content owl-wrapper catinfo_grid flexRow">
            <div class="owl-item col-xs-12 col-sm-12  col-md-3">
                <div class="item text-xs-center">
                    <div class="catinfo">
                        <a class="text-xs-center catinfo_image" href="#" title="For Him">
                            <figure>
                                <img class="img-fluid" src="{{ URL::asset('front_asset/images/cat-1.jpg') }}" alt="For Him" />
                            </figure>
                        </a>
                        <div class="catinfo_wrap">
                            <h4 class="h5">
                                <a href="#" title="For Him">
                                    Brand 1
                                </a>
                            </h4>


                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-item col-xs-12 col-sm-12  col-md-3">
                <div class="item text-xs-center">
                    <div class="catinfo">
                        <a class="text-xs-center catinfo_image" href="#" title="New baby">
                            <figure>
                                <img class="img-fluid" src="{{ URL::asset('front_asset/images/cat-2.jpg') }}" alt="New baby" />
                            </figure>
                        </a>
                        <div class="catinfo_wrap">
                            <h4 class="h5">
                                <a href="#" title="New baby">
                                    Brand 2
                                </a>
                            </h4>


                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-item col-xs-12 col-sm-12  col-md-3">
                <div class="item text-xs-center">
                    <div class="catinfo">
                        <a class="text-xs-center catinfo_image" href="#" title="Occasions">
                            <figure>
                                <img class="img-fluid" src="{{ URL::asset('front_asset/images/cat-3.jpg') }}" alt="Occasions" />
                            </figure>
                        </a>
                        <div class="catinfo_wrap">
                            <h4 class="h5">
                                <a href="#" title="Occasions">
                                    Brand 3
                                </a>
                            </h4>


                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-item col-xs-12 col-sm-12  col-md-3">
                <div class="item text-xs-center">
                    <div class="catinfo">
                        <a class="text-xs-center catinfo_image" href="#" title="For Her">
                            <figure>
                                <img class="img-fluid" src="{{ URL::asset('front_asset/images/cat-4.jpg') }}" alt="For Her" />
                            </figure>
                        </a>
                        <div class="catinfo_wrap">
                            <h4 class="h5">
                                <a href="#" title="For Her">
                                    Brand 4
                                </a>
                            </h4>


                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-item col-xs-12 col-sm-12  col-md-3">
                <div class="item text-xs-center">
                    <div class="catinfo">
                        <a class="text-xs-center catinfo_image" href="#" title="For Him">
                            <figure>
                                <img class="img-fluid" src="{{ URL::asset('front_asset/images/cat-1.jpg') }}" alt="For Him" />
                            </figure>
                        </a>
                        <div class="catinfo_wrap">
                            <h4 class="h5">
                                <a href="#" title="For Him">
                                    Brand 5
                                </a>
                            </h4>


                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-item col-xs-12 col-sm-12  col-md-3">
                <div class="item text-xs-center">
                    <div class="catinfo">
                        <a class="text-xs-center catinfo_image" href="#" title="New baby">
                            <figure>
                                <img class="img-fluid" src="{{ URL::asset('front_asset/images/cat-2.jpg') }}" alt="New baby" />
                            </figure>
                        </a>
                        <div class="catinfo_wrap">
                            <h4 class="h5">
                                <a href="#" title="New baby">
                                    Brand 6
                                </a>
                            </h4>


                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-item col-xs-12 col-sm-12  col-md-3">
                <div class="item text-xs-center">
                    <div class="catinfo">
                        <a class="text-xs-center catinfo_image" href="#" title="Occasions">
                            <figure>
                                <img class="img-fluid" src="{{ URL::asset('front_asset/images/cat-3.jpg') }}" alt="Occasions" />
                            </figure>
                        </a>
                        <div class="catinfo_wrap">
                            <h4 class="h5">
                                <a href="#" title="Occasions">
                                    Brand 7
                                </a>
                            </h4>


                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-item col-xs-12 col-sm-12  col-md-3">
                <div class="item text-xs-center">
                    <div class="catinfo">
                        <a class="text-xs-center catinfo_image" href="#" title="For Her">
                            <figure>
                                <img class="img-fluid" src="{{ URL::asset('front_asset/images/cat-4.jpg') }}" alt="For Her" />
                            </figure>
                        </a>
                        <div class="catinfo_wrap">
                            <h4 class="h5">
                                <a href="#" title="For Her">
                                    Brand 8
                                </a>
                            </h4>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="bg-faded pt60 pb60">
        <div class="container">
            <div class="title-heading1 mb40">
                <h2>Testimonial</h2>
            </div>
            <!-- Carousel -->
            <div class="carousel-testimonial owl-carousel owl-theme">

                <div class="item">
                    <div class=" testimonial">
                        <div class="row align-items-center">
                            <div class="col-md-3 offset-md-1">
                                <img src="{{ URL::asset('front_asset/images/customer-1.jpg') }}" alt="" class="img-fluid circle">
                            </div>
                            <div class="col-md-7">
                                <p class="lead">
                                    " This is a great theme! I love it and recommend it to anyone wanting to purchase it. "
                                </p>
                                <h5>John Doe - <span>Assan Customer</span></h5>
                            </div>
                        </div>
                    </div>
                </div><!--item-->
                <div class="item">
                    <div class=" testimonial">
                        <div class="row align-items-center">
                            <div class="col-md-3 offset-md-1">
                                <img src="{{ URL::asset('front_asset/images/customer-2.jpg') }}" alt="" class="img-fluid circle">
                            </div>
                            <div class="col-md-7">
                                <p class="lead">
                                    " Compared to others this was a very easy to use and adapt template. It didn't break as easy with minor changes. Thanks "
                                </p>
                                <h5>John Doe - <span>Assan Customer</span></h5>
                            </div>
                        </div>
                    </div>
                </div><!--item-->
                <div class="item">
                    <div class=" testimonial">
                        <div class="row align-items-center">
                            <div class="col-md-3 offset-md-1">
                                <img src="{{ URL::asset('front_asset/images/customer-3.jpg') }}" alt="" class="img-fluid circle">
                            </div>
                            <div class="col-md-7">
                                <p class="lead">
                                    " Hello,
                                    your theme gets better and better on every version. Gratulation to this wonderful theme and please keep on working this way (good once)!! "
                                </p>
                                <h5>John Doe - <span>Assan Customer</span></h5>
                            </div>
                        </div>
                    </div>
                </div><!--item-->
            </div> <!-- /Carousel -->
        </div>
    </div>

@endsection