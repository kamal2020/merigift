@extends('layouts.app')
@section('content')
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Registry Details</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        <div class="col-lg-3 mb40">
            @if(isset($data))
                <div class="mb40">
                    @php $price_array= priceChk($data['product_min_price'],$data['product_max_price']);@endphp
                    {{Form::open(array('url'=>'detail_registry','method'=>'GET'))}}
                    {{Form::hidden('id',null)}}
                    <ul class="list-unstyled categories">
                        <li><b>Price</b></li>
                            <li><label>{{Form::radio('price','all',true)}}All</label></li> 
                            @foreach($price_array as $index => $val)
                                <li><label>{{Form::radio('price',$index,null)}}{{$val}}</label></li>
                            @endforeach
                        <li><b>Category</b></li>
                        <li>
                            {{Form::select('category_id',category(),null,array('class'=>'form-control','placeholder'=>'Select Category'))}}
                        </li>
                    </ul>
                    <button type="submit" class="btn btn-sm btn-primary">Search</button>
                    {{Form::close()}}
                </div><!--/col-->
                @endif
            </div>
            <div class='col-lg-9'>
                @if(isset($registry))
                <div class ="row">

                    @forelse($registry as $data)
                        <div class="owl-item col-xs-12 col-sm-12  col-md-3">
                            <div class="item text-xs-center">
                                <div class="catinfo">
                                    <a class="text-xs-center catinfo_image" href="#" title="Occasions">
                                        <figure>
                                            @if(isset($data->product->product_image))
                                                <img class="img-fluid" src="{{filePath('product',$data->product->product_image->product_image)}}" alt="Occasions" />
                                            @else
                                                <img class="img-fluid" src="{{ url('no_image.png') }}" alt="Product Image" />
                                            @endif
                                           
                                        </figure>
                                    </a>
                                    <div class="catinfo_wrap">
                                            <b> Product : </b>
                                            <a href="{{url('product-details/'.$data->product->id)}}" title="Occasions">
                                                {{$data->product->product_name}}
                                            </a>
                                        <b>Quantity : </b> {{$data->qty}}<br/>
                                        <b>Received  : </b> {{$data->received}}<br/>
                                        <b>Price: </b>{{$data->product->product_price}}<br/>
                                        <b>Brand: </b>{{$data->product->brand->brand_name}}

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    @empty
                        <h3>No records Found </h3>
                    @endforelse
                    
                </div>
                {!! $registry->render() !!}
                @else
                    <h3>No records Found </h3>
                @endif
            </div>
        </div>
    </div>
@endsection
