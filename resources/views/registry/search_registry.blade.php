@extends('layouts.app')
@section('content')
<div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Search Registry</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
            <div class='col-lg-12'>

                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                    {{Form::open(array('url'=>'search-registry','method'=>'GET','class'=>'form-horizantal reg-form'))}}
                    
                    <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6 fnone">
                    {{Form::label('unique_id','Registry Code',array('class'=>'col-sm-12 control-label'))}}
                    <div class="col-sm-10">
                        {{Form::text('unique_id',null,array('class'=>'form-control','placeholder'=>'Enter Unique Id'))}}
                    </div>
                    </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                    <button type="submit" class="btn btn-info ">Submit</button>

                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection
