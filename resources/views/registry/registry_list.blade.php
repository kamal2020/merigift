@extends('layouts.app')
@section('content')
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Registry List</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        @include('layouts.sidebar')
            <div class='col-lg-9'>
                <div class="title-heading1 mb40"><h4>Registry List</h4></div>
                <div class="pull-right">
                    <a href="{{url('/registry')}}">
                        <i class="fa fa-plus"></i> <span>Add Registry</span>
                    </a>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                        <th style="width: 10px">#</th>
                        <th>Registry Type</th>
                        <th>Registry ID</th>
                        <th>Title</th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Created_at</th>
                        <th>Action</th>
                        </tr>
                    <thead>
                    <tbody>
                        @php $i=1;@endphp
                        @foreach($registry as $class)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$class->registryType->title or ''}}</td>
                                <td>{{$class->unique_id}}</td>
                                <td>{{$class->registry_type_title}}</td>
                                <td>{{$class->name}}</td>
                                <td>{{dateFormat($class->date_from) }} to  {{dateFormat($class->date_to)}}</td>
                                <td> {{ dateFormat($class->created_at) }}</td>
                                <td>
                                <a class="btn btn-primary btn-sm" href="{{url('detail_registry',$class->id)}}">Detail&nbsp;&nbsp;</a>
                                <a class="btn btn-primary btn-sm " href="{{url('edit_registry',$class->id)}}">Edit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                <a class="btn btn-danger btn-sm " href="{{url('delete_registry',$class->id)}}">Delete</a>
                                
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
