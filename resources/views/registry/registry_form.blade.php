@extends('layouts.app')
@section('content')
<div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Registery</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        @include('layouts.sidebar')
            <div class='col-lg-9'>

                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                @if(isset($registry))
                        <div class="title-heading1 mb40"><h4>Update Registry</h4></div>
                    {{Form::model($registry,array('url'=>array('update_registry',$registry->id),'id'=>'registry-form','class'=>'form-horizontal reg-form'))}}
                @else
                        <div class="title-heading1 mb40"><h4>Add Registry</h4></div>
                    {{Form::open(array('url'=>'insert_registry','id'=>'registry-form','class'=>'form-horizontal reg-form'))}}
                @endif
                

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    {{Form::label('name','Registry Type',array('class'=>'col-sm-12 control-label'))}}
                    <div class="col-sm-12">
                        {{Form::select('registry_type_id',$registry_type,null,array('class'=>'registry_type form-control','placeholder'=>'Select'))}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group registry_type_div" style="display:none;">
                    {{Form::label('name','Registry Type Title',array('class'=>'col-sm-12 control-label'))}}
                    <div class="col-sm-12">
                        {{Form::text('registry_type_title',null,array('class'=>'form-control registry_type_title','placeholder'=>'Registry Type Title'))}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    {{Form::label('name','Registry Name',array('class'=>'col-sm-12 control-label'))}}
                    <div class="col-sm-12">
                        {{Form::text('name',null,array('class'=>'form-control','placeholder'=>'Enter Name'))}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    {{Form::label('date_from','Date From',array('class'=>'col-sm-12 control-label'))}}
                    <div class="col-sm-12">
                        {{Form::text('date_from',null,array('class'=>'form-control datepicker','placeholder'=>'Enter Date From'))}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    {{Form::label('date_to','Date To',array('class'=>'col-sm-12 control-label'))}}
                    <div class="col-sm-12">
                        {{Form::text('date_to',null,array('class'=>'form-control datepicker1','placeholder'=>'Enter Date To'))}}
                    </div>
                </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('no_of_guest','No of Guest',array('class'=>'col-sm-12 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::number('no_of_guest',null,array('class'=>'form-control','placeholder'=>'Enter No of Guest'))}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('delivery_address','Delivery Address',array('class'=>'col-sm-12 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::text('delivery_address',null,array('class'=>'form-control','placeholder'=>''))}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('delivery_city','Delivery City',array('class'=>'col-sm-12 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::text('delivery_city',null,array('class'=>'form-control','placeholder'=>''))}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('delivery_state','Delivery State',array('class'=>'col-sm-12 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::text('delivery_state',null,array('class'=>'form-control','placeholder'=>''))}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('delivery_pincode','Delivery Pincode',array('class'=>'col-sm-12 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::text('delivery_pincode',null,array('class'=>'form-control','placeholder'=>''))}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('delivery_date','Delivery Date',array('class'=>'col-sm-12 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::text('delivery_date',null,array('id'=>'delivery_date','class'=>'form-control datepicker','placeholder'=>''))}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('mobile_no','Mobile No.',array('class'=>'col-sm-12 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::text('mobile_no',null,array('class'=>'form-control','placeholder'=>''))}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('email','Email',array('class'=>'col-sm-12 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::text('email',null,array('class'=>'form-control','placeholder'=>''))}}
                        </div>
                    </div>



                    <div id="registry_questions">
                        @if(isset($registry_answer))

                            @foreach($registry_answer as $raw)

                             @if($raw->question->type=='text')
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                        {{Form::label('email',$raw->question->question,array('class'=>'col-sm-12 control-label'))}}
                                        <div class="col-sm-12">
                                            {{Form::text($raw->question->id,$raw->answers_value,array('class'=>'form-control','placeholder'=>''))}}
                                        </div>
                                    </div>
                             @endif
                                 @if($raw->question->type=='number')
                                     <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                         {{Form::label('email',$raw->question->question,array('class'=>'col-sm-12 control-label'))}}
                                         <div class="col-sm-12">
                                             {{Form::number($raw->question->id,$raw->answers_value,array('class'=>'form-control','placeholder'=>''))}}
                                         </div>
                                     </div>
                                 @endif
                                 @if($raw->question->type=='date')
                                     <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                         {{Form::label('email',$raw->question->question,array('class'=>'col-sm-12 control-label'))}}
                                         <div class="col-sm-12">
                                             {{Form::text($raw->question->id,$raw->answers_value,array('class'=>'form-control datepicker','placeholder'=>''))}}
                                         </div>
                                     </div>
                                 @endif
                                 @if($raw->question->type=='radio')
                                     @php $value=explode(',',$raw->question->value);
$id_array=array('5','12','19','27');
                                     @endphp
                                     @if(in_array($raw->question->id,$id_array))
                                         @php $class='hide';@endphp
                                     @else
                                         @php $class='new';@endphp
                                     @endif

                                     <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                         {{Form::label('email',$raw->question->question,array('class'=>'col-sm-12 control-label'))}}
                                         <div class="col-sm-12">

                                             @foreach($value as $raw_val)

                                                 @if($raw->answers_value==$raw_val)
                                                 {{$raw_val}}  <input checked="checked" @if(in_array($raw->question->id,$id_array)) class='{{'hide'}}' @endif
                                                     name="{{$raw->question->id}}" type="radio" value="{{$raw_val}}">

                                                 @else
                                                 {{$raw_val}}  <input name="{{$raw->question->id}}" @if(in_array($raw->question->id,$id_array)) class='{{'hide1'}}' @endif type="radio" value="{{$raw_val}}">
                                                 @endif

                                             @endforeach
                                         </div>
                                     </div>

                                 @endif
                                 @if($raw->question->type=='checkbox')
                                    @php $value=explode(',',$raw->question->value); @endphp

                                     <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                         {{Form::label('email',$raw->question->question,array('class'=>'col-sm-12 control-label'))}}
                                         <div class="col-sm-12">
                                         @foreach($value as $raw_val)
                                                 @if($raw->answers_value==$raw_val)
                                                     {{$raw_val}}  <input checked="checked" name="{{$raw->question->id}}" type="checkbox" value="{{$raw_val}}">
                                                 @else
                                                     {{$raw_val}}  <input  name="{{$raw->question->id}}" type="checkbox" value="{{$raw_val}}">
                                                 @endif

                                             @endforeach
                                         </div>
                                     </div>
                                 @endif
                            @endforeach
                        @endif

                    </div>
                <div class="pull-right">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info ">Submit</button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>



    <script>
        function hide(val){
            //var val=$(this).val();
            //alert(val);
            if(val=='No')
            {

                $('.hide').parent().parent().nextAll('.form-group').hide();
                $('.hide').parent().parent().nextAll('.form-group').children().children().removeAttr('required');
            }
            else
            {
                $('.hide').parent().parent().nextAll('.form-group').show();
                $('.hide').parent().parent().nextAll('.form-group').children().children().prop('required',true);
            }
        }

        @if(isset($registry))

        var host='<?=url('/')?>';

        //var id=$('.registry_type').val();
        //var registry_id=<?=$registry->id;?>

        //$.get(host+'/get_registries_questions/'+id+'/'+registry_id,
         //   {
                // _token: "{{ csrf_token() }}",
                //  val: id
          //  },
           // function(response){

              //  $('#registry_questions').html(response);

           // });

        var registry_type_name=$(".registry_type option:selected").text();
        //alert(registry_type_name);
            if(registry_type_name=='Other')
            {
                $(".registry_type_div").show();
                //$(".registry_type_title").val('');
            }
            else{
                $(".registry_type_div").hide();
                $(".registry_type_title").val(registry_type_name);
            }
        @endif

        $(document).on('change','.registry_type',function(){
            $('#registry_questions').html('');
            var registry_type_name=$(".registry_type option:selected").text();
            //alert(registry_type_name);
            if(registry_type_name=='Other')
            {
                $(".registry_type_div").show();
                $(".registry_type_title").val('');
            }
            else{
                $(".registry_type_div").hide();
                $(".registry_type_title").val(registry_type_name);
            }

            //get registery questions//
            var host='<?=url('/')?>';
            var id=$('.registry_type').val();

            $.get(host+'/get_registries_questions/'+id,
                {
                   // _token: "{{ csrf_token() }}",
                   //  val: id
                },
                function(response){

                $('#registry_questions').html(response);

                });




        });

        jQuery.validator.addMethod("greaterThan",
            function(value, element, params) {

                if (!/Invalid|NaN/.test(new Date(value))) {
                    return new Date(value) > new Date($(params).val());
                }

                return isNaN(value) && isNaN($(params).val())
                    || (Number(value) > Number($(params).val()));
            },'Must be greater than {0}.');

        jQuery.validator.addMethod("greaterStart", function (value, element, params) {
            return this.optional(element) || new Date(value) >= new Date($(params).val());
        },'Must be greater than start date.');
        var twentydays = new Date();
        twentydays.setDate(twentydays.getDate() + 20);
        $("#registry-form").validate({
            rules: {
                name: "required",
                date_from: "required",
                registry_type_id : "required",
                no_of_guest: "required",
                delivery_address: "required",
                delivery_city: "required",
                delivery_state: "required",
                delivery_pincode: "required",
                //delivery_date: "required",
                mobile_no: "required",
                email: "required",
                date_to: {
                    required:true,
                    greaterThan: "#date_from"
                },
                delivery_date: {
                    required:true,
                    //date: true,
                    //greaterStart: "#date_to"
                }
            },
            messages:{
                name:{
                    required:"Please Enter Registry Name",
                },
                registry_type_id:{
                    required:"Please Select Registry Type",
                },

                date_from:{
                    required:"Please Enter Start Date",
                },
                date_to:{
                    required:"Please Enter End Date",
                },
                no_of_guest:{
                    required:"Please Enter No of Guest",
                },
                delivery_address:{
                    required:"Please Enter Delivery Address",
                },
                delivery_city:{
                    required:"Please Enter Delivery City",
                },
                delivery_state:{
                    required:"Please Enter Delivery State",
                },
                delivery_pincode:{
                    required:"Please Enter Delivery Pincode",
                },
                delivery_date:{
                    required:"Please Enter Delivery Date",
                },
                mobile_no:{
                    required:"Please Enter Mobile no",
                },
                email:{
                    required:"Please Enter Email",
                },


            }
        });


    </script>
    <script>
        $(document).ready(function() {
            var myVar = $("#registry-form").find('.hide').val();
            //console.log($(".form-group").find('.hide'));
           // alert(myVar);
            if(myVar=='No')
            {
                $('.hide').parent().parent().nextAll('.form-group').hide();
                $('.hide').parent().parent().nextAll('.form-group').children().children().removeAttr('required');
            }
            else
            {
                $('.hide').parent().parent().nextAll('.form-group').show();
                $('.hide').parent().parent().nextAll('.form-group').children().children().prop('required',true);
            }
            $('.hide').click(function(){
                var val=$(this).val();
                //alert(val);
                if(val=='No')
                {
                    //console.log($('.hide').parent().parent().nextAll('.form-group').children());
                    $('.hide').parent().parent().nextAll('.form-group').hide();
                    $('.hide').parent().parent().nextAll('.form-group').children().children().removeAttr('required');
                }
                else
                {
                    $('.hide').parent().parent().nextAll('.form-group').show();
                    $('.hide').parent().parent().nextAll('.form-group').children().children().prop('required',true);
                }
            })
            $('.hide1').click(function(){
                var val=$(this).val();
                //alert(val);
                if(val=='No')
                {
                    //console.log($('.hide').parent().parent().nextAll('.form-group').children());
                    $('.hide').parent().parent().nextAll('.form-group').hide();
                    $('.hide').parent().parent().nextAll('.form-group').children().children().removeAttr('required');
                }
                else
                {
                    $('.hide').parent().parent().nextAll('.form-group').show();
                    $('.hide').parent().parent().nextAll('.form-group').children().children().prop('required',true);
                }
            })
        });

    </script>
@endsection

