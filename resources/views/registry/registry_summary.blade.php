@extends('layouts.app')
@section('content')
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Registry Summary</h1>

                </div>

            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        @php $id= Request::segment(2) or ''; @endphp
        @include('layouts.sidebar')
            <div class='col-lg-9'>
            <div class="row special-feature">
                        <div class="dash-filter-div">
                            <form class="form-inline">
                                <label class="mr-sm-2" for="inlineFormCustomSelect">Registry: </label>
                                {{Form::select('registry_id',$registry,$id,array('id'=>"inlineFormCustomSelect",'class'=>'registry_id custom-select mb-2 mr-sm-2 mb-sm-0'))}}
                                
                            </form>


                            <div class="pull-right">
                                <a href="{{url('registry')}}" class="btn btn-primary mb5">
                                     <span>Add Registry</span>
                                </a>
                            </div>
                            <div class="pull-right mr-10">
                                <a href="{{url('registry_list')}}" class="btn btn-primary mb5">
                                     <span>Registry List</span>
                                </a>
                            </div>



                            <ul id="progress">
                                <li class="active"><a href="{{url('registry-summery')}}">Summery</a></li>
                                <li ><a href="#" class="registry-status">Status</a></li>
                                <li><a href="#" class="registry-edit">Edit</a></li>
                                <li><a href="#" class="registry-thankyou">Thank You</a></li>
                            </ul>
@php
$total=count_registry('total',Request::segment(2));
$bought=count_registry('bought',Request::segment(2));
if($total>0 && $bought>0)
$bought_per=round($bought/$total*100);
else
$bought_per=0;

$total_val=count_registry('total_val',Request::segment(2));
$total_contributed=count_registry('total_contributed',Request::segment(2));
if($total_val>0 && $total_contributed>0)
$total_contributed_per=round($total_contributed/$total_val*100)                                 ;
else
$total_contributed_per=0;
@endphp
                            
                        </div>
                            <div class="col-md-4 col-sm-6 margin20">
                                <a href="#">
                                    <div class="s-feature-box text-center">
                                        <div class="mask-top">
                                            <!-- Icon -->
                                            <i class="fa fa-gift"></i>
                                            <!-- Title -->
                                            <h4>Total Gifts</h4>
                                        </div>
                                        <div class="mask-bottom">
                                            <!-- Icon -->
                                            <i class="fa fa-gift"></i>
                                            <!-- Title -->
                                            <h1>{{$total}}</h1>
                                            <!-- Text -->
                                        </div>
                                    </div>
                                </a>
                            </div><!--services col-->
                        
                        <div class="col-md-4 col-sm-6 margin20">
                             <a href="#">
                                <div class="s-feature-box text-center">
                                    <div class="mask-top">
                                        <!-- Icon -->
                                        <i class="fa fa-credit-card"></i>
                                        <!-- Title -->
                                        <h4>Gifts Bought</h4>
                                    </div>
                                    <div class="mask-bottom">
                                        <!-- Icon -->
                                        <i class="fa fa-credit-card"></i>
                                        <!-- Title -->
                                        <h1>{{$bought}}</h1>
                                        <!-- Text -->
                                        
                                    </div>
                                </div>
                            </a>
                        </div><!--services col-->
                        <div class="col-md-4 col-sm-6 margin20">
                            <a href="#">
                                <div class="s-feature-box text-center">
                                    <div class="mask-top">
                                        <!-- Icon -->
                                        <i class="fa fa-percent"></i>
                                        <!-- Title -->
                                        <h4>Bought</h4>
                                    </div>
                                    <div class="mask-bottom">
                                        <!-- Icon -->
                                        <i class="fa fa-percent"></i>
                                        <!-- Title -->
                                        <h1>{{$bought_per}}</h1>
                                        <!-- Text -->
                                        
                                    </div>
                                </div>
                            </a>
                        </div><!--services col-->
                        <div class="col-md-4 col-sm-6 margin20">
                            <a href="#">
                                <div class="s-feature-box text-center">
                                    <div class="mask-top">
                                        <!-- Icon -->
                                        <i class="fa fa-bar-chart"></i>
                                        <!-- Title -->
                                        <h4>Total Value</h4>
                                    </div>
                                    <div class="mask-bottom">
                                        <!-- Icon -->
                                        <i class="fa fa-bar-chart"></i>
                                        <!-- Title -->
                                        <h4>{{$total_val}}</h4>
                                        <!-- Text -->
                                        
                                    </div>
                                </div>
                            </a>
                        </div><!--services col-->
                        <div class="col-md-4 col-sm-6 margin20">
                            <a href="#">
                            <div class="s-feature-box text-center">
                                <div class="mask-top">
                                    <!-- Icon -->
                                    <i class="fa fa-handshake-o"></i>
                                    <!-- Title -->
                                    <h4>Value Contributed</h4>
                                </div>
                                <div class="mask-bottom">
                                    <!-- Icon -->
                                    <i class="fa fa-handshake-o"></i>
                                    <!-- Title -->
                                    <h4>{{$total_contributed}}</h4>
                                    <!-- Text -->
                                    
                                </div>
                            </div>
                            </a>
                        </div><!--services col-->
                        <div class="col-md-4 col-sm-6 margin20">
                            <a href="#">
                            <div class="s-feature-box text-center">
                                <div class="mask-top">
                                    <!-- Icon -->
                                    <i class="fa fa-percent"></i>
                                    <!-- Title -->
                                    <h4>Contributed</h4>
                                </div>
                                <div class="mask-bottom">
                                    <!-- Icon -->
                                    <i class="fa fa-percent"></i>
                                    <!-- Title -->
                                    <h4>{{$total_contributed_per}}</h4>
                                    <!-- Text -->
                                    
                                </div>
                            </div>
                            </a>
                        </div><!--services col-->
                    </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){

        var reg_id="{{Request::segment(2)}}";
        if(reg_id==''){
            var url="{{url('registry-summery')}}";
            var id = $(".registry_id").val();
            window.location.replace(url+"/"+id);
        }


        var registry_id=$(".registry_id").val();
        var url="{{url('registry-status')}}/" +registry_id;
        var url2="{{url('registry-edit')}}/" +registry_id;
        var url3="{{url('registry-thankyou')}}/" +registry_id;
        $(".registry-status").attr('href',url);
        $(".registry-edit").attr('href',url2);
        $(".registry-thankyou").attr('href',url3);
    });
    $(document).on('change','.registry_id',function(){
        registry_id=$(".registry_id").val();
            var url="{{url('registry-summery')}}";
            var id = $(".registry_id").val();
            window.location.replace(url+"/"+id);

        url="{{url('registry-status')}}/" +registry_id;
        url2="{{url('registry-edit')}}/" +registry_id;
        url3="{{url('registry-thankyou')}}/" +registry_id;
        $(".registry-status").attr('href',url);
        $(".registry-edit").attr('href',url2);
        $(".registry-thankyou").attr('href',url3);
    })
</script>
@endsection
