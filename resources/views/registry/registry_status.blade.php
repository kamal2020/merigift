@extends('layouts.app')
@section('content')
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Registry Status</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        @php $id= Request::segment(2); @endphp
        @include('layouts.sidebar')
            <div class='col-lg-9'>
                <div class="row special-feature">
                    <div class="dash-filter-div">
                        <form class="form-inline">
                            <label class="mr-sm-2" for="inlineFormCustomSelect">Registry: </label>
                            {{Form::select('registry_id',$registry,$id,array('id'=>"inlineFormCustomSelect",'class'=>'registry_id custom-select mb-2 mr-sm-2 mb-sm-0'))}}
                            
                        </form>
                        <ul id="progress">
                            <li ><a href="{{url('registry-summery',$id)}}">Summery</a></li>
                            <li class="active"><a href="#" class="registry-status">Status</a></li>
                            <li><a href="#" class="registry-edit">Edit</a></li>
                            <li><a href="#" class="registry-thankyou">Thank You</a></li>
                        </ul>
                    </div>
                        <table id="example" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Item</th>
                <th>Price (Per_Product)</th>
                <th>Quantity</th>
                <th>Received</th>
                <th>Total Amount</th>
                <th>Contribution</th>
                <th>Status</th>
                <th>Value</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($registry_item as $data)
        <tr>
                <td>{{$data->product->product_name}}</td>
                
                <td>{{$data->product->product_price}}</td>
                <td>{{$data->qty}}</td>
                <td>{{$data->received or '-'}}</td>
                <td>{{($data->product) ? $data->qty * $data->product->product_price : '-'}}</td>
                <td>{{($data->product) ? $data->received * $data->product->product_price : '-'}}</td>
                <td>{{($data->qty==$data->received) ? 'Completed' : 'Pending'}}</td>
                <td>{{$data->product->product_price}}</td>
                <td><button type="button" class="btn btn-primary mb5">Buy</button></td>
            </tr>
        @endforeach
        </tbody>
        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
        var registry_id=$(".registry_id").val();
        var url="{{url('registry-status')}}/" +registry_id;
        var url2="{{url('registry-edit')}}/" +registry_id;
        var url3="{{url('registry-thankyou')}}/" +registry_id;
        $(".registry-status").attr('href',url);
        $(".registry-edit").attr('href',url2);
        $(".registry-thankyou").attr('href',url3);
    });
    $(document).on('change','.registry_id',function(){
        registry_id=$(".registry_id").val();
        url="{{url('registry-status')}}/" +registry_id;
        url2="{{url('registry-edit')}}/" +registry_id;
        url3="{{url('registry-thankyou')}}/" +registry_id;
        $(".registry-status").attr('href',url);
        $(".registry-edit").attr('href',url2);
        $(".registry-thankyou").attr('href',url3);
        location.href = url;
    })
</script>
@endsection
