@extends('layouts.app')
@section('content')
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Registry Edit</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        @php $id= Request::segment(2) or ''; @endphp
        @include('layouts.sidebar')
            <div class='col-lg-9'>
                <div class="row ">
                    <div class="dash-filter-div">
                        <form class="form-inline">
                            <label class="mr-sm-2" for="inlineFormCustomSelect">Registry: </label>
                            {{Form::select('registry_id',$registry,$id,array('id'=>"inlineFormCustomSelect",'class'=>'registry_id custom-select mb-2 mr-sm-2 mb-sm-0'))}}
                            
                        </form>
                        <ul id="progress">
                            <li ><a href="{{url('registry-summery',$id)}}">Summery</a></li>
                            <li ><a href="#" class="registry-status">Status</a></li>
                            <li class="active"><a href="#" class="registry-edit">Edit</a></li>
                            <li><a href="#" class="registry-thankyou">Thank You</a></li>
                        </ul>
                    </div>
                        <table id="example" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
            <tr>
                <th>Category</th>
                <th>Item</th>
                <th>Price</th>
                <th>Qty</th>
                <th>Fav</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </tr>
        </thead>
        <tbody>
        @foreach($registry_item as $data)
        <tr>
                <td>{{$data->product->category->cat_name}}</td>
                <td>{{$data->product->product_name}}</td>
                <td>{{$data->product->product_price}}</td>
                <td>{{$data->qty}}</td>
                <td><i class="fa fa-heart"></i></td>
                <td><i class="fa fa-edit"></i> </td>
                <td><a herf="#"><i class="fa fa-trash"></i></a></td>
            </tr>
        @endforeach
        </tbody>
        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
        var registry_id=$(".registry_id").val();
        var url="{{url('registry-status')}}/" +registry_id;
        var url2="{{url('registry-edit')}}/" +registry_id;
        var url3="{{url('registry-thankyou')}}/" +registry_id;
        $(".registry-status").attr('href',url);
        $(".registry-edit").attr('href',url2);
        $(".registry-thankyou").attr('href',url3);
    });
    $(document).on('change','.registry_id',function(){
        registry_id=$(".registry_id").val();
        url="{{url('registry-status')}}/" +registry_id;
        url2="{{url('registry-edit')}}/" +registry_id;
        url3="{{url('registry-thankyou')}}/" +registry_id;
        $(".registry-status").attr('href',url);
        $(".registry-edit").attr('href',url2);
        $(".registry-thankyou").attr('href',url3);
        location.href = url2;
    })
</script>
@endsection
