@extends('layouts.app')
@section('content')
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    @if(isset($details))
                        <h1 class="text-uppercase">{{get_title_registry($_GET['unique_id'])}} </h1>
                       @else
                    <h1 class="text-uppercase">Registry Status</h1>
                        @endif

                </div>
            </div>
        </div>
    </div>
    
    <div class="container pt90 pb60 shop-container">
        
        <div class='row'>
            <div class="col-lg-3 mb40 shop-sidebar">
                   
                
            @if(isset($data))
                <div class="mb40">
                        @php $price_array= priceChk($data['product_min_price'],$data['product_max_price']);@endphp
                    {{Form::open(array('url'=>'search-registry','method'=>'GET'))}}
                    {{Form::hidden('unique_id',null)}}
                    <ul class="list-unstyled categories">
                        <h4>Price</h4>
                            <li><label>{{Form::radio('price','all',true)}}All</label></li> 
                            @foreach($price_array as $index => $val)
                            
                                <li><label>{{Form::radio('price',$index,null)}}{{$val}}</label></li>
                            @endforeach
                        <h4>Category</h4>
                        <li>
                            {{Form::select('category_id',category(),null,array('class'=>'form-control','placeholder'=>'Select Category'))}}
                        </li>
                    </ul>
                    <button type="submit" class="btn btn-sm btn-primary">Search</button>
                    {{Form::close()}}
                </div><!--/col-->
                @endif
            </div>
            
            <div class='col-lg-9'>
                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif


                    @if(isset($details))
                        <div class="title-heading1 mb40">
                            <h2>{{get_title_registry($_GET['unique_id'])}} </h2>
                        </div>
                    <div class="row">
                        @forelse($details as $data)
                        <div class="col-md-4 mb40 text-center">
                            <a href="#" class="simple-hover">
                                @if(isset($data->product->product_image))
                                <img src="{{filePath('product',$data->product->product_image->product_image)}}" alt="" class="img-fluid">
                                @else
                                    <img class="img-fluid" src="{{url('no_image.png')}}" class="img-fluid">
                                @endif
                            </a>
                            <div class="clearfix product-meta">

                                <p class="lead mb0"><a href="{{url('product-details/'.$data->product->id)}}">{{$data->product->product_name}}</a></p>
                                <div class="prod-detail">
                                    <p> Quantity: {{$data->qty}}</p>
                                    <p>Received  :  {{$data->received}}</p>
                                    <p>Price: {{$data->product->product_price}}</p>
                                    <p>Brand: {{$data->product->brand->brand_name}}</p>
                                </div>
                            </div>
                            @php $limit=$data->qty-$data->received; @endphp
                                @if($limit>=1)
                                <button class="btn btn-primary cartbtn" data-registry="{{$data->registries_id}}" data-limit={{$limit}} data-product="{{$data->product->id}}" data-toggle="modal" data-target="#addCartModal">Add To Cart</button>
                                @else
                                <button class="btn btn-warning">Completed</button>
                                @endif
                        </div><!--/col-->
                         @empty
                            <h4>No Record Found</h4>
                        @endforelse 
                       
                       
                    </div>
                    {!! $details->render() !!}
                @else
                    <h4>No Record Found</h4>
                @endif
                
                
            </div>
        </div>
    </div>
    
<!--add Product to cart modal -->
<div class="modal fade" id="addCartModal" tabindex="-1" role="dialog" aria-labelledby="addCartModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addCartModal">Add to Cart</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{Form::open(array('url'=>'insert-cart','id'=>'add-cart-form','class'=>'form-horizontal reg-form'))}}
        {{Form::hidden('product_id',null,array('class'=>'product_id'))}}
        {{Form::hidden('registry_id',null,array('class'=>'registry_id'))}}
        
        <div class="form-group">
            <div class="row">
                <div class="col-sm-4">
                    {{Form::label('quantity','Quantity',array('class'=>'control-label'))}}
                </div>
                <div class="col-sm-2">
                    {{Form::number('quantity',1,array('min'=>1,'class'=>'form-control quantity','placeholder'=>'Select'))}}
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      {{Form::close()}}
    </div>
  </div>
</div>

<!-- End Modal -->
<script>
$(document).on('click','.cartbtn',function(){
    var product_id=$(this).data("product");
    var registry_id=$(this).data("registry");
    var limit_val=$(this).data("limit");
    $(".quantity").attr('max',limit_val);
    $(".quantity").val(1);
    $(".registry_id").val(registry_id);
    $(".product_id").val(product_id);
});
$('#add-cart-form').validate({
        rules:{
            qty:{
                required:true,
                number:true,
            }
        }
    });

</script>

@endsection
