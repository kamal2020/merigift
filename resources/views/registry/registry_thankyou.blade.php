@extends('layouts.app')
@section('content')

    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Registry Thanks you</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        @php $id= Request::segment(2) or ''; @endphp
        @include('layouts.sidebar')
            <div class='col-lg-9'>
                <div class="row ">
                    <div class="dash-filter-div">
                        {{Form::open(array('url'=>url('thankyou-mail'),'id'=>'',"class"=>"form-inline"))}}
                            <label class="mr-sm-2" for="inlineFormCustomSelect">Registry: </label>
                            {{Form::select('registry_id',$registry,$id,array('id'=>"inlineFormCustomSelect",'class'=>'registry_id custom-select mb-2 mr-sm-2 mb-sm-0'))}}
                            
                        
                        <ul id="progress">
                            <li ><a href="{{url('registry-summery',$id)}}">Summery</a></li>
                            <li ><a href="#" class="registry-status">Status</a></li>
                            <li ><a href="#" class="registry-edit">Edit</a></li>
                            <li class="active" ><a class="registry-thankyou" href="">Thank You</a></li>
                        </ul>

                    </div>
                    <div class="select-letter">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-left">
                            <label class="mr-sm-2" for="inlineFormCustomSelect">Select Your Thankyou Letter: </label>
                            {{Form::select('title',message(),null,array("class"=>"custom-select mb-2 mr-sm-2 mb-sm-0",'placeholder'=>'select',"id"=>"inlineFormCustomSelect"))}}
                            <!-- <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect">
                                <option selected="">Choose...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                            {{Form::select('title',message(),null)}} -->
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pull-right">
                            <button type="button" class="btn btn-outline-primary mb5 pull-right own-msg">Create Your Own Letter</button>
                        </div>
                    </div>
                    <div class="letter">
                        <textarea id="summernote" name="editordata"></textarea>
                        <table id="example" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th><input type="checkbox" class="chk-all"></th>
                                <th>Name</th>
                                <th>Items</th>
                                <th>Quantity</th>
                                <th>Total Amount Contributed</th>
                                <th>Email</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order_product as $value)
                            <tr>
                                <td><input class="chk-one" type="checkbox" name="user_id[]" value="{{$value->user->id}}"></td>
                                <td>{{$value->user->name}}</td>
                                <td>{{$value->product->product_name}}</td>
                                <td>{{$value->quantity}}</td>
                                <td>{{$value->product->product_price * $value->quantity}}</td>
                                <td>{{$value->user->email}}</td>
                                <td>Completed</td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                        @if(count($order_product)>0)
                            <button type="submit" class="btn btn-success btn-lg btn-block mb5">Send</button>
                        @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
        var registry_id=$(".registry_id").val();
        var url="{{url('registry-status')}}/" +registry_id;
        var url2="{{url('registry-edit')}}/" +registry_id;
        var url3="{{url('registry-thankyou')}}/" +registry_id;
        $(".registry-status").attr('href',url);
        $(".registry-edit").attr('href',url2);
        $(".registry-thankyou").attr('href',url3);
    });
    $(document).on('change','.registry_id',function(){
        registry_id=$(".registry_id").val();
        url="{{url('registry-status')}}/" +registry_id;
        url2="{{url('registry-edit')}}/" +registry_id;
        url3="{{url('registry-thankyou')}}/" +registry_id;
        $(".registry-status").attr('href',url);
        $(".registry-edit").attr('href',url2);
        $(".registry-thankyou").attr('href',url3);
        location.href = url3;
    });
    $(document).on('click','.chk-all',function(){
        if ($(this).prop('checked')==true)
            $(".chk-one").prop('checked',true);
        else
            $(".chk-one").prop('checked',false);
    });
    $(document).on("change",'#inlineFormCustomSelect',function(){
        if($(this).val()!='')
            $('#summernote').summernote('code',$(this).val());
        else  
            $('#summernote').summernote('code','');  
    });
    $(document).on('click','.own-msg',function(){
        $('#summernote').summernote('code','');  
    });
</script>
@endsection
