@extends('layouts.app')

@section('content')
    <div class="bg-parallax parallax-overlay accounts-page"  data-jarallax='{"speed": 0.2}' style='background-image: url("{{ URL::asset('front_asset/images/shop/bg1.jpg') }}")'>
        <div class="container">
            <div class="row pb30">
                <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
                    <h3 class="text-white text-center mb30">Login to continue</h3>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                    <form method="POST" name="log_frm" id="log_frm" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group">

                            <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus >
                            <span class="error" id="error-email"></span>
                        </div>
                        <div class="form-group">

                            <input id="password" type="password" class="form-control" placeholder="Password" name="password" required>
                            <span class="error" id="error-password"></span>

                        </div>
                        <div class="form-group">
                            <button type="submit" name="login" id="login" class="btn btn-rounded btn-primary btn-block">Sign In</button>
                        </div>
                        <div class="text-center"><a href="#" class="btn btn-link btn-block">Having trouble logging in?</a></div>
                        <hr>
                        <div>
                            <a href="{{ route('register') }}" class="btn btn-white-outline btn-block">Sign Up</a>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 mb20">
                    <h3 class="text-white text-left mb30">Sign Up with social</h3>
                    <div class="clearfix">
                        <a href="{{url('/redirect')}}" class="social-icon-lg si-dark si-colored-facebook">
                            <i class="fa fa-facebook"></i>
                            <i class="fa fa-facebook"></i>
                        </a>
                        {{--<a href="#" class="social-icon-lg  si-dark si-colored-twitter">--}}
                            {{--<i class="fa fa-twitter"></i>--}}
                            {{--<i class="fa fa-twitter"></i>--}}
                        {{--</a>--}}
                        <a href="{{url('/google-redirect')}}" class="social-icon-lg si-dark si-colored-google-plus">
                            <i class="fa fa-google-plus"></i>
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $( document ).ready(function() {
        $('#log_frm').submit(function (){
        var validation=true;
        var email=$.trim($('#email').val());
        var password=$.trim($('#password').val());
        $('.error').html('');

        if(!email)
        {
           $('#error-email').html('Please Enter Email');
           validation=false;
        }
        else
        {
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (!filter.test(email)) {
                $('#error-email').html('Please Enter Valid Email');
                validation=false;
            }
        }
        if(!password)
        {
                $('#error-password').html('Please Enter Password');
                validation=false;
        }

        if(validation==true)
        { return true; } else { return false; }

        });
    });
</script>
@endsection
