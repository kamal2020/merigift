@extends('layouts.app')

@section('content')
    <div class="bg-parallax parallax-overlay accounts-page"  data-jarallax='{"speed": 0.2}' style='background-image: url("{{ URL::asset('front_asset/images/shop/bg1.jpg') }} ")'>
        <div class="container">
            <div class="row pb30 align-items-end">
                <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
                    <h3 class="text-white text-left mb30">Join the world of Meri Gift.</h3>
                    <form method="POST" name="reg_frm" id="reg_frm" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="First Name"  autofocus>
                            <span class="error" id="error-first_name"></span>
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name"  autofocus>
                            <span class="error" id="error-last_name"></span>
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" >
                            <span class="error" id="error-email"></span>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" >
                            <span class="error" id="error-password"></span>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" >
                                <span class="error" id="error-password-confirm"></span>
                        </div>
                        <div class="form-group">
                            <input id="mobile" type="mobile" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="Mobile" >
                            <span class="error" id="error-mobile"></span>
                            @if ($errors->has('mobile'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" name="register" id="register" class="btn btn-rounded btn-primary btn-block">Sign Up</button>
                        </div>
                        <div class="text-center text-white-gray">Already have an account? SignIn below</div>
                        <hr>
                        <div>
                            <a href="{{ route('login') }}" class="btn btn-white-outline btn-block">Sign In</a>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 mb20">
                    <h3 class="text-white text-left mb30">Sign Up with social</h3>
                    <div class="clearfix">
                        <a href="{{url('/redirect')}}" class="social-icon-lg si-dark si-colored-facebook">
                            <i class="fa fa-facebook"></i>
                            <i class="fa fa-facebook"></i>
                        </a>
                        {{--<a href="#" class="social-icon-lg  si-dark si-colored-twitter">--}}
                            {{--<i class="fa fa-twitter"></i>--}}
                            {{--<i class="fa fa-twitter"></i>--}}
                        {{--</a>--}}
                        <a href="{{url('/google-redirect')}}" class="social-icon-lg si-dark si-colored-google-plus">
                            <i class="fa fa-google-plus"></i>
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $( document ).ready(function() {
            $('#reg_frm').submit(function (){
                var validation=true;
                var first_name=$.trim($('#first_name').val());
                var mobile=$.trim($('#mobile').val());
                var email=$.trim($('#email').val());
                var password=$.trim($('#password').val());
                var password_confirm=$.trim($('#password-confirm').val());
                $('.error').html('');

                if(mobile==''){
                    $("#error-mobile").append("Please Enter Mobile Number");
                    validation='false';
                }
                if (first_name=='')
                {
                    $("#error-first_name").append("Please Enter Your First Name");
                    validation='false';
                }
                else
                {

                    if(/^[a-zA-Z . ]*$/.test(first_name) == false) {
                        $("#error-first_name").append("Accepts characters only");
                        validation='false';

                    }
                    if(first_name.length < 2 || first_name.length > 60){
                        $("#error-name").html("Accepts 2-60 characters.");
                        validation='false';
                    }
                }



                if(!email)
                {
                    $('#error-email').html('Please Enter Email');
                    validation=false;
                }
                else
                {
                    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                    if (!filter.test(email)) {
                        $('#error-email').html('Please Enter Valid Email');
                        validation=false;
                    }
                }
                if(!password)
                {
                    $('#error-password').html('Please Enter Password');
                    validation=false;
                }
                else
                {
                    if(password.length<6 || password.length>15)
                    {
                        $('#error-password').html('Accepts min-max 6-15');
                        validation=false;
                    }

                        if(!password_confirm)
                        {
                            $('#error-password-confirm').html('Please Enter Confirm Password');
                            validation=false;
                        }
                        else
                        {
                            if(password!=password_confirm)
                            {
                                $('#error-password-confirm').html('Confirm Password Not Match');
                                validation=false;
                            }
                        }


                }
                if(!password_confirm)
                {
                    $('#error-password-confirm').html('Please Enter Confirm Password');
                    validation=false;
                }

                if(validation==true)
                { return true; } else { return false; }

            });

            $('#email').onblur(function(){

            });

        });
    </script>
@endsection
