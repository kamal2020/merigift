<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'MeriGift') }}</title>
    <!-- Plugins CSS -->
    <link href="{{ URL::asset('front_asset/css/plugins/plugins.css') }}" rel="stylesheet">
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('front_asset/revolution/css/settings.css') }}">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('front_asset/revolution/css/layers.css') }}">
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('front_asset/revolution/css/navigation.css') }}">

    <link href="{{ URL::asset('front_asset/css/style.css') }}" rel="stylesheet">
</head>

<body>
<div id="preloader">
    <div id="preloader-inner"></div>
</div><!--/preloader-->

<!--top bar-->
<div class='top-bar clearfix light'>
    <div class='container'>
        <div class='float-sm-right'>
            <ul class='list-inline mb0'>
                <li class='list-inline-item'>
                    <a href="#" class="social-icon-sm si-gray si-gray-round si-facebook">
                        <i class="fa fa-facebook"></i>
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>

                <li class='list-inline-item'>
                    <a href="#" class="social-icon-sm si-gray si-gray-round si-twitter">
                        <i class="fa fa-twitter"></i>
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li class='list-inline-item'>
                    <a href="#" class="social-icon-sm si-gray si-gray-round si-instagram">
                        <i class="fa fa-instagram"></i>
                        <i class="fa fa-instagram"></i>
                    </a>
                </li>
                <li class='list-inline-item'>
                    <a href="#" class="social-icon-sm si-gray si-gray-round si-g-plus">
                        <i class="fa fa-google-plus"></i>
                        <i class="fa fa-google-plus"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class='float-sm-left'>
            <ul class='list-inline mb0 links'>
                    @guest
                    <li class='list-inline-item'><a href="{{ route('login') }}">Login</a></li>
                    <li class='list-inline-item'><a href="{{ route('register') }}">Register</a></li>
                    @else

                        <li class='list-inline-item'>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
                <li class='list-inline-item'><a href='#'>What is MeriGift</a></li>
                <li class='list-inline-item'><a href='#'>Create/Find Gift list</a></li>


            </ul>
        </div>
    </div>
</div>
<!--end top bar-->
<!-- Pushy Menu -->
<aside class="pushy pushy-right">
    <div class="cart-content">
        <div class="clearfix">
            <a href="javascript:void(0)" class="pushy-link text-white-gray">Close</a>
        </div>
        <ul class="list-unstyled">
            <li class="clearfix">
                <a href="#" class="float-left">
                    <img src="images/shop/p1.jpg" class="img-fluid" alt="" width="60">
                </a>
                <div class="oHidden">
                    <span class="close"><i class="ti-close"></i></span>
                    <h4><a href="#">Men's Special Watch</a></h4>
                    <p class="text-white-gray"><strong>$299.00</strong> x 1</p>
                </div>
            </li><!--/cart item-->
            <li class="clearfix">
                <a href="#" class="float-left">
                    <img src="images/shop/p2.jpg" class="img-fluid" alt="" width="60">
                </a>
                <div class="oHidden">
                    <span class="close"><i class="ti-close"></i></span>
                    <h4><a href="#">Men's tour beg</a></h4>
                    <p class="text-white-gray"><strong>$99.00</strong> x 1</p>
                </div>
            </li><!--/cart item-->
            <li class="clearfix">
                <a href="#" class="float-left">
                    <img src="images/shop/p3.jpg" class="img-fluid" alt="" width="60">
                </a>
                <div class="oHidden">
                    <span class="close"><i class="ti-close"></i></span>
                    <h4><a href="#">Women's T-shirts</a></h4>
                    <p class="text-white-gray"><strong>$199.00</strong> x 1</p>
                </div>
            </li><!--/cart item-->
            <li class="clearfix">

                <div class="float-right">
                    <a href="#" class="btn btn-primary">Checkout</a>
                </div>
                <h4  class="text-white">
                    <small>Subtotal - </small> $49.99
                </h4>
            </li><!--/cart item-->
        </ul>
    </div>
</aside>
<!-- Site Overlay -->
<div class="site-overlay"></div>

<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
    <div class="search-inline">
        <form>
            <input type="text" class="form-control" placeholder="Type and hit enter...">
            <button type="submit"><i class="ti-search"></i></button>
            <a href="javascript:void(0)" class="search-close"><i class="ti-close"></i></a>
        </form>
    </div><!--/search form-->
    <div class="container">

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt=""></a>
        <div  id="navbarNavDropdown" class="navbar-collapse collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active ">
                    <a class="nav-link" aria-haspopup="true" aria-expanded="false" href="#">Home</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link " data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                        Products
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                        Ecards
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Electronics & IT</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Home & lifestyle</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        Travel & Stay
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        Fashion & Beauty
                    </a>
                </li>


            </ul>
        </div>
        <div class=" navbar-right-elements">
            <ul class="list-inline">
                <li class="list-inline-item"><a href="javascript:void(0)" class="search-open"><i class="ti-search"></i></a></li>
                <li class="list-inline-item"><a href="javascript:void(0)" class=" menu-btn"><i class="ti-shopping-cart"></i> <span class="badge badge-default">3</span></a></li>
            </ul>
        </div><!--right nav icons-->

    </div>
</nav>

@yield('content')



<footer class="footer footer-standard pt50 pb10">
    <div class="container">
        <div class="row">

            <div class="col-lg-2 col-md-2 mb40">

                <ul class="list-unstyled footer-list-item">
                    <li>
                        <a href="#">
                            About MeriGift
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Our Policies
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Our team
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Careers
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Contact Us
                        </a>
                    </li>

                </ul>
            </div>
            <div class="col-lg-2 col-md-2 mb40">

                <ul class="list-unstyled footer-list-item">
                    <li>
                        <a href="#">
                            Terms & Conditions
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Privacy Policy
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Return & Exchanges
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Delivery Options
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            FAQ's
                        </a>
                    </li>

                </ul>
            </div>

            <div class="col-lg-6 col-md-6 mb40">
                <h3>Subscribe to our newsletter</h3>
                <form id="widget-subscribe-form" action="http://bootstraplovers.com/bootstrap4/assan-bootstrap4/default-template/html/index.html" role="form" method="post" class="mb0">
                    <div class="input-group input-group-lg">

                        <input type="email" name="widget-subscribe-form-email" class="form-control required" placeholder="Enter your Email">
                        <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">Subscribe</button>
                                </span>
                    </div>
                </form>
                <div class="row pt30">
                    <div class="col-sm-6">
                        <p class="contact-lead">
                            <small>Call us</small>
                            <br>+01 123-493-2949
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <p class="contact-lead">
                            <small>Mail us</small>
                            <br>contact@merigift.com
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="footer-bottomAlt">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="clearfix">
                    <a href="#" class="social-icon-sm si-dark si-facebook si-dark-round">
                        <i class="fa fa-facebook"></i>
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="social-icon-sm si-dark si-twitter si-dark-round">
                        <i class="fa fa-twitter"></i>
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="#" class="social-icon-sm si-dark si-g-plus si-dark-round">
                        <i class="fa fa-google-plus"></i>
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a href="#" class="social-icon-sm si-dark si-skype si-dark-round">
                        <i class="fa fa-skype"></i>
                        <i class="fa fa-skype"></i>
                    </a>
                    <a href="#" class="social-icon-sm si-dark si-linkedin si-dark-round">
                        <i class="fa fa-linkedin"></i>
                        <i class="fa fa-linkedin"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-5">
                <span>© Copyright 2017. All Right Reserved</span>
            </div>
        </div>
    </div>
</div>
<!--back to top-->
<a href="#" class="back-to-top hidden-xs-down" id="back-to-top"><i class="ti-angle-up"></i></a>
<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="{{ URL::asset('front_asset/js/plugins/plugins.js') }}"></script>
<script src="{{ URL::asset('front_asset/js/assan.custom.js') }}"></script>
<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="{{ URL::asset('front_asset/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('front_asset/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{ URL::asset('front_asset/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('front_asset/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('front_asset/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('front_asset/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('front_asset/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('front_asset/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('front_asset/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('front_asset/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('front_asset/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
<script>
    /**Hero  script**/
    var tpj = jQuery;

    var revapi1078;
    tpj(document).ready(function () {
        if (tpj("#rev_slider_1078_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_1078_1");
        } else {
            revapi1078 = tpj("#rev_slider_1078_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "revolution/js/",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 8000,
                navigation: {
                    arrows: {
                        enable: true,
                        style: 'uranus',
                        tmp: '',
                        rtl: false,
                        hide_onleave: false,
                        hide_onmobile: true,
                        hide_under: 600,
                        hide_over: 9999,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        left: {
                            container: 'slider',
                            h_align: 'left',
                            v_align: 'center',
                            h_offset: 0,
                            v_offset: 0
                        },
                        right: {
                            container: 'slider',
                            h_align: 'right',
                            v_align: 'center',
                            h_offset: 0,
                            v_offset: 0
                        }
                    }
                },
                viewPort: {
                    enable: true,
                    outof: "pause",
                    visible_area: "80%",
                    presize: false
                },
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: [1140, 992, 700, 465],
                gridheight: [600, 600, 500, 480],
                lazyType: "none",
                parallax: {
                    type: "mouse",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, 46, 47, 48, 49, 50, 55]
                },
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false
                }
            });
        }
    });	/*ready*/

</script>
</body>

<!-- Mirrored from bootstraplovers.com/bootstrap4/assan-bootstrap4/default-template/html/shop-home.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 17 Jul 2017 15:17:25 GMT -->
</html>
