@extends('layouts.app')
@section('content')
<div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Guest List</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
            @include('layouts.sidebar')
            <div class='col-lg-9'>
                <div class="title-heading1 mb40"><h4>Guest List</h4></div>
                <form class="form-inline pull-left">
                    <label class="mr-sm-2" for="inlineFormCustomSelect">Registry: </label>
                    @php $reg_id=Request::segment(2) or '';@endphp
                    {{Form::select('registry_id',$registry,$reg_id,array('id'=>"registry_id",'class'=>'form-control'))}}
                </form>

                <div class="pull-right">
                    <a href="{{url('/add-guest-list')}}" class="btn btn-primary mb5">
                        <i class="fa fa-plus"></i> <span>Add Single Guest</span>
                    </a>
                </div>
                <div class="pull-right mr-10">
                    <a href="{{url('/guest-upload')}}" class="btn btn-primary mb5">
                        <i class="fa fa-plus"></i> <span>Bulk Guest Upload</span>
                    </a>
                </div>
                <table class="table" id="users-table">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Registry</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>No of people</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody class="">
                    @php $i=1; @endphp
                    @foreach($quest_list as $data)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$data->registry->name}}</td>
                            <td>{{$data->guest_name}}</td>
                            <td>{{$data->email}}</td>
                            <td>{{$data->phone}}</td>
                            <td>{{$data->address}}</td>
                            <td>{{$data->no_of_people}}</td>
                            <td>
                                <a href="{{url('edit-guestlist',$data->id)}}"><i class="fa fa-edit"></i> </a>
                                <a href="{{url('delete_guestlist',$data->id)}}"><i class="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            var reg_id="{{Request::segment(2)}}";
            if(reg_id==''){
                var url="{{url('guest-list')}}";
                var id = $("#registry_id").val();
                window.location.replace(url+"/"+id);
            }

        });
        $("#registry_id").change(function(){
            var url="{{url('guest-list')}}";
            var id = $(this).val();
            window.location.replace(url+"/"+id);
        });
    </script>
    <script>

    </script>



@endsection
