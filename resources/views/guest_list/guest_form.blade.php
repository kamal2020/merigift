@extends('layouts.app')
@section('content')
<div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Add Guest List</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        @include('layouts.sidebar')
            <div class='col-lg-9'>

                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                @if(isset($questList))
                        <div class="title-heading1 mb40"><h4>Update Guest</h4></div>
                    {{Form::model($questList,array('url'=>array('update_guestlist',$questList->id),'id'=>'guest-form','class'=>'form-horizontal reg-form'))}}
                @else
                        <div class="title-heading1 mb40"><h4>Add Guest</h4></div>
                    {{Form::open(array('url'=>'insert_guestlist','id'=>'guest-form','class'=>'form-horizontal reg-form'))}}
                @endif
                 <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    {{Form::label('registry_id','Select Registry',array('class'=>'col-sm-4 control-label'))}}
                    <div class="col-sm-12">
                        {{Form::select('registry_id',$registry,null,array('class'=>'form-control','placeholder'=>'Select Registry'))}}
                    </div>
                </div>
                 <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    {{Form::label('name','Guest Name',array('class'=>'col-sm-4 control-label'))}}
                    <div class="col-sm-12">
                        {{Form::text('guest_name',null,array('class'=>'form-control','placeholder'=>'Enter Name'))}}
                    </div>
                </div>
                     <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('email','Email',array('class'=>'col-sm-2 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::text('email',null,array('class'=>'form-control','placeholder'=>'Enter Email'))}}
                        </div>
                    </div>
                     <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('phone','Phone',array('class'=>'col-sm-2 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::text('phone',null,array('class'=>'form-control','placeholder'=>'Enter Phone Number'))}}
                        </div>
                    </div>
                     <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('address','Address',array('class'=>'col-sm-4 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::text('address',null,array('class'=>'form-control','placeholder'=>'Enter Address'))}}
                        </div>
                    </div>
                     <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                        {{Form::label('no_of_people','No of People',array('class'=>'col-sm-4 control-label'))}}
                        <div class="col-sm-12">
                            {{Form::number('no_of_people',null,array('class'=>'form-control','placeholder'=>'Enter no of people'))}}
                        </div>
                    </div>
                    <div class="pull-right">

                    <button type="submit" class="btn btn-info ">Submit</button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
    <script>
        $("#guest-form").validate({
            rules: {
                registry_id: "required",
                guest_name: "required",
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true,
                    number: true
                },
                address: "required",
                no_of_people: {
                    required: true,
                    number: true
                },

                    },
            messages:{
                registry_id:{
                    required:"Please Select Registry",
                },
                guest_name:{
                    required:"Please Enter Guest Name",
                },
                email:{
                    required:"Please Enter Guest Email",
                    email:"Please enter a valid email address",
                },
                phone:{
                    required:"Please Enter Guest Phone Number",
                    number:"Please Enter Number Only",
                },
                address:{
                    required:"Please Enter Guest Address",
                },
                no_of_people:{
                    required:"Please Enter No of People",
                    number:"Please Enter Number Only",
                },

            }
        });
    </script>
@endsection
