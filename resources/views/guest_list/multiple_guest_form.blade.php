@extends('layouts.app')
@section('content')
<div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Bulk Guest Upload</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        @include('layouts.sidebar')
            <div class='col-lg-9'>
                <div class="">
                    <a href="{{url('/download-sample')}}" class="btn btn-primary mb5">
                        <i class="fa fa-plus"></i> <span>Download Sample</span>
                    </a>
                </div>

                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if(Session::get('data'))
                    <div class="alert alert-danger alert-block">
                        <h5>These records not inserted,please check ! Try Again</h5>
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        @foreach (Session::get('data') as $error)
                            <li>{{$error or ''}}</li>
                        @endforeach
                    </div>
                @endif
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach


                    {{Form::open(array('url'=>'insert-guest-upload', 'id'=>'guest_upload','files'=>true,'class'=>'form-horizontal reg-form'))}}
                <div class="title-heading1 mb40"><h4>Bulk Upload</h4></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                    {{Form::label('registry_id','Select Registry',array('class'=>'col-sm-4 control-label'))}}
                    <div class="col-sm-10">
                        {{Form::select('registry_id',$registry,null,array('class'=>'form-control','placeholder'=>'Select Registry'))}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                    {{Form::label('file','Excel File',array('class'=>'col-sm-4 control-label'))}}
                    <div class="col-sm-10">
                        {{Form::file('import_file',null)}}
                    </div>
                </div>
                    <div class="pull-right">

                    <button type="submit" class="btn btn-info ">Submit</button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
    <script>
        $("#guest_upload").validate({
            rules: {
                registry_id: "required",
                import_file: "required",
                  },
            messages:{
                registry_id:{
                    required:"Please Select Registry",
                },
                import_file:{
                    required:"Please Select File",
                }

            }
        });
    </script>
@endsection
