@extends('layouts.app')
@section('content')
    
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Plan Detail</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
            <div class='col-lg-12'>
                @if(isset($plan))
                    {{Form::open(array('url'=>'buy-plan'))}}
                        <table>
                            <tr>
                                <th>Plan</th>
                                <td>{{$plan->title}}</td>
                            </tr>
                            <tr>
                                <th>Plan Type</th>
                                <td>{{($plan_type=='oneyear') ? 'One Year' : 'Life Time'}}</td>
                            </tr>
                            @if(isset($setting))
                                <tr >
                                    <th width="200px">Referal Discount (%)</th>
                                    <td>{{$setting->referal_discount}}</td>
                                </tr>
                            @endif
                            <tr>
                                <th>Discount Amount</th>
                                @php $discount=0; @endphp
                                @if($plan->$plan_type>0 && isset($setting))
                                    
                                    @php 
                                        $discount=($plan->$plan_type * $setting->referal_discount)/100;
                                    @endphp
                                @endif
                                <td>{{$discount}}</td>
                            </tr>
                            <tr>
                                <th>Paid Amount</th>
                                @php $paid_amount= $plan->$plan_type - $discount; @endphp
                                <td>{{$paid_amount}}</td>
                            </tr>
                            <tr>
                                <th>Amount</th>
                                <td>{{$plan->$plan_type}}</td>
                            </tr>
                        </table>
                        {{Form::hidden('plan_id',$plan->id)}}
                        {{Form::hidden('plan_type',$plan_type)}}
                        {{Form::hidden('amount',$plan->$plan_type)}}
                        {{Form::hidden('discount',$discount)}}
                        {{Form::hidden('paid_amount',$paid_amount)}}
                        @if(isset($user))
                            {{Form::hidden('referal_user_id',$user->id)}}   
                        @endif
                        <button type="submit" class="btn pull-right btn-info">Buy</button>
                    {{Form::close()}}
                @endif
            </div>
        </div>
    </div>
<script>

</script>
@endsection
