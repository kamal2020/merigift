@extends('layouts.app')
@section('content')


 <div class="container pt90 pb60">
            <div class='row align-items-center'>

                <div class='col-md-7'>
                    @if($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                    @if($message = Session::get('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif

                    <div id="js-grid-slider-thumbnail" class="cbp">
                        @foreach($details->product_image_all as $img)
                        <div class="cbp-item">
                            <div class="cbp-caption">
                                <div class="cbp-caption-defaultWrap">
                                    <img src="{{ filePath('product',$img->product_image) }}" alt="">
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div id="js-pagination-slider">
                        @foreach($details->product_image_all as $img)
                        <div class="cbp-pagination-item cbp-pagination-active">
                            <img src="{{ filePath('product',$img->product_image) }}" alt="">
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class='col-md-5'>

                    <h2>{{$details->product_name}}</h2>
                    <div class='clearfix mb30'>
                        {{--<span class='float-right'>--}}
                            {{--<i class='fa fa-star text-warning'></i>--}}
                            {{--<i class='fa fa-star text-warning'></i>--}}
                            {{--<i class='fa fa-star text-warning'></i>--}}
                            {{--<i class='fa fa-star text-warning'></i>--}}
                            {{--<i class='fa fa-star-half-full text-warning'></i>--}}
                            {{--(12 Reviews)--}}
                        {{--</span>--}}
                        <h4 class='text-info'>$ {{$details->product_price}}</h4>
                    </div>
                    <p>
                        {{$details->product_description}}
                    </p>
                    <form method="post" class="pt20">
                        <h4><small>Quantity</small></h4>
                        <div class="quantity">
                            <input type="button" class="minus btn-number" value="-" disabled="disabled" data-type="minus" data-field="quantity[1]">
                            <input type="text" class="input-text qty text input-number" value="1" min="1" max="10" title="Qty"  name="quantity[1]" >
                            <input type="button" class="plus btn-number" value="+" data-type="plus" data-field="quantity[1]">
                        </div>
                        @if(Auth::user())
                        <button type="button" class="btn btn-primary btn-icon registrybtn" data-product="{{$details->id}}" data-toggle="modal" data-target="#addRegistryModal">Add to Registry</button>
                        @else
                            <a href="{{url('login')}}" class="btn btn-success">Add To Registry</A>
                        @endif
                    </form>
                </div>
            </div>
            <div class="row pt70">
                <div class="col-md-10 offset-md-1">
                    <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-default justify-content-center mb30" role="tablist">
                            <li class="nav-item" role="presentation"><a class="active nav-link" href="#t3" aria-controls="#t3" role="tab" data-toggle="tab"> Description</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href="#t4" aria-controls="#t4" role="tab" data-toggle="tab"> Additional Information</a></li>
                            {{--<li class="nav-item" role="presentation"><a class="nav-link" href="#t5" aria-controls="#t5" role="tab" data-toggle="tab"> Feedback <sup>2</sup></a></li>--}}
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active show fade" id="t3">
                                {{$details->product_description}}
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="t4">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr><th>
                                                Brand:
                                            </th>
                                            <td>
                                                {{$details->brand->brand_name}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                Category
                                            </th>
                                            <td>
                                                {{$details->category->cat_name}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                Sub Category
                                            </th>
                                            <td>
                                                {{$details->sub_category->sub_cat_name}}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table> 
                            </div>    
                            <div role="tabpanel" class="tab-pane show fade" id="t5">
                                <div class="media mb40">
                                    <i class="d-flex mr-3 fa fa-user-circle-o fa-3x"></i>
                                    <div class="media-body">
                                        <h5 class="mt-0 clearfix">
                                            <span class="float-right">
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star-half-full  text-warning"></i>
                                            </span>
                                            Jane Doe</h5>
                                        Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </div>
                                </div>
                                <div class="media mb40">
                                    <i class="d-flex mr-3 fa fa-user-circle-o fa-3x"></i>
                                    <div class="media-body">
                                        <h5 class="mt-0 clearfix">
                                            <span class="float-right">
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star text-warning"></i>
                                                <i class="fa fa-star-half-full  text-warning"></i>
                                            </span>
                                            Jane Doe</h5>
                                        Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    </div>
                                </div><!--/review-->
                                <hr class="mb40">
                                <div class="title-heading1">
                                    <h3>Add Feedback</h3>
                                </div>
                                <form method="post" action="#">
                                    <div class="row">
                                        <div class="col-sm-12">                                 
                                            <div class=" mb20">
                                                <input type="text" name="name" class="form-control" placeholder="Full Name....">
                                            </div>
                                            <div class=" mb20">
                                                <input type="text" name="email" class="form-control" placeholder="Email Address....">
                                            </div>                          
                                        </div>
                                        <div class="col-sm-12">
                                            <textarea name="message" class="form-control mb20" rows="5" placeholder="Message...."></textarea>                                  
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 text-right">
                                            <button type="submit" name="submit" class="btn btn-lg btn-primary  mb20">Add Review</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <div class="modal fade" id="addRegistryModal" tabindex="-1" role="dialog" aria-labelledby="addRegistryModal" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title" id="addRegistryModal">Add to Registry</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
             </div>
             <div class="modal-body">
                 {{Form::open(array('url'=>'add-registry-item','id'=>'registry-form','class'=>'form-horizantal'))}}
                 {{Form::hidden('product_id',null,array('class'=>'product_id'))}}
                 {{Form::hidden('qty',null,array('class'=>'qty'))}}
                 {{Form::hidden('received','0',array('class'=>'received'))}}
                 <div class="form-group">
                     <div class="row">
                         <div class="col-sm-2">
                             {{Form::label('name','Registry',array('class'=>'control-label'))}}
                         </div>
                         <div class="col-sm-10">
                             {{Form::select('registries_id',isset($registry) ? $registry : [],null,array('class'=>'form-control','placeholder'=>'Select','required'=>true))}}
                         </div>
                     </div>

                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                 <button type="submit" class="btn btn-primary">Save changes</button>
             </div>
             {{Form::close()}}
         </div>
     </div>
 </div>
 <script>
     $(document).on('click','.registrybtn',function(){
         var product_id=$(this).data("product");
         var qty=$('.input-number').val();

         $(".product_id").val(product_id);
         $(".qty").val(qty);
     });
 </script>
<script>
    $(document).ready(function () {
        $('.btn-number').click(function(e){
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type      = $(this).attr('data-type');
            var input = $("input[name='"+fieldName+"']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if(type == 'minus') {

                    if(currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if(parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if(type == 'plus') {

                    if(currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if(parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });
        $('.input-number').focusin(function(){
            $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function() {

            minValue =  parseInt($(this).attr('min'));
            maxValue =  parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            name = $(this).attr('name');
            if(valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if(valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }


        });
        $(".input-number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

    })
</script>

@endsection
