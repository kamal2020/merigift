@extends('layouts.app')
@section('content')
    <div class="container pt90 pb60">
        <div class='row'>
        <div class="col-lg-3 mb40 shop-sidebar">
                <div class="mb40">
                @php $price_array= priceChk($product_min_price,$product_max_price);@endphp
                    {{Form::open(array('url'=>url('product-list'),'method'=>'GET'))}}
                    {{Form::hidden('id',null)}}
                    <ul class="list-unstyled categories">
                        <h4>Price</h4>
                            <li><label>{{Form::radio('price','all',true)}}All</label></li> 
                            @foreach($price_array as $index => $val)
                                <li><label>{{Form::radio('price',$index,null)}}{{$val}}</label></li>
                            @endforeach
                        <h4>Brand</h4>
                        <li>
                            {{Form::select('brand_id',brand(),null,array('class'=>'form-control','placeholder'=>'Select Brand'))}}
                        </li>
                    </ul>
                    <button type="submit" class="btn btn-sm btn-primary">Search</button>
                    {{Form::close()}}
                </div><!--/col-->
            </div>
            <div class='col-lg-9'>
                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif


            @if(isset($product))
                    <div class ="row">
                        @forelse($product as $data)
                            <div class="col-md-4 mb40 text-center">
                                <a href="#" class="simple-hover">
                                @if(isset($data->product_image))
                                                    <img class="img-fluid" src="{{ filePath('product',$data->product_image->product_image) }}" alt="Product Image" />
                                                @else
                                                    <img class="img-fluid" src="{{ url('no_image.png') }}" alt="Product Image" />
                                                @endif
                                </a>
                                <div class="clearfix product-meta">

                                    <p class="lead mb0"><a href="{{url('product-details/'.$data->id)}}" title="Occasions">
                                                    {{$data->product_name}}
                                                </a>
                                    </p>
                                    <div class="prod-detail">
                                        <p>Price: {{$data->product_price}}</p>
                                        <p>Brand: {{$data->brand->brand_name}}</p>
                                    </div>
                                </div>
                                            @if(Auth::user())
                                                <button class="btn btn-success registrybtn" data-product="{{$data->id}}" data-toggle="modal" data-target="#addRegistryModal">Add To Registry</button>
                                            @else
                                            <a href="{{url('login')}}" class="btn btn-success">Add To Registry</A>
                                            @endif
                                        </div>

                        @empty
                            <h3>No records Found </h3>
                        @endforelse
                    </div>
                    {!! $product->render() !!}
                @else
                    <h3>No records Found </h3>
                @endif
            </div>
        </div>
    </div>
<!--add registry modal -->
<div class="modal fade" id="addRegistryModal" tabindex="-1" role="dialog" aria-labelledby="addRegistryModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addRegistryModal">Add to Registry</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{Form::open(array('url'=>'add-registry-item','id'=>'registry-item-form','class'=>'form-horizantal'))}}
        {{Form::hidden('product_id',null,array('class'=>'product_id'))}}
          {{Form::hidden('received','0',array('class'=>'received'))}}
        <div class="form-group">
            <div class="row">
                <div class="col-sm-2">
                    {{Form::label('name','Registry',array('class'=>'control-label'))}}
                </div>
                <div class="col-sm-10">
                    {{Form::select('registries_id',isset($registry) ? $registry : [],null,array('class'=>'form-control','placeholder'=>'Select' ,'required'=>true))}}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    {{Form::label('quantity','Quantity',array('class'=>'control-label'))}}
                </div>
                <div class="col-sm-10">
                    {{Form::number('qty',1,array('class'=>'form-control','placeholder'=>'Select'))}}
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      {{Form::close()}}
    </div>
  </div>
</div>
<script>
$(document).on('click','.registrybtn',function(){
    var product_id=$(this).data("product");
    $(".product_id").val(product_id);
});
$('#registry-item-form').validate({
        rules:{
            registries_id:"required",
            qty:{
                required:true,
                number:true,
            }
        }
    });
</script>
@endsection
