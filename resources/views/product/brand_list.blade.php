@extends('layouts.app')
@section('content')
    <div class="container pt90 pb60">
        <div class='row'>

            <div class='col-lg-12'>
                <div class ="row">
                    @forelse($brand as $data)
                        <div class="owl-item col-xs-12 col-sm-12  col-md-3">
                            <div class="item text-xs-center">
                                <div class="catinfo">
                                    <a class="text-xs-center catinfo_image" href="#" title="Brand">
                                        <figure>
                                            <img class="img-fluid" src="{{ filePath('brands',$data->brand_image) }}" alt="Brand" />
                                        </figure>
                                    </a>
                                    <div class="catinfo_wrap">
                                        <h4 class="h5">
                                            <a href="{{url('product-brand/'.$data->id)}}" title="{{$data->brand_name}}">
                                                {{$data->brand_name}}
                                            </a>
                                        </h4>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <h3>No records Found </h3>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection
