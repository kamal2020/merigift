@extends('layouts.app')
@section('content')
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Check List</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        @include('layouts.sidebar')
            <div class='col-lg-9'>
                <div class="title-heading1 mb40"><h4>Check List</h4></div>
                <div class="pull-right">
                    <a href="{{url('/add_checklist')}}">
                        <i class="fa fa-plus"></i> <span>Add Checklist</span>
                    </a> 
                </div>

                <table class="table">
                    <tbody>
                        <tr>
                        <th style="width: 10px">#</th>
                        <th>Registry</th>
                        <th>Name</th>
                        <th>Created_at</th>
                        <th>Action</th>
                        </tr>
                        @php $i=1;@endphp
                        @foreach($checklist as $class)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$class->registry->name}}</td>
                                <td>{{$class->name}}</td>
                                <td> {{ $class->created_at }}</td>
                                <td>
                                <a class="btn btn-default btn-sm " href="{{url('task-list',$class->id)}}">Task</a>
                                <a class="btn btn-primary btn-sm " href="{{url('edit_checklist',$class->id)}}">Edit</a>
                                <a class="btn btn-danger btn-sm " href="{{url('delete_checklist',$class->id)}}">Delete</a>
                                
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
