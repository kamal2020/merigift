@extends('layouts.app')
@section('content')
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Check List</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        @include('layouts.sidebar')
            <div class='col-lg-9'>

                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                @if(isset($checklist))
                        <div class="title-heading1 mb40"><h4>Update Check List</h4></div>
                    {{Form::model($checklist,array('url'=>array('update_checklist',$checklist->id),'id'=>'checklist-form','class'=>'form-horizontal reg-form'))}}
                @else
                        <div class="title-heading1 mb40"><h4>Add Check List</h4></div>
                    {{Form::open(array('url'=>'insert_checklist','id'=>'checklist-form','class'=>'form-horizontal reg-form'))}}
                @endif
                    <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    {{Form::label('registry_id','Select Registry',array('class'=>'col-sm-12 control-label'))}}
                    <div class="col-sm-12">
                        {{Form::select('registry_id',$registry,null,array('class'=>'form-control','placeholder'=>'Select Registry'))}}
                    </div>
                </div>
                    </div>
                    <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    {{Form::label('name','Checklist Name',array('class'=>'col-sm-12 control-label'))}}
                    <div class="col-sm-12">
                        {{Form::text('name',null,array('class'=>'form-control','placeholder'=>'Enter Name'))}}
                    </div>
                </div>
                    </div>
                <div class="pull-right">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info ">Submit</button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
    <script>
        $("#checklist-form").validate({
            rules: {
                registry_id: "required",
                name: "required",
                           },
            messages:{
                registry_id:{
                    required:"Please Select Registry",
                },
                name:{
                    required:"Please Enter Checklist Name",
                },

            }
        });
    </script>
@endsection
