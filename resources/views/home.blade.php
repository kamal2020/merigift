@extends('layouts.app')
@section('content')
    
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Dashboard</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>

        @include('layouts.sidebar')

            <div class='col-lg-9'>

                <div class="row special-feature">
                    <div class="dash-filter-div">

                        <ul class="dash_filter-anchor" id="filterable-item-filter-1">
                            <li class="active"><a href="">New Items</a></li>
                            <li><a href="">New Bought Items</a></li>
                            <li><a href="" >Notifications</a></li>
                            <li><a href="" >Upcoming Tasks <sub class="notification">3</sub></a></li>
                            <li>200 Days Left</li>
                        </ul>

                    </div>

                    <div class="col-md-4 col-sm-6 margin20">
                        <a href="#">
                            <div class="s-feature-box text-center">
                                <div class="mask-top">
                                    <!-- Icon -->
                                    <i class="fa fa-gift"></i>
                                    <!-- Title -->
                                    <h4>Total Gifts</h4>
                                </div>
                                <div class="mask-bottom">
                                    <!-- Icon -->
                                    <i class="fa fa-gift"></i>
                                    <!-- Title -->
                                    <h1>30</h1>
                                    <!-- Text -->
                                </div>
                            </div>
                        </a>
                    </div><!--services col-->
                    <div class="col-md-4 col-sm-6 margin20">
                        <a href="#">
                            <div class="s-feature-box text-center">
                                <div class="mask-top">
                                    <!-- Icon -->
                                    <i class="fa fa-credit-card"></i>
                                    <!-- Title -->
                                    <h4>Gifts Bought</h4>
                                </div>
                                <div class="mask-bottom">
                                    <!-- Icon -->
                                    <i class="fa fa-credit-card"></i>
                                    <!-- Title -->
                                    <h1>6</h1>
                                    <!-- Text -->

                                </div>
                            </div>
                        </a>
                    </div><!--services col-->
                    <div class="col-md-4 col-sm-6 margin20">
                        <a href="#">
                            <div class="s-feature-box text-center">
                                <div class="mask-top">
                                    <!-- Icon -->
                                    <i class="fa fa-percent"></i>
                                    <!-- Title -->
                                    <h4>Bought</h4>
                                </div>
                                <div class="mask-bottom">
                                    <!-- Icon -->
                                    <i class="fa fa-percent"></i>
                                    <!-- Title -->
                                    <h1>20%</h1>
                                    <!-- Text -->

                                </div>
                            </div>
                        </a>
                    </div><!--services col-->
                    <div class="col-md-4 col-sm-6 margin20">
                        <a href="#">
                            <div class="s-feature-box text-center">
                                <div class="mask-top">
                                    <!-- Icon -->
                                    <i class="fa fa-bar-chart"></i>
                                    <!-- Title -->
                                    <h4>Total Value</h4>
                                </div>
                                <div class="mask-bottom">
                                    <!-- Icon -->
                                    <i class="fa fa-bar-chart"></i>
                                    <!-- Title -->
                                    <h4>1,00,000</h4>
                                    <!-- Text -->

                                </div>
                            </div>
                        </a>
                    </div><!--services col-->
                    <div class="col-md-4 col-sm-6 margin20">
                        <a href="#">
                            <div class="s-feature-box text-center">
                                <div class="mask-top">
                                    <!-- Icon -->
                                    <i class="fa fa-handshake-o"></i>
                                    <!-- Title -->
                                    <h4>Value Contributed</h4>
                                </div>
                                <div class="mask-bottom">
                                    <!-- Icon -->
                                    <i class="fa fa-handshake-o"></i>
                                    <!-- Title -->
                                    <h4>25,000</h4>
                                    <!-- Text -->

                                </div>
                            </div>
                        </a>
                    </div><!--services col-->
                    <div class="col-md-4 col-sm-6 margin20">
                        <a href="#">
                            <div class="s-feature-box text-center">
                                <div class="mask-top">
                                    <!-- Icon -->
                                    <i class="fa fa-percent"></i>
                                    <!-- Title -->
                                    <h4>Contributed</h4>
                                </div>
                                <div class="mask-bottom">
                                    <!-- Icon -->
                                    <i class="fa fa-percent"></i>
                                    <!-- Title -->
                                    <h4>25%</h4>
                                    <!-- Text -->

                                </div>
                            </div>
                        </a>
                    </div><!--services col-->
                </div>
            </div>
        </div>
    </div>
<script>

</script>
@endsection
