@extends('layouts.app')
@section('content')
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Update Profile</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
            @include('layouts.sidebar')
            <div class='col-lg-9'>

                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                {{Form::model($profile,array('url'=>array('update_profile',$profile->id),'id'=>'profile-form','class'=>'form-horizontal reg-form','enctype' => 'multipart/form-data'))}}
        <div class="box-body">
            <div class="row">
            <div class="col-sm-6">
            <div class="form-group">
                {{Form::label('name','Name',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                {{Form::text('name',null,array('class'=>'form-control','placeholder'=>'Enter Name'))}}
                </div>
            </div>
            </div>
            <div class="col-sm-6">
            <div class="form-group">
                {{Form::label('contact_number','Contact Number',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                    {{Form::text('contact_number',null,array('id'=>'contact_number','class'=>'form-control','placeholder'=>'Enter Contact Number'))}}
                </div>
            </div>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-6">

                <div class="form-group">
                {{Form::label('address_line1','Address line 1',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                    {{Form::text('address_line1',isset($profile->user_address)? $profile->user_address->address_line1 : '' ,array('id'=>'address_line1','class'=>'form-control','placeholder'=>'Enter Address Line 1'))}}
                </div>
            </div>
                </div>

                    <div class="col-sm-6">

                    <div class="form-group">
                {{Form::label('address_line2','Address line 2',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                    {{Form::text('address_line2',isset($profile->user_address)? $profile->user_address->address_line2 : '' ,array('id'=>'address_line2','class'=>'form-control','placeholder'=>'Enter Address Line 2'))}}
                </div>
            </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-sm-6">

                <div class="form-group">
                {{Form::label('town','Town',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                    {{Form::text('town',isset($profile->user_address)? $profile->user_address->town : '' ,array('id'=>'town','class'=>'form-control','placeholder'=>'Enter Town'))}}
                </div>
            </div>
                </div>

                    <div class="col-sm-6">

                    <div class="form-group">
                {{Form::label('county','County',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                    {{Form::text('county',isset($profile->user_address)? $profile->user_address->county : '' ,array('id'=>'county','class'=>'form-control','placeholder'=>'Enter County'))}}
                </div>
            </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-sm-6">

                <div class="form-group">
                {{Form::label('postcode','Postcode',array('class'=>'col-sm-2 control-label'))}}
                <div class="col-sm-12">
                    {{Form::text('postcode',isset($profile->user_address)? $profile->user_address->postcode : '' ,array('id'=>'postcode','class'=>'form-control','placeholder'=>'Enter Postcode'))}}
                </div>
            </div>
                </div>

                    <div class="col-sm-6">

                    <div class="form-group">
                {{Form::label('country','Country',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                    {{Form::text('country',isset($profile->user_address)? $profile->user_address->country : '' ,array('id'=>'country','class'=>'form-control','placeholder'=>'Enter Country'))}}
                </div>
            </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-sm-6">

                <div class="form-group">
                {{Form::label('additional_info','Additional info',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                    {{Form::text('additional_info',isset($profile->user_address)? $profile->user_address->additional_info : '' ,array('id'=>'additional_info','class'=>'form-control','placeholder'=>'Enter Additional Info'))}}
                </div>
            </div>
                </div>

                    <div class="col-sm-6">

                    <div class="form-group">
                {{Form::label('image','Image',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                    {{Form::file('image',array('class'=>'form-control'))}}
                    @if($profile->image!='')
                        <span ><img src="{{URL::asset('user_image/'.$profile->image)}}"></span>
                    @endif
                </div>
                    </div>
                    </div>

            </div>

              </div>
              <div class="box-footer pull-right">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info ">Submit</button>
              </div>
           {{Form::close()}}
            </div>
        </div>
    </div>
    <script>
        $("#profile-form").validate({
            rules: {
                name: "required",
                contact_number: "required",
                address_line1: "required",
                address_line2: "required",
                town: "required",
                county: "required",
                postcode: "required",
                country: "required",

            },
            messages:{
                name:{
                    required:"Please Enter Name",
                },

                contact_number:{
                    required:"Please Enter Contact Number",
                },
                address_line1:{
                    required:"Please Enter Address line 1",
                },
                address_line2:{
                    required:"Please Enter Address line 2",
                },
                town:{
                    required:"Please Enter Town",
                },
                county:{
                    required:"Please Enter County",
                },
                postcode:{
                    required:"Please Enter Postcode",
                },
                country:{
                    required:"Please Enter Country",
                },


            }
        });
    </script>

@endsection