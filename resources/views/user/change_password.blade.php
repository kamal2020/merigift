@extends('layouts.app')
@section('content')
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Change Password</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
            @include('layouts.sidebar')
            <div class='col-lg-9'>

                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            {{Form::open(array('url'=>'update_password','id'=>'password-form','class'=>'form-horizontal reg-form','enctype' => 'multipart/form-data'))}}
<div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                {{Form::label('old_password','Old Password',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                {{Form::password('old_password',array('class'=>'form-control','placeholder'=>'Enter Old Password'))}}
                </div>
            </div>
</div>
                    <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                {{Form::label('new_password','New Password',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                    {{Form::password('new_password',array('id'=>'new_password','class'=>'form-control','placeholder'=>'Enter New Password'))}}
                </div>
            </div>
                    </div>
                    <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                {{Form::label('confirm_password','Confirm Password',array('class'=>'col-sm-12 control-label'))}}
                <div class="col-sm-12">
                    {{Form::password('confirm_password',array('class'=>'form-control','placeholder'=>'Enter Confirm Password'))}}
                </div>
            </div>
                    </div>
              <div class="box-footer pull-right">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info ">Submit</button>
              </div>
           {{Form::close()}}
            </div>
        </div>
    </div>
<script>
    $("#password-form").validate({
        rules: {
            old_password: "required",
            new_password: "required",
            confirm_password: {
                required:true,
                equalTo: "#new_password"
            }
        },
        messages:{
            old_password:{
                required:"Please Enter Old Password",
            },
            new_password:{
                required:"Please Enter New Password",
            },
            confirm_password:{
                required:"Please Enter Confirm Password",
            },

        }
    });
</script>
@endsection