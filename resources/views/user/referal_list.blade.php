@extends('layouts.app')
@section('content')
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Referral List</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
        @include('layouts.sidebar')
            <div class='col-lg-9'>

                <div class="title-heading1 mb40"><h4>Referral List</h4></div>
                <div class="pull-right">
                    <span>Your Referral Link: <i class="fa fa-copy" title="click to copy" onclick="copyDivToClipboard()"></i></span><div id="a">{{url('/').'/referral-code/'.$referral_code}}</div>

                </div>

                <table class="table">
                    <thead>
                        <tr>
                        <th style="width: 10px">#</th>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Date</th>
                        </tr>
                    <thead>
                    <tbody>
                        @php $i=1;@endphp
                        @foreach($referral   as $class)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$class->type}}</td>
                                <td>{{$class->amount}}</td>
                                <td> {{ dateFormat($class->created_at) }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function copyDivToClipboard() {
            var range = document.getSelection().getRangeAt(0);
            range.selectNode(document.getElementById("a"));
            window.getSelection().addRange(range);
            document.execCommand("copy");
            $('.fa-copy').html(' Copy Done');
        }
    </script>
@endsection
