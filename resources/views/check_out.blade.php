@extends('layouts.app')
@section('content')

    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Check Out</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
    
            <div class='col-lg-12'>
            {{Form::open(array('url'=>'proceed-checkout','class'=>'form-horizontal reg-form'))}}

                <h4>Cart Items</h4>
                <table class="table">
                    <tbody>
                        <tr>
                        <th style="width: 10px">#</th>
                        <th>Image</th>
                        <th>Product</th>
                        <th>Qty</th>
                        <th>Price</th>
                        </tr>
                        @php $i=1; $cart_list=cart_list(); $total=0; @endphp
                        @foreach($cart as $data)
                            @php
                                $total=$total+$data->quantity*$data->product->product_price;
                            @endphp
                            {{Form::hidden('cart_ids[]',$data->id)}}
                            {{Form::hidden('registry_id',$data->registry_id)}}
                            {{Form::hidden('product_ids[]',$data->product_id)}}
                            {{Form::hidden('quantity[]',$data->quantity)}}
                            <tr>
                                <td>{{$i++}}</td>
                                <td> @if(isset($data->product_image->product_image))
                                        <img style="width: 60px;" class="img-fluid" src="{{ filePath('product',$data->product_image->product_image) }}" alt="Product Image" />
                                    @else
                                        <img style="width: 60px;" class="img-fluid" src="{{ url('no_image.png') }}" alt="Product Image" />
                                    @endif</td>
                                <td>{{$data->product->product_name}}</td>
                                <td> {{$data->quantity}}</td>
                                <td> ${{$data->product->product_price*$data->quantity}}</td>

                            </tr>
                        @endforeach
                    <tr><td colspan="4" align="right">Total</td><td>${{$total}}</td></tr>
                    </tbody>
                </table>
                <hr>


                <h4>Delivery Address</h4>
                <div class="box-body">
                    <div class="row">

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                {{Form::label('name','Name',array('class'=>'col-sm-4 control-label'))}}
                                <div class="col-sm-12">
                                    {{Form::text('name',$profile->name,array('class'=>'form-control','placeholder'=>'Enter Name'))}}
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                {{Form::label('contact_number','Contact Number',array('class'=>'col-sm-6 control-label'))}}
                                <div class="col-sm-12">
                                    {{Form::text('contact_number',$profile->mobile,array('id'=>'contact_number','class'=>'form-control','placeholder'=>'Enter Contact Number'))}}
                                </div>
                            </div>

                    </div>
                    <div class="row">


                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                {{Form::label('address_line1','Address line 1',array('class'=>'col-sm-4 control-label'))}}
                                <div class="col-sm-12">
                                    {{Form::text('address_line1',isset($profile->user_address)? $profile->user_address->address_line1 : '' ,array('id'=>'address_line1','class'=>'form-control','placeholder'=>'Enter Address Line 1'))}}
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                {{Form::label('address_line2','Address line 2',array('class'=>'col-sm-4 control-label'))}}
                                <div class="col-sm-12">
                                    {{Form::text('address_line2',isset($profile->user_address)? $profile->user_address->address_line2 : '' ,array('id'=>'address_line2','class'=>'form-control','placeholder'=>'Enter Address Line 2'))}}
                                </div>
                            </div>

                    </div>
                    <div class="row">

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                {{Form::label('town','Town',array('class'=>'col-sm-4 control-label'))}}
                                <div class="col-sm-12">
                                    {{Form::text('town',isset($profile->user_address)? $profile->user_address->town : '' ,array('id'=>'town','class'=>'form-control','placeholder'=>'Enter Town'))}}
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                {{Form::label('county','County',array('class'=>'col-sm-4 control-label'))}}
                                <div class="col-sm-12">
                                    {{Form::text('country',isset($profile->user_address)? $profile->user_address->county : '' ,array('id'=>'county','class'=>'form-control','placeholder'=>'Enter County'))}}
                                </div>

                        </div>
                    </div>
                    <div class="row">

                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                {{Form::label('postcode','Postcode',array('class'=>'col-sm-4 control-label'))}}
                                <div class="col-sm-12">
                                    {{Form::text('postcode',isset($profile->user_address)? $profile->user_address->postcode : '' ,array('id'=>'postcode','class'=>'form-control','placeholder'=>'Enter Postcode'))}}
                                </div>
                            </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                {{Form::label('country','Country',array('class'=>'col-sm-4 control-label'))}}
                                <div class="col-sm-12">
                                    {{Form::text('country',isset($profile->user_address)? $profile->user_address->country : '' ,array('id'=>'country','class'=>'form-control','placeholder'=>'Enter Country'))}}
                                </div>

                        </div>
                    </div>
                
                    @php $referal_balance=$credit-$debit;@endphp
                    @if($referal_balance>0)
                        <h4>Referal Discount<span class="pull-right">{{number_format($referal_balance,2)}}</span></h4>
                        <label>{{form::checkbox('referal_amount',$referal_balance,false,array('class'=>'referal-amount'))}}Use Referal Amount</label>
                    @endif
                    {{Form::hidden('use_referal',0,array('class'=>'txt-use-referal'))}}
                    {{Form::hidden('paid_amount',$total,array('class'=>'txt-paid-amount'))}}
                    {{Form::hidden('total',$total,array('class'=>''))}}

                    <h4>Total Amount <span class="pull-right">${{number_format($total,2)}}</spna></h4>
                    
                </div>
                <button type="submit" class="btn btn-primary pull-right">Proceed to Checkout</button>
                {{Form::close()}}
            </div>

        </div>
    </div>
<script>
var total={{$total}};
$(document).on('click','.referal-amount',function(){
    if($(this).is(":checked"))
    {
        var ref_dis=$(this).val();
        if(ref_dis>=total)
        {
            var total_amount=0;
            $(".txt-use-referal").val(total);
        }
        else
        {
            var total_amount=total-ref_dis;
            $(".txt-use-referal").val(ref_dis);
        }
        $(".total-paid-amount").html(total_amount.toFixed(2));
        $(".txt-paid-amount").val(total_amount.toFixed(2));
        
    }
    else
    {
        $(".total-paid-amount").html(total.toFixed(2));
        $(".txt-paid-amount").val(total);
        $(".txt-use-referal").val(0);
    }
    
});
</script>
@endsection
