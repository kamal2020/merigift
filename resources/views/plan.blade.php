@extends('layouts.app')
@section('content')
    
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Plan</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>
            <div class='col-lg-12'>
                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if(isset($error))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $error }}</strong>
                    </div>
                @endif
                <div class="row plans-table">
                    <article>
                        <ul>
                            <li class="bg-purple">
                                <button>Gold</button>
                            </li>
                            <li class="bg-blue">
                                <button>Diamond</button>
                            </li>
                            <li class="bg-blue active">
                                <button>Platinum</button>
                            </li>
                        </ul>

                        <table>
                            <thead>
                            <tr>
                                <th class="hide"></th>
                                @foreach($plan as $class2)
                                    <th>
                                        {{$class2->title}}
                                    </th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($feature as $class)
                                <tr>
                                <td>{{$class->title}}</td>
                                    @foreach($plan as $class2)
                                        @foreach($planfeature as $value)
                                            @if($class2->id==$value->plan_id && $class->id==$value->feature_id)
                                            <td>
                                                @if($value->status=='Y')
                                                    {!! ($value->extra_value!='') ? $value->extra_value : '<span class="tick">&#10004;</span>' !!}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </tr>
                                @endforeach
                            <tr>
                                <td>One Year</td>
                                @foreach($plan as $class2)
                                    <th align="center">
                                        <span class="txt-l">${{$class2->oneyear}}</span>
                                        @if(Auth::user())
                                        {{Form::open(array('url'=>'plan-detail'))}}
                                            {{Form::hidden('plan_id',$class2->id)}}
                                            {{Form::hidden('plan_type','oneyear')}}
                                            @if(isset($user) && $user->plan_id==$class2->id && $user->plan_type=='oneyear')
                                               <h5>Purchased</h5>
                                            @else
                                                <button type="submit" class="btn btn-sm btn-info">Buy</button>
                                            @endif

                                        {{Form::close()}}
                                        @else
                                            <a href="{{url('login')}}" class="btn btn-sm btn-info">Buy</button>
                                        @endif
                                    </th>
                                @endforeach
                            </tr>
                            <tr>
                                <td>Life Time</td>
                                @foreach($plan as $class2)
                                    <th align="center">
                                        <span class="txt-l">${{$class2->lifetime}}</span>
                                        @if(Auth::user())
                                        {{Form::open(array('url'=>'plan-detail'))}}
                                            {{Form::hidden('plan_id',$class2->id)}}
                                            {{Form::hidden('plan_type','lifetime')}}
                                            @if(isset($user) && $user->plan_id==$class2->id && $user->plan_type=='lifetime')
                                               <h5>Purchased</h5>
                                            @else
                                                <button type="submit" class="btn btn-sm btn-info">Buy</button>
                                            @endif
                                        {{Form::close()}}
                                        @else   
                                            <a href="{{url('login')}}" class="btn btn-sm btn-info">Buy</button>
                                        @endif
                                    </th>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>

                    </article>



                </div>
            </div>
        </div>
    </div>
<script>

</script>
@endsection
