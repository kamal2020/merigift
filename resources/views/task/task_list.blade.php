@extends('layouts.app')
@section('content')
    <div class="container pt90 pb60">
        <div class='row'>
        @include('layouts.sidebar')
            <div class='col-lg-9'>
                <div class="pull-right">
                    <a href="{{url('/add-task/'.$id)}}">
                        <i class="fa fa-plus"></i> <span>Add Task</span>
                    </a>
                </div>
                <table class="table">
                    <tbody>
                        <tr>
                        <th style="width: 10px">#</th>
                        <th>Checklist</th>
                        <th>Task Name</th>
                        <th>Created_at</th>
                        <th>Action</th>
                        </tr>
                        @php $i=1;@endphp
                        @foreach($task as $class)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$class->checklist->name}}</td>
                                <td>{{$class->name}}</td>
                                <td> {{ $class->created_at }}</td>
                                <td>
                                <a class="btn btn-primary btn-sm " href="{{url('edit-task',$class->id)}}">Edit</a>
                                <a class="btn btn-danger btn-sm " href="{{url('delete_task',$class->id)}}">Delete</a>
                                
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
