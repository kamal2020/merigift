@extends('layouts.app')
@section('content')
    <div class="container pt90 pb60">
        <div class='row'>
        @include('layouts.sidebar')
            <div class='col-lg-9'>

                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                @if(isset($task))
                        <div class="title-heading1 mb40"><h4>Update Task</h4></div>
                    {{Form::model($task,array('url'=>array('update_task',$task->id),'id'=>'task-form','class'=>'form-horizantal'))}}
                @else
                        <div class="title-heading1 mb40"><h4>Add Task</h4></div>
                    {{Form::open(array('url'=>array('insert_task',$id),'id'=>'task-form','class'=>'form-horizantal'))}}
                @endif
                <div class="form-group">
                    {{Form::label('name','Task Name',array('class'=>'col-sm-2 control-label'))}}
                    <div class="col-sm-10">
                        {{Form::text('name',null,array('class'=>'form-control','placeholder'=>'Enter Task Name','required' => 'required'))}}
                    </div>
                </div>
                
                <div class="pull-right">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info ">Submit</button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
    <script>
        $("#task-form").validate({
            rules: {
                name: "required",

            },
            messages:{
                name:{
                    required:"Please Enter Task Name",
                },
            }
        });
    </script>

@endsection
