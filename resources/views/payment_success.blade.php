@extends('layouts.app')
@section('content')
    
    <div class="page-titles-img title-space-lg parallax-overlay bg-parallax" data-jarallax='{"speed": 0.4}' style='background-image: url("{{ URL::asset('front_asset/images/bg9.jpg') }}");background-position:top center;'>
        <div class="container">
            <div class="row">
                <div class=" col-md-12">
                    <h1 class="text-uppercase">Payment Success</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="container pt90 pb60">
        <div class='row'>

            <div class='col-lg-12'>
                <h4>Your Order Placed Successfully, Thanks for useing Merigift.</h4>
                <h5>Your Order Details Are</h5>
                <div><span>Invoice No.</span><span>#{{$payment->order_id}}</span></div>
                <div><span>Order Date.</span><span>{{$payment->date}}</span></div>
                <div><span>Total Amount</span><span>{{$payment->currency.' '.$payment->price}}</span></div>

                <h5>Product Details</h5>
                <table class="table">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Image</th>
                        <th>Product</th>
                        <th>Qty</th>
                        <th>Price</th>
                    </tr>
                    @php $i=1; $cart_list=cart_list(); $total=0; @endphp
                    @foreach($cart as $data)
                        @php
                            $total=$total+$data->quantity*$data->product->product_price;
                        @endphp

                        <tr>
                            <td>{{$i++}}</td>
                            <td> @if(isset($data->product_image->product_image))
                                    <img style="width: 60px;" class="img-fluid" src="{{ filePath('product',$data->product_image->product_image) }}" alt="Product Image" />
                                @else
                                    <img style="width: 60px;" class="img-fluid" src="{{ url('no_image.png') }}" alt="Product Image" />
                                @endif</td>
                            <td>{{$data->product->product_name}}</td>
                            <td> {{$data->quantity}}</td>
                            <td> ${{$data->product->product_price*$data->quantity}}</td>

                        </tr>
                    @endforeach
                    <tr><td colspan="4" align="right">Total</td><td>${{$total}}</td></tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
<script>

</script>
@endsection
