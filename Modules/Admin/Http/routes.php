<?php

Route::group(['middleware' => ['web','auth','CheckAdmin'], 'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{

    Route::get('dashboard', 'AdminController@index');
    Route::get('/category', 'AdminController@category');
    Route::post('insert_category','AdminController@insert_category');
    Route::get('edit_category/{id}','AdminController@edit_category')->name('edit-cat'); 
    Route::post('update_category/{id}','AdminController@update_category')->name('up-cat');
    Route::get('delete_category/{id}','AdminController@delete_category')->name('delete-cat');
    Route::get('category_list','AdminController@category_list');
    Route::post('cat_status/{id}','AdminController@cat_status');

    Route::get('sub_category','AdminController@sub_category');
    Route::post('insert_sub_category','AdminController@insert_sub_category');
    Route::get('edit_sub_category/{id}','AdminController@edit_sub_category')->name('edit-sub-cat'); 
    Route::post('update_sub_category/{id}','AdminController@update_sub_category')->name('update-sub-cat');
    Route::get('delete_sub_category/{id}','AdminController@delete_sub_category')->name('delete-sub-cat'); 
    Route::get('sub_category_list','AdminController@sub_category_list');
    Route::post('sub_cat_status/{id}','AdminController@sub_cat_status');

    Route::get('/brand', 'AdminController@brand');
    Route::post('insert_brand','AdminController@insert_brand');
    Route::get('edit_brand/{id}','AdminController@edit_brand')->name('edit-brand');
    Route::post('update_brand/{id}','AdminController@update_brand')->name('up-brand');
    Route::get('delete_brand/{id}','AdminController@delete_brand')->name('delete-brand');
    Route::get('brand_list','AdminController@brand_list');
    Route::post('brand_status/{id}','AdminController@brand_status');

    Route::get('/product', 'AdminController@product');
    Route::post('insert_product','AdminController@insert_product');
    Route::get('edit_product/{id}','AdminController@edit_product')->name('edit-product');
    Route::post('update_product/{id}','AdminController@update_product')->name('up-product');
    Route::get('delete_product/{id}','AdminController@delete_product')->name('delete-product');
    Route::get('product_list','AdminController@product_list');
    Route::post('product_status/{id}','AdminController@product_status');
    Route::post('get_sub_cat_list/{id}','AdminController@get_sub_cat_list');
    Route::get('delete_product_image/{id}','AdminController@delete_product_image')->name('delete-product-img');
    Route::post('get_product_type_list/{id}','AdminController@get_product_type_list');

    Route::get('plan','AdminController@plan');
    Route::post('insert_plan','AdminController@insert_plan');
    Route::get('edit_plan/{id}','AdminController@edit_plan')->name('edit-plan');
    Route::post('update_plan/{id}','AdminController@update_plan')->name('update-plan');
    Route::get('delete_plan/{id}','AdminController@delete_plan')->name('delete-plan');
    Route::get('plan_list','AdminController@plan_list');

    Route::get('product_type','AdminController@product_type');
    Route::post('insert_product_type','AdminController@insert_product_type');
    Route::get('edit_product_type/{id}','AdminController@edit_product_type')->name('edit-pt');
    Route::post('update_product_type/{id}','AdminController@update_product_type')->name('update-pt');
    Route::get('delete_product_type/{id}','AdminController@delete_product_type')->name('delete-pt');
    Route::get('product_type_list','AdminController@product_type_list');
    Route::post('product_type_status/{id}','AdminController@product_type_status');

    Route::get('user_list','UserController@user');

    Route::get('registry_list/{id}','AdminController@registry_list');
    Route::get('registry_item_list/{id}','UserController@registry_item_list');
    Route::get('check_list/{id}','AdminController@check_list');
    Route::get('task_list/{id}','AdminController@task_list');

    Route::get('change_password',function(){
        return view("admin::change_password");
    });
    Route::post('update_password','AdminController@update_password');


    Route::get('profile/{id}','AdminController@profile');
    Route::post('update_profile/{id}','AdminController@update_profile');

});
