<?php

namespace Modules\Admin\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\User;
use App\Registry;
use App\Registry_item;
class UserController extends Controller
{
    public function __construct()
    {
        ///$this->middleware('auth');
    }
    /////////////category methods///////////////////////////
    public function user()
    {
        $data['records']=User::where('role','User')->get();
        return view('admin::user.user_list',$data);
    }
    public function registry_item_list($id){
        $data['registry']=Registry::where('is_deleted','N')->pluck('name','id');
        $data['registry_item'] = Registry_item::where('registries_id',$id)->with('product')->get();
        return view('admin::user.registry_item_list',$data);
    }
}
