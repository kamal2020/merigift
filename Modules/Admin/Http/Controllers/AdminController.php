<?php

namespace Modules\Admin\Http\Controllers;
use App\Category;
use App\Sub_category;
use App\Brand;
use App\Product;
use App\Feature;
use App\Plan;
use App\Product_image;
use App\Product_type;
use App\Registry;
use App\Checklist;
use App\Task;
use App\User;
use App\Planfeature;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Auth;
use Hash;

class AdminController extends Controller
{
    public function __construct()
    {
        ///$this->middleware('auth');
    }

    public function index()
    {
        $count['users'] = User::where('is_deleted','N')->where('role','User')->count();
        $count['registry'] = Registry::where('is_deleted','N')->count();
        $count['brand'] = Brand::where('brand_deleted','N')->count();
        $count['product'] = Product::where('product_deleted','N')->count();
        $count['category'] = Category::where('cat_deleted','N')->count();
        $count['sub_category'] = Sub_category::where('sub_cat_deleted','N')->count();

        return view('admin::dashboard',$count);
    }
    /////////////category methods///////////////////////////
    public function category_list()
    {

        $data['records']=Category::where('cat_deleted','N')->get();
        return view('admin::category.category_list',$data);


    }
    public function category()
    {
        return view('admin::category.category_form');
    }
    public function insert_category(Request $request)
    {
        $validation = Validator::make( $request->all(), [
            'cat_name' => 'required',
        ]);
        if($validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }

        $data=Category::insert($request->except(['_token']));
        if($data)
            return redirect('admin/category_list')->with('success','Category created successfully!');
        else
            return redirect('admin/category_list')->with('error','Category not created !');

    }

    public function edit_category($id){

        $data['category'] = Category::find($id);
        return view('admin::category.category_form',$data);
    }
    public function update_category(Request $request,$id){
        $validation = Validator::make( $request->all(), [
            'cat_name' => 'required',
        ]);
        if ( $validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        $data=Category::where('id',$id)->update($request->except(['_token']));
        if($data)
            return redirect('admin/category_list')->with('success','Category Updated!');
        else
            return redirect('admin/category_list')->with('error','Category not Update!');
    }
    public function delete_category($id)
    {
        $data=Category::where('id',$id)->update(['cat_deleted'=>'Y']);
        if($data)
            return redirect('admin/category_list')->with('success','Category Deleted!');
        else
            return redirect('admin/category_list')->with('error','Category not delete!');
    }
    public function cat_status(Request $request,$id)
    {
        $data=Category::where('id',$id)->update(['cat_status'=>$request->val]);
        return json_encode($id);
    }

    /////////////////sub category methods///////////////////
    public function sub_category(){
        $data['category']=Category::where('cat_deleted','N')->where('cat_status','Y')->pluck('cat_name','id');
        return view('admin::sub_category.sub_category_form',$data);
    }
    public function insert_sub_category(Request $request)
    {
        $validation = Validator::make( $request->all(), [
            'sub_cat_name' => 'required',
            'category_id' => 'required',
        ]);
        if ( $validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        $data=Sub_category::insert($request->except(['_token']));
        if($data)
            return redirect('admin/sub_category_list')->with('success','Sub Category created successfully!');
        else
            return redirect('admin/sub_category_list')->with('error','Sub Category not created !');
    }
    public function edit_sub_category($id){
        $data['sub_category'] = Sub_category::find($id);
        $data['category']=Category::where('cat_deleted','N')->where('cat_status','Y')->pluck('cat_name','id');
        return view('admin::sub_category.sub_category_form',$data);
    }
    public function sub_category_list()
    {
        $data['sub_category']=Sub_category::where('sub_cat_deleted','N')->with('category')->get();
        return view('admin::sub_category.sub_category_list',$data);
    }
    public function update_sub_category(Request $request,$id)
    {
        $validation = Validator::make( $request->all(), [
            'sub_cat_name' => 'required',
            'category_id' => 'required',
        ]);
        if ( $validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        $data=Sub_category::where('id',$id)->update($request->except(['_token']));
        if($data)
            return redirect('admin/sub_category_list')->with('success','Sub Category Updated!');
        else
            return redirect('admin/sub_category_list')->with('error','Sub Category not Update!');
    }
    public function delete_sub_category($id)
    {
        $data=Sub_category::where('id',$id)->update(['sub_cat_deleted'=>'Y']);
        if($data)
            return redirect('admin/sub_category_list')->with('success','Sub Category Deleted!');
        else
            return redirect('admin/sub_category_list')->with('error','Sub Category not delete!');
    }
    public function sub_cat_status(Request $request,$id)
    {
        $data=Sub_category::where('id',$id)->update(['sub_cat_status'=>$request->val]);
        return json_encode($id);
    }

    /////////////////product type methods///////////////////
    public function product_type(){
        $data['category']=Category::where('cat_deleted','N')->where('cat_status','Y')->pluck('cat_name','id');
        $data['sub_category']=Sub_category::where('sub_cat_deleted','N')->where('sub_cat_status','Y')->pluck('sub_cat_name','id');
        return view('admin::product_type.product_type_form',$data);
    }
    public function insert_product_type(Request $request)
    {
        $validation = Validator::make( $request->all(), [
            'pro_type_name' => 'required',
            'category_id' => 'required',
            'sub_category_id' => 'required',
        ]);
        if ( $validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        $data=Product_type::insert($request->except(['_token']));
        if($data)
            return redirect('admin/product_type_list')->with('success','Product Type created successfully!');
        else
            return redirect('admin/product_type_list')->with('error','Product Type not created !');
    }
    public function edit_product_type($id){
        $data['product_type'] = Product_type::find($id);
        $data['category']=Category::where('cat_deleted','N')->where('cat_status','Y')->pluck('cat_name','id');
        $data['sub_category']=Sub_category::where('sub_cat_deleted','N')->where('sub_cat_status','Y')->pluck('sub_cat_name','id');
        return view('admin::product_type.product_type_form',$data);
    }
    public function product_type_list()
    {
        $data['product']=Product_type::where('pro_type_deleted','N')->with('category','sub_category')->get();
        return view('admin::product_type.product_type_list',$data);
    }
    public function update_product_type(Request $request,$id)
    {
        $data=Product_type::where('id',$id)->update($request->except(['_token']));
        if($data)
            return redirect('admin/product_type_list')->with('success','Sub Category Updated!');
        else
            return redirect('admin/product_type_list')->with('error','Sub Category not Update!');
    }
    public function delete_product_type($id)
    {
        $data=Product_type::where('id',$id)->update(['pro_type_deleted'=>'Y']);
        if($data)
            return redirect('admin/product_type_list')->with('success','Product Type Deleted!');
        else
            return redirect('admin/product_type_list')->with('error','Product Type not delete!');
    }
    public function product_type_status(Request $request,$id)
    {
        $data=Product_type::where('id',$id)->update(['pro_type_status'=>$request->val]);
        return json_encode($id);
    }

    ///////////Brand Methods/////////////////////
    public function brand_list()
    {
        $data['records']=Brand::where('brand_deleted','N')->get();
        return view('admin::brand.brand_list',$data);
    }
    public function brand()
    {
        return view('admin::brand.brand_form');
    }
    public function insert_brand(Request $request)
    {
        $validation = Validator::make( $request->all(), [
            'brand_name' => 'required',
            'brand_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'brand_logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ( $validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        $input['brand_name']=$request->brand_name;
        $image = $request->file('brand_image');
        $input['brand_image'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/brands');
        $image->move($destinationPath, $input['brand_image']);
        $logo = $request->file('brand_logo');
        $input['brand_logo'] = time().'1.'.$logo->getClientOriginalExtension();
        $destinationPath = public_path('/brands');
        $logo->move($destinationPath, $input['brand_logo']);
        $data=Brand::insert($input);
        if($data)
            return redirect('admin/brand_list')->with('success','Brand created successfully!');
        else
            return redirect('admin/brand_list')->with('error','Brand not created !');

    }
    public function edit_brand($id){
        $data['brand'] = Brand::find($id);
        return view('admin::brand.brand_form',$data);
    }
    public function update_brand(Request $request,$id){
        $validation = Validator::make( $request->all(), [
            'brand_name' => 'required',
            'brand_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'brand_logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ( $validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        $input['brand_name']=$request->brand_name;
        $image = $request->file('brand_image');
        if($image)
        {
            $input['brand_image'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/brands');

            $image->move($destinationPath, $input['brand_image']);

        }

        $logo = $request->file('brand_logo');


       if($logo) {
           $input['brand_logo'] = time() . '1.' . $logo->getClientOriginalExtension();

           $destinationPath = public_path('/brands');

           $logo->move($destinationPath, $input['brand_logo']);
       }

        $data=Brand::where('id',$id)->update($input);
        if($data)
            return redirect('admin/brand_list')->with('success','Brand Updated!');
        else
            return redirect('admin/brand_list')->with('error','Brand not Update!');
    }
    public function delete_brand($id)
    {
        $data=Brand::where('id',$id)->update(['brand_deleted'=>'Y']);
        if($data)
            return redirect('admin/brand_list')->with('success','Brand Deleted!');
        else
            return redirect('admin/brand_list')->with('error','Brand not delete!');
    }
    public function brand_status(Request $request,$id)
    {
        $data=Brand::where('id',$id)->update(['brand_status'=>$request->val]);
        return json_encode($id);
    }

    /////////////////Product methods///////////////////
    public function product(){
        $data['category']=Category::where('cat_status','Y')->where('cat_deleted','N')->pluck('cat_name','id');
        $data['sub_category']=Sub_category::where('sub_cat_status','Y')->where('sub_cat_deleted','N')->pluck('sub_cat_name','id');
        $data['brand']=Brand::where('brand_status','Y')->where('brand_deleted','N')->pluck('brand_name','id');
        $data['product_type']=Product_type::pluck('pro_type_name','id');
        return view('admin::product.product_form',$data);
    }
    public function insert_product(Request $request)
    {
        $validation = Validator::make( $request->all(), [
            'product_name' => 'required',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'product_type_id' => 'required',
            'brand_id' => 'required',
            'product_price' => 'required',
            'product_description' => 'required',
            //'product_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        if ( $validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }

        $input['product_name']=$request->product_name;
        $input['category_id']=$request->category_id;
        $input['sub_category_id']=$request->sub_category_id;
        $input['product_type_id']=$request->product_type_id;
        $input['brand_id']=$request->brand_id;
        $input['product_price']=$request->product_price;
        $input['product_description']=$request->product_description;

        $id=Product::insertGetId($input);
        if($id)
        {
            $image = $request->file('product_image');
            //dd($image);
            if($image) {
                $input1['product_id'] = $id;
                $i=1;
                foreach ($image as $file) {
                    // dd($file);
                    $image = $file;
                    $input1['product_image'] = time() .$i. '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('/product');
                    $image->move($destinationPath, $input1['product_image']);
                    $data = Product_image::insert($input1);
                    $i++;
                }
            }
            return redirect('admin/product_list')->with('success','Product created successfully!');
        }
        else
        { return redirect('admin/product_list')->with('error','Product not created !'); }
    }
    public function edit_product($id){
        $data['product'] = Product::find($id);
        $data['category']=Category::pluck('cat_name','id');
        $data['product_type']=Product_type::pluck('pro_type_name','id');
        
        $data['sub_category']=Sub_category::pluck('sub_cat_name','id');
        $data['brand']=Brand::pluck('brand_name','id');
        $data['product_image'] = Product_image::where('product_id',$id)->get();

        return view('admin::product.product_form',$data);
    }
    public function product_list()
    {
        $data['product']=Product::where('product_deleted','N')->with('category','sub_category','brand')->orderBy('id','desc')->get();
        return view('admin::product.product_list',$data);
    }
    public function update_product(Request $request,$id)
    {
        $validation = Validator::make( $request->all(), [
            'product_name' => 'required',
            'category_id' => 'required',
            'sub_category_id' => 'required',
            'product_type_id' => 'required',
            'brand_id' => 'required',
            'product_price' => 'required',
            'product_description' => 'required',
            //'product_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if($validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        $input['product_name']=$request->product_name;
        $input['category_id']=$request->category_id;
        $input['sub_category_id']=$request->sub_category_id;
        $input['product_type_id']=$request->product_type_id;
        $input['brand_id']=$request->brand_id;
        $input['product_price']=$request->product_price;
        $input['product_description']=$request->product_description;
        $data=Product::where('id',$id)->update($input);
        if($data)
        {
            $image = $request->file('product_image');
            //dd($image);
            if($image) {
                $input1['product_id'] = $id;
                $i=1;
                foreach ($image as $file) {
                   // dd($file);
                    $image = $file;
                    $input1['product_image'] = time() .$i. '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('/product');
                    $image->move($destinationPath, $input1['product_image']);
                    $data = Product_image::insert($input1);
                    $i++;
                }
            }
            return redirect('admin/product_list')->with('success','Product created successfully!');
        }
        else
        { return redirect('admin/product_list')->with('error','Product not created !'); }

    }
    public function delete_product($id)
    {
        $data=Product::where('id',$id)->update(['product_deleted'=>'Y']);
        if($data)
            return redirect('admin/product_list')->with('success','Product Deleted!');
        else
            return redirect('admin/product_list')->with('error','Product not delete!');
    }

    public function delete_product_image($id)
    {
        $data=Product_image::where('id',$id)->delete();
        return response()->json($data);
    }

    public function product_status(Request $request,$id)
    {
        $data=Product::where('id',$id)->update(['product_status'=>$request->val]);
        return json_encode($id);
    }

    public function get_sub_cat_list($id)
    {
        $list=Sub_category::where('sub_cat_deleted','N')->where('sub_cat_status','Y')->where('category_id',$id)->get();
        return response()->json($list);
    }
    public function get_product_type_list($id)
    {
        $list=Product_type::where('sub_category_id',$id)->where('pro_type_status','Y')->where('pro_type_deleted','N')->get();
        return response()->json($list);
    }

    /////////////////Plans methods///////////////////
    public function plan(){
        $data['feature']=Feature::pluck('title','id');
        return view('admin::plan.plan_form',$data);
    }
    public function insert_plan(Request $request)
    {
        $validation = Validator::make( $request->all(), [
            'title' => 'required',
           // 'amount' => 'required',
        ]);
        if ( $validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }
        $id=plan::insertGetId($request->except(['_token','features','extra']));
        $feature=Feature::get();
        foreach($feature as $value){
            if(in_array($value->id,$request->features)){
                $feat_array[]=array('plan_id'=>$id,'feature_id'=>$value->id,'status'=>'Y','extra_value'=>(isset($request->extra[$value->id])) ? $request->extra[$value->id] : NULL);
            }
            else{
                $feat_array[]=array('plan_id'=>$id,'feature_id'=>$value->id,'status'=>'N','extra_value'=>NULL);
            }   
        }
        $data=PlanFeature::insert($feat_array);
        if($id)
            return redirect('admin/plan_list')->with('success','Plan created successfully!');
        else
            return redirect('admin/plan_list')->with('error','Plan not created !');
    }
    public function edit_plan($id){
        $data['plan'] = plan::with('planfeature')->find($id);
        $data['planfeature']=Planfeature::where(['plan_id'=>$id,'status'=>'Y'])->get();
        $data['feature']=Feature::pluck('title','id');
        return view('admin::plan.plan_form',$data);
    }
    public function plan_list()
    {
        $data['plan']=plan::where('plan_deleted','N')->with('planfeature')->get();
        $data['feature']=Feature::pluck('title','id');
        return view('admin::plan.plan_list',$data);
    }
    public function update_plan(Request $request,$id)
    {
        //dd($request->all());
        $data=plan::where('id',$id)->update($request->except(['_token','features','extra']));
        PlanFeature::where('plan_id',$id)->delete();
        $feature=Feature::get();
        foreach($feature as $value){
            if(in_array($value->id,$request->features)){
                $feat_array[]=array('plan_id'=>$id,'feature_id'=>$value->id,'status'=>'Y','extra_value'=>(isset($request->extra[$value->id])) ? $request->extra[$value->id] : NULL);
            }
            else{
                $feat_array[]=array('plan_id'=>$id,'feature_id'=>$value->id,'status'=>'N','extra_value'=>NULL);
            }    
        }
        $data=PlanFeature::insert($feat_array);
        if($data)
            return redirect('admin/plan_list')->with('success','Plan Updated!');
        else
            return redirect('admin/plan_list')->with('error','Plan not Update!');
    }
    public function delete_plan($id)
    {
        $data=plan::where('id',$id)->update(['plan_deleted'=>'Y']);
        $feat=Planfeature::where('plan_id',$id)->delete();
        if($data)
            return redirect('admin/plan_list')->with('success','Plan Deleted!');
        else
            return redirect('admin/plan_list')->with('error','Plan not delete!');
    }

    //Start Registry Method
    public function registry_list($id)
    {
        $data['registry']=Registry::where('user_id',$id)->where('is_deleted','N')->get();
        return view("admin::user.registry_list",$data);
    }
    //End Registry Method


    //Start Check list Method
    public function check_list($id)
    {
        $data['check_list']=Checklist::where('registry_id',$id)->where('is_deleted','N')->get();
        return view("admin::user.check_list",$data);
    }
    //End Check list Method


    //Start Task Method
    public function task_list($id)
    {
        $data['task']=Task::where('checklist_id',$id)->where('is_deleted','N')->get();
        return view("admin::user.task_list",$data);
    }
    //End Task Method


    //Start Change Password
    public function update_password(Request $request)
    {
        if (!(Hash::check($request->get('old_password'), Auth::user()->password))) {
            return redirect()->back()->with("error","Old Password does not match. Please try again.");
        }
        if(strcmp($request->get('old_password'), $request->get('new_password')) == 0){
            return redirect()->back()->with("error","New Password cannot be same as your current password.");
        } 
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return back()->with('success','Password Updated!');
    }

    //Start admin profile
    public function profile($id)
    {
        //dd(Auth::user());
        $data['profile']=User::where('id',$id)->first();
        //dd($data);
        return view("admin::change_profile",$data);
    }
    public function update_profile(Request $request,$id)
    {
        $validation = Validator::make( $request->all(), [
            'name' => 'required',
            'contact_number' => 'required',
            'product_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if($validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }

        $image = $request->file('image');
        //dd($image);
        if($image) {
                $input['image'] = time() .'.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/user_image');
                $image->move($destinationPath, $input['image']);

        }
        $input['name']=$request->name;
        $input['contact_number']=$request->contact_number;

        $data=User::where('id',$id)->update($input);
        if($data)
        {

            return redirect('admin/profile/'.$id)->with('success','Profile Updated successfully!');
        }
        else
        { return redirect('admin/profile/'.$id)->with('error','Profile Not Updated, Please Try Again!'); }
    }
    //End Change Password
}
