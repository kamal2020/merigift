@extends('admin::layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Plan List

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Plan</a></li>
        <li class="active">Plan List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Plan List</h3>

          <div class="box-tools pull-right">
              <a href="{{url('admin/plan')}}">
                  <i class="fa fa-plus"></i> <span>Add Plan</span>
              </a>
        </div>
        </div>
        <div class="box-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif


        <div class="box-body">
            <table class="table table-striped">
                <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Title</th>
                 <!--  <th>Date From To </th> -->
                  
                  <th>Features</th>
                  <th>One Year</th>
                  <th>Life Time</th>
                  <th>Created_at</th>
                  <th>Action</th>
                </tr>
                @php $i=1;@endphp
                @foreach($plan as $class)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$class->title}}</td>
                        <td>
                            @foreach($class->planfeature as $index => $value)
                                @if($value->status=='Y')
                                    <li>{{$value->feature->title}}
                                    {{($value->extra_value!='') ? "(".$value->extra_value.")" : ''}}
                                    </li>
                                @endif
                            @endforeach
                        </td>
                        <td>{{$class->oneyear}}</td>
                        <td>{{$class->lifetime}}</td>
                        <td> {{ $class->created_at }}</td>
                        <td>  
                            <a class="" href="{{route('edit-plan',$class->id)}}"> <i class="fa fa-edit"></i></a>
                            <a class="delete" href="{{route('delete-plan',$class->id)}}"> <i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
<script>
    $(document).on('click','.change_status',function(){
        if(confirm('Are You Sure ?'))
        {
            var id=$(this).data("id");
            var val=$(this).data("value");
            $.post('sub_cat_status/'+id,
                {
                    _token: "{{ csrf_token() }}",  
                    val: val
                }, 
                function(response){
                    location.reload();
                
            });
        }
    });
    $(document).on('click','.delete',function(event){
        event.preventDefault();
        var r=confirm("Are you sure you want to delete?");
        if (r==true)   {
            window.location = $(this).attr('href');
        }

    });
</script>
@endsection