@extends('admin::layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Plan

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Plan</a></li>

      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">@if(isset($plan)) Edit @else Add @endif Plans</h3>


        </div>
        <div class="box-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
            @if(isset($plan))
              {{Form::model($plan,array('url'=>array('admin/update_plan',$plan->id),'id'=>'plan-form','class'=>'form-horizontal'))}}
            @else
              {{Form::open(array('url'=>'admin/insert_plan','id'=>'plan-form','class'=>'form-horizontal'))}}
            @endif
          <div class="box-body">
            <div class="form-group">
              {{Form::label('title','Plan Title',array('class'=>'col-sm-2 control-label'))}}
              <div class="col-sm-10">
                {{Form::text('title',null,array('class'=>'form-control','placeholder'=>'Enter Plan Title'))}}
              </div>
            </div>
            <div class="form-group">
              {{Form::label('feature','Features',array('class'=>'col-sm-2 control-label'))}}
              <div class="col-sm-10">
              @foreach($feature as $val=>$item)
                <div class="checkbox">
                  <label>
                    @php $flag=false; $extra=null; @endphp
                    @if(isset($plan))
                      @foreach($planfeature as $data)
                          @if($data->feature_id==$val)
                              @php $flag=true; $extra=$data->extra_value; @endphp

                          @endif
                      @endforeach
                    @endif
                    {{Form::checkbox('features[]', $val,$flag)}}
                    {{$item}}
                  </label> 
                </div>
                @if($val==3)
                  {{Form::number('extra[3]',$extra,array('class'=>'form-control','placeholder'=>'No of website design'))}}
                @endif
              @endforeach
              </div>
            </div>
            <div class="form-group">
              {{Form::label('oneyear','One Year',array('class'=>'col-sm-2 control-label'))}}
              <div class="col-sm-10">
                {{Form::text('oneyear',null,array('class'=>'form-control','placeholder'=>'Enter Amount'))}}
              </div>
            </div>
            <div class="form-group">
              {{Form::label('lifetime','Life time',array('class'=>'col-sm-2 control-label'))}}
              <div class="col-sm-10">
                {{Form::text('lifetime',null,array('class'=>'form-control','placeholder'=>'Enter Amount'))}}
              </div>
            </div>
            <div class="box-footer pull-right">
              <button type="reset" class="btn btn-default">Cancel</button>
              <button type="submit" class="btn btn-info ">Submit</button>
            </div>
          </div>
           {{Form::close()}}
        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection