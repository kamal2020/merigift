@extends('admin::layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          Product Type

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Product Type</a></li>

      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">@if(isset($product_type)) Edit @else Add @endif Product Type</h3>


        </div>
        <div class="box-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if(isset($product_type))
              {{Form::model($product_type,array('url'=>array('admin/update_product_type',$product_type->id),'id'=>'product-type-form','class'=>'form-horizontal'))}}
            @else
              {{Form::open(array('url'=>'admin/insert_product_type','id'=>'product-type-form','class'=>'form-horizontal'))}}
            @endif
        <div class="box-body">
            <div class="form-group">
                {{Form::label('pro_type_name','Product Type',array('class'=>'col-sm-2 control-label'))}}
                <div class="col-sm-10">
                    {{Form::text('pro_type_name',null,array('class'=>'form-control','placeholder'=>'Enter Product Name'))}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('category','Category',array('class'=>'col-sm-2 control-label'))}}
                <div class="col-sm-10">
                    {{Form::select('category_id',$category,null,array('class'=>'form-control','id'=>'category_id','placeholder'=>'Select Category'))}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('sub_category','Sub Category',array('class'=>'col-sm-2 control-label'))}}
                <div class="col-sm-10">
                    @if(isset($product_type))
                        {{Form::select('sub_category_id',$sub_category,null,array('class'=>'form-control','id'=>'sub_category_id','placeholder'=>'Select Sub Category'))}}
                    @else
                        {{Form::select('sub_category_id',[],null,array('class'=>'form-control','id'=>'sub_category_id','placeholder'=>'Select Sub Category'))}}
                    @endif
                </div>
            </div>
            <div class="box-footer pull-right">
              <button type="reset" class="btn btn-default">Cancel</button>
              <button type="submit" class="btn btn-info ">Submit</button>
            </div>
           {{Form::close()}}
        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
    <script>
        var host='<?=url('/')?>';
        $(document).on('click','#category_id',function(){
            var id=$('#category_id').val();

            $.post(host+'/admin/get_sub_cat_list/'+id,
                {
                    _token: "{{ csrf_token() }}",
                    // val: val
                },
                function(response){
                    //var category_data = response.data;
                    //console.log(response);
                    var option_string_default = '';
                    var option_string = '<option value="">Enter Sub Category</option>';
                    if(response!='') {
                        $.each(response, function (i, e) {
                            option_string += '<option ';
                            option_string += 'value="' + e.id + '" ';
                            option_string += '>';
                            option_string += e.sub_cat_name;
                            option_string += '</option>';
                        });
                    }
                    // console.log($this.parentsUntil('form').parent());
                    $('#sub_category_id').html('').append(option_string_default + ' ' + option_string);


                });

        });
    </script>
@endsection