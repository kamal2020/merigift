@extends('admin::layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category page
        <small>it all starts here</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Category</a></li>
        <li class="active">Category page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Category</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            {{Form::open(array('url'=>'admin/update_category','class'=>'form-horizantal'))}}
                {{Form::text('id',$records->id,array('class'=>'form-control','placeholder'=>'Enter Category'))}}
        <div class="box-body">
                <div class="form-group">
                  {{Form::label('category','Category',array('class'=>'col-sm-2 control-label'))}}

                  <div class="col-sm-10">
                    {{Form::text('category',$records->cat_name,array('class'=>'form-control','placeholder'=>'Enter Category'))}}
                  </div>
                </div>
              </div>
              <div class="box-footer pull-right">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info ">Submit</button>
              </div>
           {{Form::close()}}
        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection