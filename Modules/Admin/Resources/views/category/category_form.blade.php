@extends('admin::layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Category</a></li>

      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">@if(isset($category)) Edit @else Add @endif Category</h3>

        </div>
        <div class="box-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            @if(isset($category))
                    {{Form::model($category,array('url'=>array('admin/update_category',$category->id),'id'=>'category-form','class'=>'form-horizontal'))}}
                @else
                    {{Form::open(array('url'=>'admin/insert_category','id'=>'category-form','class'=>'form-horizontal'))}}
                @endif
        <div class="box-body">
                <div class="form-group">
                  {{Form::label('category','Category',array('class'=>'col-sm-2 control-label'))}}

                  <div class="col-sm-10">
                    {{Form::text('cat_name',null,array('class'=>'form-control','placeholder'=>'Enter Category'))}}
                  </div>
                </div>
              </div>
              <div class="box-footer pull-right">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info ">Submit</button>
              </div>
           {{Form::close()}}
        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<script>

</script>
@endsection