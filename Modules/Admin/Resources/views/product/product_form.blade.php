@extends('admin::layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Product

            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Product</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">@if(isset($product)) Edit @else Add @endif Product</h3>


                </div>
                <div class="box-body">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                    @if ($message = Session::get('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif

                        @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>

                        @endforeach

                    @if(isset($product))
                        {{Form::model($product,array('url'=>array('admin/update_product',$product->id),'id'=>'product-form','class'=>'form-horizontal','enctype' => 'multipart/form-data'))}}
                    @else
                        {{Form::open(array('url'=>'admin/insert_product','id'=>'product-form','class'=>'form-horizontal','enctype' => 'multipart/form-data'))}}
                    @endif
                    <div class="box-body">
                        <div class="form-group">
                            {{Form::label('product_name','Product Name',array('class'=>'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                {{Form::text('product_name',null,array('class'=>'form-control','placeholder'=>'Enter Product Name'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('category','Category',array('class'=>'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                {{Form::select('category_id',$category,null,array('class'=>'form-control','id'=>'category_id','placeholder'=>'Select Category'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('sub_category','Sub Category',array('class'=>'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                @if(isset($product))
                                {{Form::select('sub_category_id',$sub_category,null,array('class'=>'form-control','id'=>'sub_category_id','placeholder'=>'Select Sub Category'))}}
                                @else
                                {{Form::select('sub_category_id',[],null,array('class'=>'form-control','id'=>'sub_category_id','placeholder'=>'Select Sub Category'))}}
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('product_type_id','Product Type',array('class'=>'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                @if(isset($product))
                                    {{Form::select('product_type_id',$product_type,null,array('class'=>'form-control','id'=>'product_type_id','placeholder'=>'Select Product Type'))}}
                                @else
                                    {{Form::select('product_type_id',[],null,array('class'=>'form-control','id'=>'product_type_id','placeholder'=>'Select Product Type'))}}
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('brand','Brand',array('class'=>'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                {{Form::select('brand_id',$brand,null,array('class'=>'form-control','placeholder'=>'Select Braand'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('price','Price',array('class'=>'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                {{Form::text('product_price',null,array('class'=>'form-control','placeholder'=>'Enter Price'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('product_description','Product Description',array('class'=>'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                {{Form::text('product_description',null,array('class'=>'form-control','placeholder'=>'Enter Description'))}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('product_image','Product Image',array('class'=>'col-sm-2 control-label'))}}
                            <div class="col-sm-10">
                                {{Form::file('product_image[]',array('class'=>'form-control','multiple' => 'multiple'))}}
                            </div>
                            @if(isset($product_image))
                                @foreach($product_image as $class)
                                    <span id="img_{{$class->id}}"><img src="{{URL::asset('product/'.$class->product_image)}}"><a class="btn btn-danger btn-sm delete_image" data-id="{{$class->id}}"> Delete</a> </span>
                                @endforeach
                            @endif

                        </div>


                    </div>
                    <div class="box-footer pull-right">
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-info ">Submit</button>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>



    <script>
        var host='<?=url('/')?>';
        $(document).on('click','#category_id',function(){
                var id=$('#category_id').val();

                $.post(host+'/admin/get_sub_cat_list/'+id,
                    {
                        _token: "{{ csrf_token() }}",
                       // val: val
                    },
                    function(response){
                        //var category_data = response.data;
                        //console.log(response);
                        var option_string_default = '';
                        var option_string = '<option value="">Enter Sub Category</option>';
                        if(response!='') {
                            $.each(response, function (i, e) {
                                option_string += '<option ';
                                option_string += 'value="' + e.id + '" ';
                                option_string += '>';
                                option_string += e.sub_cat_name;
                                option_string += '</option>';
                            });
                        }
                        // console.log($this.parentsUntil('form').parent());
                        $('#sub_category_id').html('').append(option_string_default + ' ' + option_string);


                    });

        });
        $(document).on('click','#sub_category_id',function(){
            var id=$('#sub_category_id').val();

            $.post(host+'/admin/get_product_type_list/'+id,
                {
                    _token: "{{ csrf_token() }}",
                    // val: val
                },
                function(response){
                    //var category_data = response.data;
                    //console.log(response);
                    var option_string_default = '';
                    var option_string = '<option value="">Select Product Type</option>';
                    if(response!='') {
                        $.each(response, function (i, e) {
                            option_string += '<option ';
                            option_string += 'value="' + e.id + '" ';
                            option_string += '>';
                            option_string += e.pro_type_name;
                            option_string += '</option>';
                        });
                    }
                    // console.log($this.parentsUntil('form').parent());
                    $('#product_type_id').html('').append(option_string_default + ' ' + option_string);


                });

        });
        $(document).on('click','.delete_image',function(){
            if(confirm('Are You Sure ?'))
            {
                var id=$(this).data("id");
                //var val=$(this).data("value");
                $.get(host+'/admin/delete_product_image/'+id,
                    {
                        _token: "{{ csrf_token() }}",
                        //val: val
                    },
                    function(response){
                       $('#img_'+id).hide();

                    });
            }
        });
    </script>
@endsection