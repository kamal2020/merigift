@extends('admin::layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Brand List

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Brand</a></li>
        <li class="active">Brand List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Brand List</h3>

          <div class="box-tools pull-right">
              <a href="{{url('admin/brand')}}">
                  <i class="fa fa-plus"></i> <span>Add Brand</span>
              </a>
        </div>
        <div class="box-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif


            <div class="box-body">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Brand</th>
                        <th>Created_at</th>
                        <th>Action</th>
                    </tr>
                    @php $i=1;@endphp
                    @foreach($records as $class)
        <tr>
            <td>{{$i++}}</td>

            <td>
                {{$class->brand_name}}
            </td>
            <td>
                {{ $class->created_at }}
            </td>

            <td>
                @if($class->brand_status==='Y')
                    <a class="change_status" data-value="N" data-id="{{$class->id}}"><i class="fa fa-check"></i> </a>
                @else
                    <a class="change_status" data-value="Y" data-id="{{$class->id}}"><i class="fa fa-times"></i> </a>
                @endif
                <a class="" href="{{route('edit-brand',$class->id)}}"> <i class="fa fa-edit"></i> </a>
                <a class="delete" href="{{route('delete-brand',$class->id)}}"> <i class="fa fa-trash"></i> </a>
            </td>

        </tr>
    @endforeach

</table>

        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<script>
    $(document).on('click','.change_status',function(){
        if(confirm('Are You Sure ?'))
        {
            var id=$(this).data("id");
            var val=$(this).data("value");
            $.post('brand_status/'+id,
                {
                    _token: "{{ csrf_token() }}",
                    val: val
                },
                function(response){
                    location.reload();

                });
        }
    });
    $(document).on('click','.delete',function(event){
            event.preventDefault();
            var r=confirm("Are you sure you want to delete?");
            if (r==true)   {
                window.location = $(this).attr('href');
            }

    });
</script>
@endsection