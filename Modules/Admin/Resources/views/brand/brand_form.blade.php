@extends('admin::layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Brand page

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Brand</a></li>

      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">@if(isset($brand)) Edit @else Add @endif Brand</h3>


        </div>
        <div class="box-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            @if(isset($brand))
                    {{Form::model($brand,array('url'=>array('admin/update_brand',$brand->id),'id'=>'brand-form','class'=>'form-horizontal','enctype' => 'multipart/form-data'))}}
                    {{form::hidden('id',$brand->id,array('id'=>'brand-id'))}}
                @else
                    {{Form::open(array('url'=>'admin/insert_brand','id'=>'brand-form','class'=>'form-horizontal','enctype' => 'multipart/form-data'))}}
                    {{form::hidden('id',null,array('id'=>'brand-id'))}}
                @endif
        <div class="box-body">
                <div class="form-group">
                  {{Form::label('brand_name','Brand Name',array('class'=>'col-sm-2 control-label'))}}

                  <div class="col-sm-10">
                    {{Form::text('brand_name',null,array('class'=>'form-control','placeholder'=>'Enter Brand'))}}
                  </div>
                </div>
            <div class="form-group">
                {{Form::label('brand_image','Brand Image',array('class'=>'col-sm-2 control-label'))}}

                <div class="col-sm-10">
                    {{Form::file('brand_image',null,array('class'=>'form-control'))}}

                   </div>
                @if(isset($brand))
                    <img src="{{URL::asset('brands/'.$brand->brand_image)}}">
                @endif

            </div>
            <div class="form-group">
                {{Form::label('brand_logo','Brand Logo',array('class'=>'col-sm-2 control-label'))}}

                <div class="col-sm-10">
                    {{Form::file('brand_logo',null,array('class'=>'form-control'))}}

                </div>
                @if(isset($brand))
                    <img src="{{URL::asset('brands/'.$brand->brand_logo)}}">
                @endif
            </div>
              </div>
              <div class="box-footer pull-right">
                <button type="reset" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info ">Submit</button>
              </div>
           {{Form::close()}}
        </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection