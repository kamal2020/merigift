<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Registry_item extends Model
{
    public $table='registries_items';
    public function product(){
        return $this->belongsTo('App\Product');
    }
}