<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
class Sub_category extends Model
{
    //
    public $table='sub_categorys';
    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function product_type(){
        return $this->hasOne('App\Product_type');
    }

}