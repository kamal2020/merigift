<?php

namespace App\Services;
use App\SocialFacebookAccount;
use App\User;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialFacebookAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialFacebookAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialFacebookAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'first_name' => $providerUser->getName(),
                    'password' => bcrypt('password'),
                ]);
                $data=User::where('email', $providerUser->getEmail())->first(['id']);
                UserPlan::insert(['user_id'=>$data->id,'plan_id'=>1,'plan_type'=>'oneyear','date_from'=>date('Y-m-d')]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }
    //For Google
    public function createOrGetUserGoogle(ProviderUser $providerUser)
    {
        $account = SocialFacebookAccount::whereProvider('google')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialFacebookAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'google'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'first_name' => $providerUser->getName(),
                    'password' => bcrypt('password'),
                ]);
                $data=User::where('email', $providerUser->getEmail())->first(['id']);
                UserPlan::insert(['user_id'=>$data->id,'plan_id'=>1,'plan_type'=>'oneyear','date_from'=>date('Y-m-d')]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }
}