<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Cart extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function product(){
        return $this->belongsTo('App\Product','product_id','id');
    }
    public function registry(){
        return $this->belongsTo('App\Registry');
    }
    public function product_image(){
        return $this->hasOne('App\Product_image','product_id','id');
   }
}