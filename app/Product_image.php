<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Sub_category;
use App\Brand;
class Product_image extends Model
{
    //
    public $table='products_images';
    public function category(){
        return $this->belongsTo('App\Product');
    }

}