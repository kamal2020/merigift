<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class User_address extends Model
{
    public $table='user_address';

    protected $fillable = [
        'users_id', 'address_line1', 'address_line2','town','county','postcode','country','additional_info'
    ];

}