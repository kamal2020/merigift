<?php


namespace App\Http\Middleware;
use Closure;
use Auth;
use App\UserPlan;
class CheckPlan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(UserPlan::where('user_id',Auth::user()->id)->where('plan_status','Y')->where('date_to','>=',date('Y-m-d'))->orWhere('date_to',null)->first()){
            return $next($request);
            //return response()->json('Please enter valid type');
        }
        return redirect('/plan');


        
    }
}