<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Registry;
use App\Registry_item;
use App\Registry_type;
use App\Registries_question;
use App\Registries_question_answer;
use App\Payment;
use App\Cart;
use App\Product;
use App\User;
use Mail;
use Auth;


class RegistryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function get_registries_questions($id)
    {
        //dd($id);
        $data=Registries_question::where('registries_type',$id)->get();
        //dd($data);
        $desing='';
        if($data)
        {
            $id_array=array('5','12','19','27');
            $desing.='<div id="'.$data[0]->registries_type.'">';
            foreach ($data as $raw)
            {

                $desing.='<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    <label class="col-sm-12 control-label">'.$raw->question.'</label>
                    <div class="col-sm-12">';
                if($raw->type=='text')
                {
                    $desing.='<input type="text" required class="form-control" name="'.$raw->id.'">';
                }
                elseif($raw->type=='checkbox')
                {
                 $value=explode(',',$raw->value);

                 foreach ($value as $raw_val)
                 {
                     $desing.='<input type="checkbox" name="'.$raw->id.'" value="'.$raw_val.'">'.$raw_val;
                 }
                }
                elseif($raw->type=='radio')
                {
                    $value=explode(',',$raw->value);
                    if(in_array($raw->id,$id_array))
                    {
                        $class='hide';

                    }
                    else
                    {
                        $class='';
                    }
                    foreach ($value as $raw_val)
                    {

                        if($class)
                        {
                            $click="onClick=hide('".$raw_val."');";
                            $desing.='<input type="radio" class="'.$class.'" '.$click.' name="'.$raw->id.'" value="'.$raw_val.'">'.$raw_val;
                        }

                        else
                        {
                            $desing.='<input type="radio" class="'.$class.'"  name="'.$raw->id.'" value="'.$raw_val.'">'.$raw_val;
                        }

                    }
                }
                elseif($raw->type=='date')
                {
                    $desing.='<input type="date" required class="form-control datepicker" name="'.$raw->id.'">';
                }
                elseif($raw->type=='number')
                {
                    $desing.='<input type="number" required  class="form-control" name="'.$raw->id.'">';
                }
                else
                {

                }

                $desing.='</div></div>';
            }
            $desing.='</div>';

        }
        return ($desing);


    }
    public function registry(){
        
        $data['registry_type']=Registry_type::where('is_deleted','N')->pluck('title','id');
        return view('registry.registry_form',$data);
    }
    public function registry_list()
    {
        $data['registry']=Registry::where('is_deleted','N')->where('user_id',Auth::user()->id)->with('registryType')->get();
        //dd($data);
        return view('registry.registry_list',$data);
    }
    public function insert_registry(Request $request){
        //dd($request->all());
        $request_data=array(
            'user_id'=>Auth::user()->id,
            'unique_id'=>$this->randomString(),
            'registry_type_id'=>$request->registry_type_id,
            'registry_type_title'=>$request->registry_type_title,
            'name'=>$request->name,
            'date_from'=>$request->date_from,
            'date_to'=>$request->date_to,
            'no_of_guest'=>$request->no_of_guest,
            'delivery_address'=>$request->delivery_address,
            'delivery_city'=>$request->delivery_city,
            'delivery_state'=>$request->delivery_state,
            'delivery_pincode'=>$request->delivery_pincode,
            'delivery_date'=>$request->delivery_date,
            'mobile_no'=>$request->mobile_no,
            'email'=>$request->email,


        );
        $data_id=Registry::insertGetId($request_data);
        if($data_id)
        {
            $question=Registries_question::where('registries_type',$request->registry_type_id)->get();
            if($question)
            {
                foreach ($question as $raw)
                {
                    if(isset($_POST[$raw->id]))
                    {
                        $question_data=array(
                            'registries_id'=>$data_id,
                            'type'=>$request->registry_type_id,
                            'registries_questions_id'=>$raw->id,
                            'answers_value'=>$_POST[$raw->id],
                        );

                        $data=Registries_question_answer::insert($question_data);
                    }
                    else
                    {
                        $question_data=array(
                            'registries_id'=>$data_id,
                            'type'=>$request->registry_type_id,
                            'registries_questions_id'=>$raw->id,
                            'answers_value'=>'',
                        );

                        $data=Registries_question_answer::insert($question_data);
                    }
                }
                //dd($question_data);
            }
            return redirect('/registry-summery')->with('success','Registry Insert successfully!');
        }

        else
        {
            return redirect('/registry-summery')->with('error','Registry not insert!');
        }

    }
    public function edit_registry($id){
        $data['registry_type']=Registry_type::where('is_deleted','N')->pluck('title','id');
        $data['registry'] = Registry::find($id);
        $data['registry_answer'] = Registries_question_answer::where('registries_id',$id)->with('question')->get();

        //dd($data['registry_answer']);


        return view('registry.registry_form',$data);
    }
    public function update_registry(Request $request,$id)
    {
        $request_data=array(
            'registry_type_id'=>$request->registry_type_id,
            'registry_type_title'=>$request->registry_type_title,
            'name'=>$request->name,
            'date_from'=>$request->date_from,
            'date_to'=>$request->date_to,
            'no_of_guest'=>$request->no_of_guest,
            'delivery_address'=>$request->delivery_address,
            'delivery_city'=>$request->delivery_city,
            'delivery_state'=>$request->delivery_state,
            'delivery_pincode'=>$request->delivery_pincode,
            'delivery_date'=>$request->delivery_date,
            'mobile_no'=>$request->mobile_no,
            'email'=>$request->email,

        );
        $data=Registry::where('id',$id)->update($request_data);
        if($data)
        {
            Registries_question_answer::where('registries_id',$id)->delete();
            //$old_question_answer->delete();
            $question=Registries_question::where('registries_type',$request->registry_type_id)->get();
            if($question)
            {
                foreach ($question as $raw)
                {
                    if(isset($_POST[$raw->id]))
                    {
                        $question_data=array(
                            'registries_id'=>$id,
                            'type'=>$request->registry_type_id,
                            'registries_questions_id'=>$raw->id,
                            'answers_value'=>$_POST[$raw->id],
                        );

                        $data=Registries_question_answer::insert($question_data);
                    }
                    else
                    {
                        $question_data=array(
                            'registries_id'=>$id,
                            'type'=>$request->registry_type_id,
                            'registries_questions_id'=>$raw->id,
                            'answers_value'=>'',
                        );

                        $data=Registries_question_answer::insert($question_data);
                    }



                }
                //dd($question_data);
            }
            return redirect('registry_list')->with('success','Registry Updated!');
        }

        else
        {
            return redirect('registry_list')->with('error','Registry not Update!');
        }

    }
    public function delete_registry($id)
    {
        $data=Registry::where('id',$id)->update(['is_deleted'=>'Y']);
        if($data)
            return redirect('registry_list')->with('success','Registry Deleted!');
        else
            return redirect('registry_list')->with('error','Registry not delete!');
    }
    public function detail_registry($id=null,Request $request){
        $data=null;
        if($id==null){
            $id=$request->id;
            $category_id = $request->category_id;
            $price = $request->price;
            $query = Registry_item::where('registries_id',$id);
            if(($price=='all' || $price=='') && $category_id!=''){
                $query->whereIn('product_id',Product::where('category_id',$category_id)->pluck('id'));
            } else if(($price!='all' && $price!='')){
                $price_arr=explode('-',$price);
                $query->whereIn('product_id',Product::whereBetween('product_price',[$price_arr[0],$price_arr[1]])->pluck('id'));
                if($category_id!=''){
                    $query->whereIn('product_id',Product::where('category_id',$category_id)->pluck('id'));
                }
            }
            $registry=$query->with('product')->paginate(4)->setPath ('');   
        }
        else{
            $request->merge(['id'=>$id]);
            $registry = Registry_item::where('registries_id',$id)->with('product')->paginate(4)->setPath ('');
        }
        $product_id=Registry_item::where('registries_id',$id)->pluck('product_id');
        if(isset($product_id)){
            $data['product_min_price']=productMinPrice($product_id);
            $data['product_max_price']=productMaxPrice($product_id);
        }
        if(!empty($registry)){
            $pagination = $registry->appends ($request->except(['_token']));
            if (count ( $registry ) > 0)
                return view('registry.registry_detail')->withData($data)->withregistry($registry);
        } 
        return view('registry.registry_detail')->withData($data)->withregistry($registry);
    }
    public function registry_summery($id=null){
        $data['registry']=Registry::where('is_deleted','N')->where('user_id',Auth::user()->id)->pluck('name','id');
        return view('registry.registry_summary',$data);
    }
    public function registry_status($id){
        $data['registry']=Registry::where('is_deleted','N')->where('user_id',Auth::user()->id)->pluck('name','id');
        $data['registry_item'] = Registry_item::where('registries_id',$id)->with('product')->get();
        return view('registry.registry_status',$data);
    }
    public function registry_edit($id){
        $data['registry']=Registry::where('is_deleted','N')->where('user_id',Auth::user()->id)->pluck('name','id');
        $data['registry_item'] = Registry_item::where('registries_id',$id)->with('product')->get();
        //dd($data);
        return view('registry.registry_edit',$data);
    }
    public function registry_thankyou($id){
        $data['registry']=Registry::where('is_deleted','N')->where('user_id',Auth::user()->id)->pluck('name','id');
        $data['order_product']=Cart::whereIn('order_id',Payment::where('registry_id',$id)->where('status','Y')->get(['order_id']))
                        ->where('status','Y')->with('user','product','registry')->get();
        return view('registry.registry_thankyou',$data);
    }
    public function add_registry_item(Request $request){
        //dd($request);
        if(Registry_item::insert($request->except(['_token'])))
            return redirect()->back()->with('success','Product added to registry successfully!');
        else
            return redirect()->back()->with('error','Product not added to registry');
    }
    public function thankyou_mail(Request $request){

       // dd($request->all());
        if(isset($request->user_id))
        {
            $email=User::whereIn('id',$request->user_id)->distinct('email')->pluck('email');
        }
        $title = "Thank You";
        $content = $request->editordata;
        /* Mail::send('emails_body', ['title' => $title, 'content' => $content], function ($message)
        {
            $message->from('merigift008@gmail.com ', 'Meri Gift');
            $message->to('bhansalibhawana192@gmail.com');
        });  */

        dd($request->all());
    }

    
    function randomString(){
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 8)), 0, 8);
    }
}