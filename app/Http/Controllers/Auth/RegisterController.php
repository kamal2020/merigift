<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserPlan;
use App\Mail\Welcome;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use PHPMailerAutoload;
use PHPMailer;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
       $data= User::insertGetId([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'password' => bcrypt($data['password']),
            'referal_code'=>randomString(),
            'role'=>'User'
        ]);
        if($data){
            UserPlan::insert(['user_id'=>$data,'plan_id'=>1,'plan_type'=>'oneyear','date_from'=>date('Y-m-d')]);
            return User::where('id',$data)->first();
        }
       // auth()->login($user);
       // \Mail::to($user)->send(new Welcome);

//        $mail = new PHPMailer(true); // notice the \  you have to use root namespace here
//        try {
//            $mail->isSMTP(); // tell to use smtp
//            $mail->CharSet = "utf-8"; // set charset to utf8
//            $mail->SMTPAuth = true;  // use smpt auth
//            $mail->SMTPSecure = "tls"; // or ssl
//            $mail->Host = "concierge@luxtrips.tours";
//            $mail->Port = 25; // most likely something different for you. This is the mailtrap.io port i use for testing.
//            $mail->Username = "concierge@luxtrips.tours";
//            $mail->Password = "Iloveluxurytours999";
//            $mail->setFrom("concierge@luxtrips.tours", "Firstname Lastname");
//            $mail->Subject = "Test";
//            $mail->MsgHTML("This is a test");
//            $mail->addAddress("kamalps008@gmail.com", "Recipient Name");
//            $mail->send();
//        } catch (phpmailerException $e) {
//            dd($e);
//        } catch (Exception $e) {
//            dd($e);
//        }
//        die('success');

        //return redirect()->home();
    }
}
