<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Services\SocialFacebookAccountService;
use Auth;
class SocialAuthFacebookController extends Controller
{
    /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::with('facebook')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback(SocialFacebookAccountService $service)
    {

        //$user = $service->createOrGetUser(Socialite::driver('facebook')->stateless()->user());

        $user = $service->createOrGetUser(Socialite::driver('facebook')->stateless()->user());
        auth()->login($user);

        return redirect('/dashboard');
    }

    // gmail login
    public function googleRedirect()
    {
        return Socialite::with('google')->redirect();
    }


    public function googleCallback(SocialFacebookAccountService $service)
    {

        //$user = $service->createOrGetUser(Socialite::driver('facebook')->stateless()->user());

        $user = $service->createOrGetUserGoogle(Socialite::driver('google')->stateless()->user());
        auth()->login($user);
        return redirect('/dashboard');
    }


}