<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Plan;
use App\Feature;
use App\Registry_item;
use App\Registry;
use App\Cart;
use App\User;
use App\PlanFeature;
use App\GeneralSetting;
use App\ReferalDiscount;
use App\UserPlan;
use App\Product;
use Cookie;
use Auth,DB;
use App\DeliveryAddress;
use App\Payment;
use Illuminate\Routing\Controller;
use Illuminate\Cookie\CookieJar;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //check if exist cookie
        $value = Cookie::get('merigift');
        if(!$value)
        {
            Cookie::queue('merigift', randomString(), 45000);
        }
        return view('welcome');
    }
    public function referal_code($code){
        session(['referal_code' => $code]);
        return redirect('/plan');
    }

    public function plan()
    {
        $data['plan']=plan::where('plan_deleted','N')->get();
        $data['feature']=Feature::where('feature_deleted','N')->get();
        $data['planfeature']=PlanFeature::get();
        if(Auth::user())
        {
            $data['user']=UserPlan::where('user_id',Auth::user()->id)->where('plan_status','Y')->first();
            if($data['user']->date_to<date('Y-m-d')){
                return view('plan',$data)->withError('Your Plan has been expried please renew your Plan !');
            }
        }
        return view('plan',$data);
    }
    public function delete_cart($id)
    {
        $cart=Cart::find($id);
        $cart->delete();
        return back();
    }
    public function search_registry_form(){
        return view('registry.search_registry');
    }
    public function search_registry(Request $request){
        $unique_id = $request->unique_id;
        $category_id = $request->category_id;
        $price = $request->price;
        $data=null;
        $registry_item=null;
        if($unique_id!='')
        {
            $reg_unique_id = Registry::where('unique_id',$unique_id)->where('date_to','>=',date('Y-m-d'))->first(['id']);
            if($reg_unique_id!=''){    
                $product_id=Registry_item::where('registries_id',$reg_unique_id->id)->pluck('product_id');
                    $data['product_min_price']=productMinPrice($product_id);
                    $data['product_max_price']=productMaxPrice($product_id);
                $query = Registry_item::where('registries_id',$reg_unique_id->id);
                if(($price=='all' || $price=='') && $category_id!=''){
                    $query->whereIn('product_id',Product::where('category_id',$category_id)->pluck('id'));
                } else if(($price!='all' && $price!='')){
                    $price_arr=explode('-',$price);
                    $query->whereIn('product_id',Product::whereBetween('product_price',[$price_arr[0],$price_arr[1]])->pluck('id'));
                    if($category_id!=''){
                        $query->whereIn('product_id',Product::where('category_id',$category_id)->pluck('id'));
                    }
                }
                $registry_item=$query->with('product')->paginate(4)->setPath ('');
                //dd($registry_item);
                if(!empty($registry_item)){
                    $pagination = $registry_item->appends ( $request->except(['_token']));
                    if (count ( $registry_item ) > 0)
                        return view('registry.registry_list_ref')->withData($data)->withDetails($registry_item);
                } 
            }
        }
        return view('registry.registry_list_ref')->withData($data)->withDetails($registry_item);   
    }

    public function insert_cart(Request $request){
        if(Auth::user())
            $request->merge(['user_id'=>Auth::user()->id,'date'=>date('Y-m-d')]);
        else{
            $c_value = Cookie::get('merigift');
            $request->merge(['user_id'=>$c_value,'date'=>date('Y-m-d')]);
        }
        $check_array=Cart::where(['product_id'=>$request->product_id,
                                'registry_id'=>$request->registry_id,
                                'user_id'=>$request->user_id,
                                'status'=>'N'])
                            ->first();
        if($check_array)
        {
            $data=Cart::where(['product_id'=>$request->product_id,'registry_id'=>$request->registry_id,'user_id'=>$request->user_id])
                        ->update(['quantity'=>$request->quantity]);
        }
        else{
            $chkCart=Cart::where('user_id',$request->user_id)->where('status','N')->first();
            if(!$chkCart || $chkCart->registry_id==$request->registry_id)
                $data=Cart::insert($request->except(['_token','unique_id']));
            else
                return redirect()->back()->with('error','You have already added another registry in your Cart');;
        }
        return redirect()->back()->with('success','Product inserted in Cart!');
    }

    public function plan_detail(Request $request){
        //session()->forget('referal_code');
        $data['plan']=Plan::where('id',$request->plan_id)->first();
        $data['plan_type']=$request->plan_type;
        if(session('referal_code'))
        {
            $data['user']=User::where('referal_code',session('referal_code'))->where('id','!=',Auth::user()->id)->first();
            if($data['user'])
            {
                $data['setting']=GeneralSetting::first();
            }
        }
        return view('plan.plan_detail',$data);
    }
    public function buy_plan(Request $request){
        $user_plan['user_id']=Auth::user()->id;
        $user_plan['plan_id']=$request->plan_id;
        $user_plan['plan_type']=$request->plan_type;
        $user_plan['date_from']=date('Y-m-d');
        $user_plan['amount']=$request->amount;
        $user_plan['plan_status']='Y';
        $user_plan['discount']=$request->discount;
        $user_plan['paid_amount']=$request->paid_amount;

        if($request->plan_type=='oneyear')
            $user_plan['date_to']=date('Y-m-d', strtotime('+1 years'));
        else
            $user_plan['date_to']=null;
        
        if(isset($request->referal_user_id))
        {
            ReferalDiscount::insert(['referal_user_id'=>$request->referal_user_id,
                            'user_id'=>Auth::user()->id,
                            'amount'=>$request->discount,
                            'date'=>date('Y-m-d'),
                            'type'=>'Cr'
                            ]);
        }
        UserPlan::where('user_id',Auth::user()->id)->update(['plan_status'=>'N']);
        Payment::insert(['user_id'=>Auth::user()->id,
                        'order_id'=>randomString(),
                        'payment_type'=>'Plan',
                        'currency'=>'$',
                        'discount'=>$request->discount,
                        'amount'=>$request->amount,
                        'paid_amount'=>$request->paid_amount,
                        'date'=>date('Y-m-d'),
                        'status'=>'Y'
                        
            ]); 
        session()->forget('referal_code');
     
        if(UserPlan::insert($user_plan))
            return redirect('plan')->with('success','Plan Purchased!');
        else
            return redirect('plan')->with('error','Plan not Purchased!');   

    }

    public function cart_check_out()
    {
        $user_id=Auth::user()->id;
        $cart=Cart::where('status','N')->where('user_id',$user_id)->with('product','product_image')->get();

        if(count($cart)>0)
        {
            $data['cart']=$cart;
            $data['profile']=User::where('id',Auth::user()->id)->with('user_address')->first();

            $data['credit']=ReferalDiscount::where('referal_user_id',$user_id)->where('type','Cr')->sum('amount');
            $data['debit']=ReferalDiscount::where('referal_user_id',$user_id)->where('type','Dr')->sum('amount');
            return view('check_out',$data);
        }
        else
        {
            return redirect('home');
        }

    }
    public function proceed(Request $request){
        //dd($request->quantity[0]);
        //$request->merge(['cart_id'=>implode(",",$request->cart_ids)]);
        $request->merge(['user_id'=>Auth::user()->id]);
        if(isset($request->referal_amount)){
            ReferalDiscount::insert(['referal_user_id'=>Auth::user()->id,
                            'amount'=>$request->use_referal,
                            'date'=>date('Y-m-d'),
                            'type'=>'Dr']);
        }
        $order_id=randomString();
        $request->merge(['order_id'=>$order_id]);
        DeliveryAddress::insert($request->except(['_token','registry_id','total','use_referal','paid_amount','cart_ids','product_ids','quantity','referal_amount']));
        
        if(Payment::insert(['user_id'=>Auth::user()->id,
                        'order_id'=>$order_id,
                        'registry_id'=>$request->registry_id,
                        'payment_type'=>'Product',
                        'amount'=>$request->total,
                        'referal_amount'=>$request->use_referal,
                        'paid_amount'=>$request->paid_amount,
                        'currency'=>'$',
                        'date'=>date('Y-m-d'),
                        'status'=>'Y'
                    ]))
        {
            foreach($request->cart_ids as $val){
                Cart::where('id',$val)->update(['order_id'=>$order_id,'status'=>'Y']);
                   }
            ///update registry item buy/////
            $i=0;
            foreach ($request->product_ids as $raw)
            {
                $registry_item= Registry_item::where('registries_id',$request->registry_id)->where('product_id',$raw)->first();
                $received=$registry_item->received+$request->quantity[$i];
                Registry_item::where('registries_id',$request->registry_id)->where('product_id',$raw)->update(['received'=>$received]);
                $i++;
            }
        }
        return redirect('payment-success/'.$order_id);
    }
    public function payment_success($order_id)
    {
        $data['payment']=Payment::where('order_id',$order_id)->first();
        $data['cart']=Cart::where('order_id',$order_id)->with('product','product_image')->get();
        return view('payment_success',$data);
    }
}