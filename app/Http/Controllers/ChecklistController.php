<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Registry;
use App\Checklist;
use Auth;

class ChecklistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function add_checklist(){
        $data['registry']=Registry::where('user_id',Auth::user()->id)->pluck('name','id');
        return view('checklist.checklist_form',$data);
    }
    public function insert_checklist(Request $request){
        $data=Checklist::insert($request->except(['_token']));
        if($data)
            return redirect('check_list')->with('success','Checklist insert!');
        else
            return redirect('check_list')->with('error','Checklist not insert!');
    }
    public function check_list()
    {
        $data['checklist']=Checklist::where('is_deleted','N')
            ->whereIn('registry_id',Registry::where('user_id',Auth::user()->id)->get(['id']))
            ->with('registry')
            ->get();
        return view('checklist.check_list',$data);
    }
    public function edit_checklist($id){
        $data['registry']=Registry::where('user_id',Auth::user()->id)->pluck('name','id');
        $data['checklist'] = Checklist::find($id);
        return view('checklist.checklist_form',$data);
    }
    public function update_checklist(Request $request,$id)
    {
        $data=Checklist::where('id',$id)->update($request->except(['_token']));
        if($data)
            return redirect('check_list')->with('success','Checklist Updated!');
        else
            return redirect('check_list')->with('error','Checklist not Update!');
    }
    public function delete_checklist($id)
    {
        $data=Checklist::where('id',$id)->update(['is_deleted'=>'Y']);
        if($data)
            return redirect('check_list')->with('success','Checklist Deleted!');
        else
            return redirect('check_list')->with('error','Checklist not delete!');
    }

    function randomString(){
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 8)), 0, 8);
    }
}