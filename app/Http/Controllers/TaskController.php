<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Registry;
use App\Checklist;
use App\Task;
use Auth;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function task_list($id)
    {
        $data['task']=Task::where('is_deleted','N')
            ->where('checklist_id',$id)
            ->with('checklist')
            ->get();
        $data['id']=$id;
        return view('task.task_list',$data);
    }

     public function add_task($id){
         $data['id']=$id;
        return view('task.task_form',$data);
    }
    public function insert_task(Request $request,$id){
         $insert=array('checklist_id'=>$id,'name'=>$request->name);

        $data=task::insert($insert);
        if($data)
            return redirect('task-list/'.$id)->with('success','task insert!');
        else
            return redirect('task-list/'.$id)->with('error','task not insert!');
    }
    
    public function edit_task($id){

        $data['task'] = task::find($id);
        return view('task.task_form',$data);
    }
    public function update_task(Request $request,$id)
    {
        $data=task::where('id',$id)->update($request->except(['_token']));
        if($data)
            return redirect('edit-task/'.$id)->with('success','task Updated!');
        else
            return redirect('edit-task/'.$id)->with('error','task not updated!');
    }
    public function delete_task($id)
    {
        $data=Task::where('id',$id)->update(['is_deleted'=>'Y']);
        if($data)
            return back()->with('success','Task Deleted!');
        else
            return back()->with('error','Task not delete!');
    }
}