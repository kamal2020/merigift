<?php

namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    
    public function __construct()
    {
        //$this->middleware('auth');

    }
    public function category_list()
    {
        $data=(Category::all());
        return view('admin.category_list')->with('records',$data);
    }
    public function category()
    {
        return view('admin.category_form');
    }
    public function insert_category(Request $request)
    {

         Category::create([
            'cat_name' => $request['category'],
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
                    ]);
        //dd($request->all());
        return back()->with('success','Category created successfully!');
        //return redirect()->back();

    }

    public function edit_category(Request $request){

        $category = Category::find($request->id);
        //return view::make('admin.category_form')->with('category', $category);
        return view('admin.category_edit')->with('records',$category);


//        $category->title = 'HEAVY METAL';
//        $category->save();


    }
    public function update_category(Request $request){

        $category = Category::find($request->id);
        $category->cat_name = $request->category;
        $category->save();
        //return view::make('admin.category_form')->with('category', $category);
        return view('admin.category_edit')->with('records',$category);


//        $category->title = 'HEAVY METAL';
//        $category->save();


    }

}
