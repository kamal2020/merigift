<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\User_address;
use App\ReferalDiscount;
use Validator;
use Auth;
use Hash;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /////////////category methods///////////////////////////
    public function change_password()
    {
      return view("user.change_password");
    }

    //Start Change Password
    public function update_password(Request $request)
    {
        if (!(Hash::check($request->get('old_password'), Auth::user()->password))) {
            return redirect()->back()->with("error","Old Password does not match. Please try again.");
        }
        if(strcmp($request->get('old_password'), $request->get('new_password')) == 0){
            return redirect()->back()->with("error","New Password cannot be same as your current password.");
        }
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return back()->with('success','Password Updated!');
    }

    //Start admin profile
    public function profile()
    {
        //dd(Auth::user());
        $data['profile']=User::where('id',Auth::user()->id)->with('user_address')->first();
        //dd($data);
        return view("user.change_profile",$data);
    }
    public function update_profile(Request $request,$id)
    {
        $validation = Validator::make( $request->all(), [
            'name' => 'required',
            'contact_number' => 'required',
            'address_line1' => 'required',
            'address_line2' => 'required',
            'town' => 'required',
            'county' => 'required',
            'postcode' => 'required',
            'country' => 'required',
            'product_image' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);
        if($validation->fails() ) {
            return \Redirect::back()->withInput()->withErrors( $validation->messages() );
        }

        $image = $request->file('image');
        //dd($image);
        if($image) {
            $input['image'] = time() .'.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/user_image');
            $image->move($destinationPath, $input['image']);

        }
        $input['name']=$request->name;
        $input['contact_number']=$request->contact_number;

        $data=User::where('id',$id)->update($input);
        if($data)
        {
            $user_address = User_address::updateOrCreate(
                ['users_id' => $id],
                [
                    'address_line1' => $request->address_line1,
                    'address_line2' => $request->address_line2,
                    'town' => $request->town,
                    'county' => $request->county,
                    'postcode' => $request->postcode,
                    'country' => $request->country,
                    'additional_info' =>$request->additional_info,

                    ]
            );

            return redirect('profile')->with('success','Profile Updated successfully!');
        }
        else
        { return redirect('profile')->with('error','Profile Not Updated, Please Try Again!'); }



    }

    public function referral_list()
    {
        $user=User::where('id',Auth::user()->id)->with('user_address')->first(['referal_code']);
        $data['referral_code']=$user->referal_code;
        $data['referral']=ReferalDiscount::where('referal_user_id',Auth::user()->id)->get();
        //dd($data);
        return view("user.referal_list",$data);
    }
}
