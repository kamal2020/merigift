<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Sub_category;
use App\Product_type;
use App\Brand;
use App\Registry;
use Auth;

class ProductController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth');
    }
    /////////product listing by product type/////////
    public function product($type_id=null,Request $request)
    {
        $product=null;
        if($type_id==null){
            $brand_id = $request->brand_id;
            $price = $request->price;
            $id=$type_id=$request->id;
            $query=Product::where('product_deleted','N')->where('product_type_id',$id);
            if($brand_id!='')
                $query->where('brand_id',$brand_id);
            if($price!='all' && $price!=''){
                $price_arr=explode('-',$price);
                $query->whereBetween('product_price',[$price_arr[0],$price_arr[1]]);
            }
            $product=$query->with('category','sub_category','brand','product_type','product_image')->paginate(4)->setPath('');
        }
        else{
            $request->merge(['id'=>$type_id]);
            $product=Product::where('product_deleted','N')
                ->where('product_type_id',$type_id)
                ->with('category','sub_category','brand','product_type','product_image')
                ->paginate(4)->setPath('');   
        }
        $product_id=Product::where('product_deleted','N')->where('product_type_id',$type_id)->pluck('id');
        if($product_id){
            $data['product_min_price']=productMinPrice($product_id);
            $data['product_max_price']=productMaxPrice($product_id);
        }
        $data['registry']=array();
        if(Auth::user())
            $data['registry']=Registry::where('is_deleted','N')->where('user_id',Auth::user()->id)->pluck('name','id');

        if(!empty($product)){
            $pagination = $product->appends($request->except(['_token']));
            if (count ( $product ) > 0)
                return view('product.product_list',$data)->withproduct($product);
        }
        return view('product.product_list',$data)->withproduct($product);
    }

    //////////////product listing by brand//////////
    public function product_brand()
    {
        $data['brand']=Brand::where('brand_deleted','N')->where('brand_status','Y')->get();
        return view('product.brand_list',$data);
    }
    public function product_by_brand($type_id=null,Request $request)
    {
        $product=null;
        if($type_id==null){
            $category_id = $request->category_id;
            $price = $request->price;
            $id=$type_id=$request->id;
            $query=Product::where('product_deleted','N')->where('brand_id',$type_id);
            if($category_id!='')
                $query->where('category_id',$category_id);
            if($price!='all' && $price!=''){
                $price_arr=explode('-',$price);
                $query->whereBetween('product_price',[$price_arr[0],$price_arr[1]]);
            }
            $product=$query->with('category','sub_category','brand','product_type','product_image')->paginate(4)->setPath('');
        }
        else{
            $request->merge(['id'=>$type_id]);
            $product=Product::where('product_deleted','N')
                ->where('brand_id',$type_id)
                ->with('category','sub_category','brand','product_type','product_image')
                ->paginate(4)->setPath('');
        }
        $product_id=Product::where('product_deleted','N')->where('brand_id',$type_id)->pluck('id');
        if($product_id){
            $data['product_min_price']=productMinPrice($product_id);
            $data['product_max_price']=productMaxPrice($product_id);
        }
        $data['registry']=array();
        if(Auth::user())
            $data['registry']=Registry::where('is_deleted','N')->where('user_id',Auth::user()->id)->pluck('name','id');

        if(!empty($product)){
            $pagination = $product->appends($request->except(['_token']));
            if (count ( $product ) > 0)
                return view('product.product_list_brand',$data)->withproduct($product);
        }
        return view('product.product_list_brand',$data)->withproduct($product);

    }

    public function product_details($id)
    {
        $data['registry']=array();
        if(Auth::user())
            $data['registry']=Registry::where('is_deleted','N')->where('user_id',Auth::user()->id)->pluck('name','id');

        $data['details']=Product::where('product_deleted','N')->where('id',$id)
            ->with('category','sub_category','brand','product_type','product_image_all')
            ->get()->first();
        return view('product.product_details',$data);
    }
    

}