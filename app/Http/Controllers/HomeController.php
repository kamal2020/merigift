<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use Cookie;
use Auth;
use Illuminate\Routing\Controller;
use Illuminate\Cookie\CookieJar;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('welcome');
    }
    public function index()
    {
        $temp_id = Cookie::get('merigift');
        if($temp_id)
        {
            $cart_registry='';
            $cart_product=array();
            $cookie_registry='';
            $cookie_product=array();
            //////get already cart items by user id/////

            $user_id=Auth::user()->id;
            $cart_old=Cart::where('status','N')->where('user_id',$user_id)->get();
            if(count($cart_old)>0)
            {

                foreach($cart_old as $raw)
                {
                    $cart_registry=$raw->registry_id;
                    $cart_product[]=$raw->product_id;

                }

                ////get current cart by cookies
                $cart_by_cookies=Cart::where('status','N')->where('user_id',$temp_id)->get();

                if($cart_by_cookies)
                {
                    foreach ($cart_by_cookies as $raw_cookie)
                    {
                        $cookie_registry=$raw_cookie->registry_id;
                        $cookie_product[]=$raw_cookie->product_id;

                    }
                }

                if($cookie_registry!='' && $cart_registry==$cookie_registry)
                {
                    //////delete old cart item if same as cookies item//////
                    foreach ($cookie_product as $raw)
                    {
                        $collection = Cart::where('user_id',Auth::user()->id)->where('registry_id',$cart_registry)->where('product_id',$raw)->get(['id']);
                        Cart::destroy($collection->toArray());

                    }
                    ////////update cart cookies to user id////////////
                    $request=array('user_id'=>Auth::user()->id);
                    $data=Cart::where('user_id',$temp_id)->update($request);
                }
            }
            else
            {

                $request=array('user_id'=>Auth::user()->id);
                $data=Cart::where('user_id',$temp_id)->update($request);
            }
        }

        return view('home');
    }
    

}
