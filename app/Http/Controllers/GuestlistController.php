<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Registry;
use App\Guest_list;
use App\Checklist;
use Auth;
use Excel;

class GuestlistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function add_guest_list(){
        $data['registry']=Registry::where('user_id',Auth::user()->id)->pluck('name','id');
        return view('guest_list.guest_form',$data);
    }
    public function insert_guestlist(Request $request){
        $data=Guest_list::insert($request->except(['_token']));
        if($data)
            return redirect('guest-list')->with('success','Guest list insert!');
        else
            return  redirect('guest-list')->with('error','Guest list not insert!');
    }
    public function guest_list($id=null)
    {
        $data['registry']=Registry::where('user_id',Auth::user()->id)->pluck('name','id');
        $data['quest_list']=array();
        if($id){
            $data['quest_list']=Guest_list::where('registry_id',$id)->where('is_deleted','N')->with('registry')->get();
        }
        $data['quest_list']=array();
        if($id){
            $data['quest_list']=Guest_list::where('registry_id',$id)->with('registry')->get();
        }
        return view('guest_list.guest_list',$data);
    }
    public function edit_guestlist($id){
        $data['registry']=Registry::where('user_id',Auth::user()->id)->pluck('name','id');
        $data['questList'] = Guest_list::find($id);
        return view('guest_list.guest_form',$data);
    }
    public function update_guestlist(Request $request,$id)
    {
        $data=Guest_list::where('id',$id)->update($request->except(['_token']));
        if($data)
            return redirect('guest-list')->with('success','guest-list Updated!');
        else
            return redirect('guest-list')->with('error','guest-list not Update!');
    }
    public function delete_guestlist($id)
    {
        $data=Guest_list::where('id',$id)->update(['is_deleted'=>'Y']);
        if($data)
            return redirect('guest-list')->with('success','Checklist Deleted!');
        else
            return redirect('guest-list')->with('error','Checklist not delete!');
    }

    public function guestUploadForm(){
        $data['registry']=Registry::where('user_id',Auth::user()->id)->pluck('name','id');
        return view('guest_list.multiple_guest_form',$data);
    }

    public function insertGuestUpload(Request $request){
        if($request->hasFile('import_file'))
        {
            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {})->get();
            if(!empty($data) && $data->count()){
                $records=null;
                foreach ($data->toArray() as $key => $value) 
                {
                    if(!empty($value))
                    {
                        if($value['first_name']!='' && $value['last_name']!='' && $value['adults_guests']!='' && $value['kids_guests']!='' && $value['email']!='' && $value['mobile']!='' && $value['mobile']!='guest_location'){
                            if (filter_var($value['email'], FILTER_VALIDATE_EMAIL) && is_numeric($value['adults_guests']) && is_numeric($value['kids_guests'])) {
                                $insert = [
                                    'registry_id' => $request->registry_id,
                                    'guest_name' => $value['first_name'].' '.$value['last_name'],
                                    'email' => $value['email'],
                                    'phone' => $value['mobile'],
                                    'address' => $value['guest_location'],
                                    'no_of_people' => $value['adults_guests']+$value['kids_guests']
                                ];
                                Guest_list::updateOrCreate(['registry_id' => $request->registry_id, 'email' => $value['email']], $insert);
                            }
                            else
                            {
                                $records[]= "Record with name : " .$value['first_name'].' '.$value['last_name'] .' have some issue';
                            }
                        }
                        else{
                            $records[]= "Record with name : " .$value['first_name'].' '.$value['last_name'] .' have some issue';
                        }                      
                    }
                }
                return back()->with(['success'=>'Insert Record successfully.','data'=>$records]);
            }
        }
    }

    function download_sample()
    {
        $path = public_path('SampleXLSFile_19kb.xls');
        return response()->download($path);
    }

    function randomString(){
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 8)), 0, 8);
    }
}