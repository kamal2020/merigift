<?php
use App\Category;
use App\Cart;
use App\Product;
use App\Brand;
use App\Registry;
use App\Message;
use App\Registry_item;

function getCategory(){
  return Category::with('sub_category','product_type')->where('cat_status','Y')->where('cat_deleted','N')->get();
}
function category(){
    return Category::where('cat_status','Y')->where('cat_deleted','N')->pluck('cat_name','id');
}
function brand(){
    return Brand::where('brand_status','Y')->where('brand_deleted','N')->pluck('brand_name','id');
}
function filePath($path,$file){
    if(File::exists(public_path($path."/".$file))){
        return url($path."/".$file);
    }else{
        return url('no_image.png');
    }
}
function dateFormat($date){
    return date('Y-m-d',strtotime($date));
}
function randomString(){
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return substr(str_shuffle(str_repeat($pool, 8)), 0, 8);
}

function cart_count()
{
    if(!Auth::user())
        $user_id=Cookie::get('merigift');
    else
        $user_id=Auth::user()->id;
    $count=Cart::where('status','N')->where('user_id',$user_id)->count();
    return $count;
}
function cart_list()
{
    if(!Auth::user())
        $user_id=Cookie::get('merigift');
    else
        $user_id=Auth::user()->id;
    $cart_list=Cart::where('status','N')->where('user_id',$user_id)->with('product','product_image')->get();
    //dd($cart_list);
    //dd(DB::getQueryLog());
    return $cart_list;
}
function productMinPrice($product_id){
    return Product::whereIn('id',$product_id)->min('product_price');
}
function productMaxPrice($product_id){
   return Product::whereIn('id',$product_id)->max('product_price');
}
function priceChk($min,$max){
    $price_array=array();
    if($min!=null && $max!=null){
        $price_array["0-".$min]="0 To ".$min;
        $divide=round($max/4);
        while($min < $max){
            $old=$min;
            $min=$min+$divide;  
            $price_array[$old+1 ."-".$min]=$old+1 ." To ".$min; 
        }
    }
    return $price_array;
}
function get_title_registry($id){
    $data= Registry::where('unique_id',$id)->first(['name']);
    if($data)
        return $data->name;
    else
        return '';
}
function cart_get_product_image($id){
    $data= \App\Product_image::where('product_id',$id)->first(['product_image']);
    if($data)
        return $data->product_image;
    else
        return '';
}
function message(){
    return Message::pluck('title','msg');
}
function count_registry($type,$id)
{
    if ($id) {
        if ($type == 'total') {
            $total = Registry_item::where('is_deleted', 'N')->where('registries_id', $id)->count();
            if ($total > 0)
                return $total;
            else
                return 0;
        }
        if ($type == 'bought') {
            $total_bought = Registry_item::where('is_deleted', 'N')->where('registries_id', $id)->whereColumn('qty', 'received')->count();
            if ($total_bought > 0)
                return $total_bought;
            else
                return 0;
        }
        if ($type == 'total_val') {
            $result = Registry_item::where('is_deleted', 'N')->where('registries_id', $id)->with('product')->get();
            if (count($result) > 0) {
                $total_val = 0;
                foreach ($result as $raw) {
                    $total_val = $total_val + $raw->qty * $raw->product->product_price;
                }
                return $total_val;
            } else
                return 0;
        }
        if ($type == 'total_contributed') {
            $result = Registry_item::where('is_deleted', 'N')->where('registries_id', $id)->with('product')->get();
            if (count($result) > 0) {
                $total_val = 0;
                foreach ($result as $raw) {
                    $total_val = $total_val + $raw->received * $raw->product->product_price;
                }
                return $total_val;
            } else
                return 0;
        }


    }
    else
    {
        return 0;
    }
}
?>