<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class PlanFeature extends Model
{
    protected $table="plan_features";
    public function plan(){
        return $this->belongsTo('App\Plan');
    }
    public function feature(){
        return $this->belongsTo('App\Feature');
    }
}