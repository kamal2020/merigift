<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Category extends Model
{

    public $table='categorys';
    protected $fillable = [
        'cat_name'
    ];
    public function sub_category(){
        return $this->hasMany('App\Sub_category');  
        //return $this->hasMany('App\Sub_category')->where('sub_cat_deleted','N')->where('sub_cat_status','Y');
    }
    public function product_type(){
        return $this->hasMany('App\Product_type');
        //return $this->hasMany('App\Product_type')->where('pro_type_deleted','N')->where('pro_type_status','Y');
    }
}
