<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
class Product_type extends Model
{
    //
    public $table='product_types';
    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function sub_category(){
        return $this->belongsTo('App\Sub_category');
    }
}