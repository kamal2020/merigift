<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Sub_category;
use App\Brand;

class Product extends Model
{
    //
    public $table='products';
    public $with='category';

    //public $with1='product_image';
    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function sub_category(){
        return $this->belongsTo('App\Sub_category');
    }
    public function brand(){
        return $this->belongsTo('App\Brand');
    }
    public function product_type(){
        return $this->belongsTo('App\Product_type');
    }
    public function product_image(){
        return $this->hasOne('App\Product_image');
    }
    public function product_image_all(){
        return $this->hasMany('App\Product_image');
    }

}