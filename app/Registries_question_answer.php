<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Registries_question_answer extends Model
{
    public function question(){
        return $this->belongsTo('App\Registries_question','registries_questions_id','id');
    }
}