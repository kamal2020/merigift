<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Checklist extends Model
{
    public function registry(){
        return $this->belongsTo('App\Registry');
    }
}