<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Plan extends Model
{
    public function planfeature(){
        return $this->hasMany('App\PlanFeature');
    }
}