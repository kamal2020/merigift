<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Guest_list extends Model
{
    protected $fillable=['registry_id','guest_name','email','phone','address','no_of_people'];
    public function registry(){
        return $this->belongsTo('App\Registry');
    }
}