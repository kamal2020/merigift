<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'IndexController@index')->name('index');
Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');

Route::get('/google-redirect', 'SocialAuthFacebookController@googleRedirect');
Route::get('/google-callback', 'SocialAuthFacebookController@googleCallback');
Route::get('/plan','IndexController@plan')->name('plan');
Route::post('/plan-detail','IndexController@plan_detail')->name('plan-detail');
Route::post('/buy-plan','IndexController@buy_plan')->name('buy-plan');
Route::get('/dashboard', 'HomeController@index')->name('home');
    /*Route::get('/dashboard', function(){
        return view('home');
    });*/

Route::group(['middleware' => ['CheckPlan']], function()
{

    Route::any('/product-list/{id?}','ProductController@product');
    Route::get('/product-details/{id}','ProductController@product_details');
    Route::get('/brand','ProductController@product_brand')->name('brand');
    Route::any('/product-brand/{id?}','ProductController@product_by_brand')->name('brand-p');
    Route::get('/delete_cart/{id}','IndexController@delete_cart');
    Route::get('/search-registry-page','IndexController@search_registry_form');
    Route::any('/search-registry/{unique_id?}','IndexController@search_registry')->name('search-registry');
    Route::any('/search-registry-ref/{unique_id?}','IndexController@search_registry_ref')->name('search-registry-ref');
    Route::post('/insert-cart','IndexController@insert_cart')->name('insert-cart');
    Route::get('/referral-code/{code}','IndexController@referal_code')->name('referral-code');
    Route::get('/referral-list','UserController@referral_list')->name('referral-list');

    Route::get('sendemail', function () {
        $data = array(
            'name' => "Learning Laravelsss",
        );
        Mail::send('welcome', $data, function ($message) {
            $message->from('merigift008@gmail.com', 'Learning Laravel');
            $message->to('kamalps008@gmail.com')->subject('Learning Laravel test email');
        });
        return "Your email has been sent successfully";
    });
    
    Route::group(['middleware' => ['web','auth','CheckUser']], function()
    {
        
        Route::get('/get_registries_questions/{id}','RegistryController@get_registries_questions');
        Route::get('/registry_list','RegistryController@registry_list');
        Route::get('/registry','RegistryController@registry');
        Route::post('/insert_registry','RegistryController@insert_registry');
        Route::get('/edit_registry/{id}','RegistryController@edit_registry');
        Route::post('/update_registry/{id}','RegistryController@update_registry');
        Route::get('/delete_registry/{id}','RegistryController@delete_registry');
        Route::any('/detail_registry/{id?}','RegistryController@detail_registry');
        Route::get('/registry-summery/{id?}','RegistryController@registry_summery');
        Route::get('/registry-status/{id}','RegistryController@registry_status');
        Route::get('/registry-edit/{id}','RegistryController@registry_edit');
        Route::get('/registry-thankyou/{id}','RegistryController@registry_thankyou');
        Route::post('/add-registry-item','RegistryController@add_registry_item')->name('add-registry-item');
        Route::post('/thankyou-mail','RegistryController@thankyou_mail')->name('thankyou-mail');

        Route::get('/check_list','ChecklistController@check_list');
        Route::get('/add_checklist','ChecklistController@add_checklist');
        Route::post('/insert_checklist','ChecklistController@insert_checklist');
        Route::get('/edit_checklist/{id}','ChecklistController@edit_checklist');
        Route::post('/update_checklist/{id}','ChecklistController@update_checklist');
        Route::get('/delete_checklist/{id}','ChecklistController@delete_checklist');

        Route::get('task_list/{id}','TaskController@task_list');
        Route::get('task-list/{id}','TaskController@task_list');
        Route::get('/add-task/{id}','TaskController@add_task');
        Route::post('/insert_task/{id}','TaskController@insert_task');
        Route::get('/edit-task/{id}','TaskController@edit_task');
        Route::post('/update_task/{id}','TaskController@update_task');

        Route::get('/delete_task/{id}','TaskController@delete_task');
        Route::get('/change_password','HomeController@change_password')->name('change-password');

        Route::get('change_password','UserController@change_password')->name('change-password');
        Route::post('update_password','UserController@update_password');
        Route::get('/profile','UserController@profile')->name('profile-update');
        Route::post('/update_profile/{id}','UserController@update_profile');

        Route::get('/home', 'HomeController@index')->name('home');

        Route::get('/guest-list/{id?}','GuestlistController@guest_list');
        Route::get('/guest_list_ajax/{id}','GuestlistController@guest_list_ajax');
        Route::get('/add-guest-list','GuestlistController@add_guest_list');
        Route::post('/insert_guestlist','GuestlistController@insert_guestlist');
        Route::get('/edit-guestlist/{id}','GuestlistController@edit_guestlist');
        Route::post('/update_guestlist/{id}','GuestlistController@update_guestlist');
        Route::get('/delete_guestlist/{id}','GuestlistController@delete_guestlist');

        Route::post('/insert-guest-upload','GuestlistController@insertGuestUpload');
        Route::get('/guest-upload','GuestlistController@guestUploadForm');
        Route::get('/download-sample','GuestlistController@download_sample');

        
        Route::get('/check-out','IndexController@cart_check_out')->name('check-out');
        Route::post('/proceed-checkout','IndexController@proceed')->name('proceed-checkout');
        Route::get('/payment-success/{id}','IndexController@payment_success')->name('payment-success');
    });
});