<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo e(config('app.name', 'MeriGift')); ?></title>
    <!-- Plugins CSS -->

    <!-- <link href="<?php echo e(URL::asset('css/bootstrap.min.css')); ?>" rel="stylesheet"> -->


    <link href="<?php echo e(URL::asset('front_asset/css/plugins/plugins.css')); ?>" rel="stylesheet">
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('front_asset/revolution/css/settings.css')); ?>">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('front_asset/revolution/css/layers.css')); ?>">
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('front_asset/revolution/css/navigation.css')); ?>">

    <link href="<?php echo e(URL::asset('front_asset/css/style.css')); ?>" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('front_asset/cubeportfolio/css/cubeportfolio.min.css')); ?>">

    <?php if(Request::segment(1)=='product-list' || Request::segment(1)=='search-registry' || Request::segment(1)=='detail_registry' || Request::segment(1)=='product-details'): ?>
    <link href="<?php echo e(URL::asset('front_asset/css/shop-style.css')); ?>" rel="stylesheet">
    <?php endif; ?>
    

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo e(URL::asset('js/jquery.validate.min.js')); ?>"></script>
    <script src="<?php echo e(URL::asset('js/additional-methods.min.js')); ?>"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css" rel="stylesheet">

</head>

<body>
<!--top bar-->
<div class='top-bar clearfix light'>
    <div class='container'>
        <div class='float-sm-right'>
            <ul class='list-inline mb0'>
                <li class='list-inline-item'>
                    <a href="#" class="social-icon-sm si-gray si-gray-round si-facebook">
                        <i class="fa fa-facebook"></i>
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>

                <li class='list-inline-item'>
                    <a href="#" class="social-icon-sm si-gray si-gray-round si-twitter">
                        <i class="fa fa-twitter"></i>
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li class='list-inline-item'>
                    <a href="#" class="social-icon-sm si-gray si-gray-round si-instagram">
                        <i class="fa fa-instagram"></i>
                        <i class="fa fa-instagram"></i>
                    </a>
                </li>
                <li class='list-inline-item'>
                    <a href="#" class="social-icon-sm si-gray si-gray-round si-g-plus">
                        <i class="fa fa-google-plus"></i>
                        <i class="fa fa-google-plus"></i>
                    </a>
                </li>
            </ul>
        </div>
        
        <div class='float-sm-left'>
            <ul class='list-inline mb0 links'>
                <?php if(!Auth::user()): ?>
                    <li class='list-inline-item'><a href="<?php echo e(route('login')); ?>">Login</a></li>
                    <li class='list-inline-item'><a href="<?php echo e(route('register')); ?>">Register</a></li>
                <?php else: ?>
                    <?php if(Auth::user()->role=='User'): ?>
                        <li class='list-inline-item'>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                <?php echo e(Auth::user()->first_name); ?> <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class='list-inline-item'><a href="<?php echo e(route('home')); ?>">My Account</a></li>
                                <li class='list-inline-item'><a href="<?php echo e(route('change-password')); ?>">Change Password</a></li>
                                <li class='list-inline-item'><a href="<?php echo e(route('profile-update')); ?>">Update Profile</a></li>



                                <li class='list-inline-item'>
                                    <a href="<?php echo e(route('logout')); ?>"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                        <?php echo e(csrf_field()); ?>

                                    </form>
                                </li>
                            </ul>
                        </li>
                        <?php endif; ?>
                    <?php endif; ?>
                        <li class='list-inline-item'><a href='#'>What is MeriGift</a></li>
                        <li class='list-inline-item'><a href='#'>Create/Find Gift list</a></li>
                        <li class='list-inline-item'><a href="<?php echo e(url('search-registry-page')); ?>">Search Registry</a></li>
                        

            </ul>
        </div>
    </div>
</div>

<!--end top bar-->
<!-- Pushy Menu -->
<aside class="pushy pushy-right">
    <div class="cart-content">
        <div class="clearfix">
            <a href="javascript:void(0)" class="pushy-link text-white-gray">Close</a>
        </div>
<?php $cart_list=cart_list(); $total=0; ?>
        <ul class="list-unstyled">
            <?php $__empty_1 = true; $__currentLoopData = $cart_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <?php

                $total=$total+$data->quantity*$data->product->product_price;
                ?>
            <li class="clearfix">
                <a href="#" class="float-left">
                    <?php
                    $image=cart_get_product_image($data->product->id);
                    ?>
                    <?php if($image): ?>
                        <img class="img-fluid" src="<?php echo e(filePath('product',$image)); ?>" alt="Product Image" />
                    <?php else: ?>
                        <img class="img-fluid" src="<?php echo e(url('no_image.png')); ?>" alt="Product Image" />
                    <?php endif; ?>
                </a>
                <div class="oHidden">
                    <span class="close"><a href="<?php echo e(url('delete_cart/'.$data->id)); ?>"> <i class="ti-close"></i></a></span>
                    <h4><a href="#"><?php echo e($data->product->product_name); ?></a></h4>
                    <p class="text-white-gray"><strong>$<?php echo e($data->product->product_price); ?></strong> x <?php echo e($data->quantity); ?></p>
                </div>
            </li><!--/cart item-->
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <span>Your Cart is Empty, Please Add Some Item in Your Cart</span>
            <?php endif; ?>
            <?php if(count($cart_list)>0): ?>
            <li class="clearfix">


                <div class="float-right">
                    <?php if(!Auth::user()): ?>
                    <a href="<?php echo e(route('login')); ?>" class="btn btn-primary">Checkout</a>
                    <?php else: ?>
                    <a href="<?php echo e(route('check-out')); ?>" class="btn btn-primary">Checkout</a>
                    <?php endif; ?>
                </div>
                <h4  class="text-white">
                    <small>Subtotal - </small> $<?php echo e($total); ?>

                </h4>
            </li><!--/cart item-->
            <?php endif; ?>
        </ul>
    </div>
</aside>
<!-- Site Overlay -->
<div class="site-overlay"></div>

<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
    <div class="search-inline">
        <form>
            <input type="text" class="form-control" placeholder="Type and hit enter...">
            <button type="submit"><i class="ti-search"></i></button>
            <a href="javascript:void(0)" class="search-close"><i class="ti-close"></i></a>
        </form>
    </div><!--/search form-->
    <div class="container">
    
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="<?php echo e(route('index')); ?>"><img src="<?php echo e(URL::asset('front_asset/images/logo.png')); ?>" alt=""></a>
        <div  id="navbarNavDropdown" class="navbar-collapse collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active ">
                    <a class="nav-link" aria-haspopup="true" aria-expanded="false" href="<?php echo e(route('index')); ?>">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" aria-haspopup="true" aria-expanded="false" href="<?php echo e(route('brand')); ?>">Brand</a>
                </li>
                <?php $__currentLoopData = getCategory(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link " data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                            <?php echo e($data->cat_name); ?>

                        </a>
                        <?php if(count($data->sub_category)>0): ?>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <?php $__currentLoopData = $data->sub_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub_cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($sub_cat->sub_cat_deleted=='N' && $sub_cat->sub_cat_status=='Y'): ?>
                                    <li class=" dropdown-submenu dropdown">
                                        <a tabindex="-1" href="#" class="dropdown-item"><?php echo e($sub_cat->sub_cat_name); ?></a>
                                        <ul class="dropdown-menu">
                                            <?php $__currentLoopData = $data->product_type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product_type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($product_type->sub_category_id==$sub_cat->id && $product_type->category_id==$data->id): ?>
                                                    <?php if($product_type->pro_type_deleted=='N' && $product_type->pro_type_status=='Y'): ?>
                                                        <li><a class="dropdown-item" href="<?php echo e(url('product-list/'.$product_type->id)); ?>"><?php echo e($product_type->pro_type_name); ?></a></li>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </li>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                    
                        
                    
                
                
                    
                        
                    
                
                
                    
                
                
                    
                
                
                    
                        
                    
                
                
                    
                        
                    
                


            </ul>
        </div>
        <div class=" navbar-right-elements">
            <ul class="list-inline">
                <li class="list-inline-item"><a href="javascript:void(0)" class="search-open"><i class="ti-search"></i></a></li>

                <?php if(Request::segment(1)!='check-out'): ?>
                <li class="list-inline-item"><a href="javascript:void(0)" class=" menu-btn"><i class="ti-shopping-cart"></i> <span class="badge badge-default"><?php echo e(cart_count()); ?></span></a></li>
                <?php endif; ?>

            </ul>
        </div><!--right nav icons-->

    </div>
</nav>
<div class="category-list" style="display:none;">

<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
    <div class="container">
        <div  id="navbarNavDropdown" class="navbar-collapse collapse">
            <ul class="navbar-nav ml-auto">

            </ul>
        </div>
    </div>
</nav>
</div>
<?php echo $__env->yieldContent('content'); ?>



<footer class="footer footer-standard pt50 pb10">
    <div class="container">
        <div class="row">

            <div class="col-lg-2 col-md-2 mb40">

                <ul class="list-unstyled footer-list-item">
                    <li>
                        <a href="#">
                            About MeriGift
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Our Policies
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Our team
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Careers
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Contact Us
                        </a>
                    </li>

                </ul>
            </div>
            <div class="col-lg-2 col-md-2 mb40">

                <ul class="list-unstyled footer-list-item">
                    <li>
                        <a href="#">
                            Terms & Conditions
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Privacy Policy
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Return & Exchanges
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Delivery Options
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            FAQ's
                        </a>
                    </li>

                </ul>
            </div>

            <div class="col-lg-6 col-md-6 mb40">
                <h3>Subscribe to our newsletter</h3>
                <form id="widget-subscribe-form" action="" role="form" method="post" class="mb0">
                    <div class="input-group input-group-lg">

                        <input type="email" name="widget-subscribe-form-email" class="form-control required" placeholder="Enter your Email">
                        <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button">Subscribe</button>
                                </span>
                    </div>
                </form>
                <div class="row pt30">
                    <div class="col-sm-6">
                        <p class="contact-lead">
                            <small>Call us</small>
                            <br>+01 123-493-2949
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <p class="contact-lead">
                            <small>Mail us</small>
                            <br>contact@merigift.com
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="footer-bottomAlt">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="clearfix">
                    <a href="#" class="social-icon-sm si-dark si-facebook si-dark-round">
                        <i class="fa fa-facebook"></i>
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="social-icon-sm si-dark si-twitter si-dark-round">
                        <i class="fa fa-twitter"></i>
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="#" class="social-icon-sm si-dark si-g-plus si-dark-round">
                        <i class="fa fa-google-plus"></i>
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a href="#" class="social-icon-sm si-dark si-skype si-dark-round">
                        <i class="fa fa-skype"></i>
                        <i class="fa fa-skype"></i>
                    </a>
                    <a href="#" class="social-icon-sm si-dark si-linkedin si-dark-round">
                        <i class="fa fa-linkedin"></i>
                        <i class="fa fa-linkedin"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-5">
                <span>© Copyright 2018. All Right Reserved</span>
            </div>
        </div>
    </div>
</div>
<!--back to top-->
<a href="#" class="back-to-top hidden-xs-down" id="back-to-top"><i class="ti-angle-up"></i></a>



<!--<script src="<?php echo e(URL::asset('js/front_validations.js')); ?>"></script>-->

<!-- jQuery first, then Tether, then Bootstrap JS. -->

<script src="<?php echo e(URL::asset('front_asset/js/plugins/plugins.js')); ?>"></script>

<script src="<?php echo e(URL::asset('front_asset/js/assan.custom.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/cubeportfolio/js/jquery.cubeportfolio.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('front_asset/js/cube-thumb-slider.js')); ?>"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var date = new Date();

        var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();


        $('.datepicker').datepicker({

            minDate: 0,

            dateFormat: 'yy-mm-dd',

        });

        $(".datepicker1").datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 0,
            onSelect: function (date) {
                var dt2 = $('#delivery_date');
                var startDate = $(this).datepicker('getDate');
                var minDate = $(this).datepicker('getDate');
                dt2.datepicker('setDate', minDate);
                startDate.setDate(startDate.getDate() + 20);
                //sets dt2 maxDate to the last day of 30 days window
                //dt2.datepicker('option', 'maxDate', startDate);
                dt2.datepicker('option', 'minDate', startDate);
                $(this).datepicker('option', 'minDate', minDate);
            }
        });

    });
</script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/revolution/js/jquery.themepunch.tools.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/revolution/js/jquery.themepunch.revolution.min.js')); ?>"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/revolution/js/extensions/revolution.extension.actions.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/revolution/js/extensions/revolution.extension.carousel.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/revolution/js/extensions/revolution.extension.kenburn.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/revolution/js/extensions/revolution.extension.layeranimation.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/revolution/js/extensions/revolution.extension.migration.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/revolution/js/extensions/revolution.extension.navigation.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/revolution/js/extensions/revolution.extension.parallax.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/revolution/js/extensions/revolution.extension.slideanims.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('front_asset/revolution/js/extensions/revolution.extension.video.min.js')); ?>"></script>


<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        /* var table = $('#example').DataTable( {
            responsive: true
        } );
 */
        //new $.fn.dataTable.FixedHeader( table );

        $('#summernote').summernote({
            placeholder: 'Thank you Letter',
            tabsize: 2,
            height: 100
        });

    } );

</script>

<script>
    /**Hero  script**/
    var tpj = jQuery;

    var revapi1078;
    tpj(document).ready(function () {
        if (tpj("#rev_slider_1078_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_1078_1");
        } else {
            revapi1078 = tpj("#rev_slider_1078_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "revolution/js/",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 8000,
                navigation: {
                    arrows: {
                        enable: true,
                        style: 'uranus',
                        tmp: '',
                        rtl: false,
                        hide_onleave: false,
                        hide_onmobile: true,
                        hide_under: 600,
                        hide_over: 9999,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        left: {
                            container: 'slider',
                            h_align: 'left',
                            v_align: 'center',
                            h_offset: 0,
                            v_offset: 0
                        },
                        right: {
                            container: 'slider',
                            h_align: 'right',
                            v_align: 'center',
                            h_offset: 0,
                            v_offset: 0
                        }
                    }
                },
                viewPort: {
                    enable: true,
                    outof: "pause",
                    visible_area: "80%",
                    presize: false
                },
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: [1140, 992, 700, 465],
                gridheight: [600, 600, 500, 480],
                lazyType: "none",
                parallax: {
                    type: "mouse",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, 46, 47, 48, 49, 50, 55]
                },
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false
                }
            });
        }
    });	/*ready*/

    $(".product").click(function(){
        $(".category-list").toggle();
    });

</script>

</body>

<!-- Mirrored from bootstraplovers.com/bootstrap4/assan-bootstrap4/default-template/html/shop-home.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 17 Jul 2017 15:17:25 GMT -->
</html>

