<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
    'facebook' => [
        'client_id' => '277746195916762',
        'client_secret' => 'b76eb579dc2b8099395a5cf0c84591af',
        'redirect' => 'http://parkproject.asia/merigift/public/callback',
    ],
    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],
    'google' => [
        'client_id' => '867684342878-tkge3oub4t809lu06kjedl51a02gkkku.apps.googleusercontent.com',
        'client_secret' => 'NG_gyVr19DWB8zxm9jSPG2xI',
        'redirect' => 'http://parkproject.asia/merigift/public/google-callback',
    ], 

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

];
