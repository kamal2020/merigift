-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 23, 2018 at 02:49 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.25-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `merigift`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(150) DEFAULT NULL,
  `brand_image` varchar(100) DEFAULT NULL,
  `brand_logo` varchar(100) DEFAULT NULL,
  `brand_status` enum('Y','N') DEFAULT 'Y',
  `brand_deleted` enum('Y','N') DEFAULT 'N',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(100) DEFAULT NULL,
  `cat_status` enum('Y','N') DEFAULT 'Y',
  `cat_daleted` enum('Y','N') DEFAULT 'N',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `cat_name`, `cat_status`, `cat_daleted`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'Y', 'N', '2018-02-22 10:19:16', '2018-02-22 12:28:21'),
(2, 'Kitchen', 'Y', 'N', '2018-02-22 10:20:51', '2018-02-22 12:29:41'),
(3, 'Other', 'Y', 'N', '2018-02-22 10:25:20', '2018-02-22 12:30:03'),
(4, 'Home1111', 'Y', 'N', '2018-02-22 12:26:20', '2018-02-22 12:26:20'),
(5, 'erwerwer', 'Y', 'N', '2018-02-23 05:28:38', '2018-02-23 05:28:38'),
(6, 'AAA', 'Y', 'N', '2018-02-23 05:40:23', '2018-02-23 05:40:23'),
(7, NULL, 'Y', 'N', '2018-02-23 05:59:00', '2018-02-23 05:59:00'),
(8, 'AAA', 'Y', 'N', '2018-02-23 06:54:18', '2018-02-23 06:54:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `sub_cat_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_description` text,
  `product_price` float(10,2) DEFAULT NULL,
  `product_status` enum('Y','N') NOT NULL DEFAULT 'Y',
  `product_deleted` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products_images`
--

CREATE TABLE `products_images` (
  `pro_img_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `pro_type_id` int(11) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `sub_cat_id` int(11) DEFAULT NULL,
  `pro_type_name` varchar(100) DEFAULT NULL,
  `pro_type_status` enum('Y','N') NOT NULL DEFAULT 'Y',
  `pro_type_deleted` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categorys`
--

CREATE TABLE `sub_categorys` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_cat_name` varchar(100) DEFAULT NULL,
  `sub_cat_status` enum('Y','N') NOT NULL DEFAULT 'Y',
  `sub_cat_deleted` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_categorys`
--

INSERT INTO `sub_categorys` (`id`, `category_id`, `sub_cat_name`, `sub_cat_status`, `sub_cat_deleted`, `created_at`, `updated_at`) VALUES
(1, 2, 'saas', 'Y', 'N', '2018-02-21 18:30:00', '2018-02-23 07:37:25'),
(2, 2, 'qwe', 'N', 'N', '2018-02-23 06:51:07', '2018-02-23 07:37:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner1_first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner2_first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weeding_date` date DEFAULT NULL,
  `guest_exp` int(11) DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('Admin','User') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Admin',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `partner1_first_name`, `partner2_first_name`, `weeding_date`, `guest_exp`, `contact_number`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sdasd', 'kamal@gmail.com', '$2y$10$q8nABPFRAESHOels3UdQAeKrAwGJyOzrXHlOo3KyJ9FmU9D6LiN4e', NULL, NULL, NULL, NULL, NULL, 'Admin', 'rk5i3vhkKybswxkBvTKgQiJHVxJQVR72DpHYUdbjIW5oxeCrfayDThhkeki1', '2018-02-21 04:12:17', '2018-02-21 04:12:17'),
(8, 'sadsad', 'kamalps008@gmail.com', '$2y$10$u2u7dnvTS32bqEsvDfK/n.5FrrJ4z4le8cQPHlHHFY6RFFhzCly/e', NULL, NULL, NULL, NULL, NULL, 'User', 'fmSW15WTMUkkOGkV6F17poyGwkhCPFYA6TrrsCyIeqwNgor6UrcZVQvCMlEP', '2018-02-21 06:07:42', '2018-02-21 06:07:42'),
(9, 'sadsad', 'kamalps0081@gmail.com', '$2y$10$CXfZq/b94Z4hM/PHQbebCeQH3HMNA.67DKaLNZSpN/2cd2ZngJWDC', NULL, NULL, NULL, NULL, NULL, 'Admin', NULL, '2018-02-21 06:08:34', '2018-02-21 06:08:34'),
(10, 'sadsad', 'kamalps0081s@gmail.com', '$2y$10$.Y.xH7GHNNf0BCyqKysh4.bpeokT1F0cW8oakHYPk9bH.6JD6h6cu', NULL, NULL, NULL, NULL, NULL, 'Admin', 'GCT3u6NvIpgbRywrukuQ8JTn3RXsKYIdN1mt0IYsmzaFgUW6HeknqPkQMkwK', '2018-02-21 06:09:53', '2018-02-21 06:09:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `products_images`
--
ALTER TABLE `products_images`
  ADD PRIMARY KEY (`pro_img_id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`pro_type_id`);

--
-- Indexes for table `sub_categorys`
--
ALTER TABLE `sub_categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products_images`
--
ALTER TABLE `products_images`
  MODIFY `pro_img_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_types`
--
ALTER TABLE `product_types`
  MODIFY `pro_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_categorys`
--
ALTER TABLE `sub_categorys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
